package uz.droid.dialog;

import android.os.Bundle;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 11.06.2015 16:40.
 */
public interface DialogEventListener {

    void onDialogEvent(int eventId, Bundle data);
}
