package uz.droid.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 11.06.2015 16:42.
 */
public class AlertDialogFragment extends BaseDialog {
    public static final String TITLE = "TITLE";
    public static final String MESSAGE = "MESSAGE";

    public static final String POSITIVE_BUTTON_TEXT = "POSITIVE_BUTTON_TEXT";
    public static final String NEGATIVE_BUTTON_TEXT = "NEGATIVE_BUTTON_TEXT";
    public static final String NEUTRAL_BUTTON_TEXT = "NEUTRAL_BUTTON_TEXT";

    public static final String POSITIVE_CLICK_EVENT_ID = "POSITIVE_CLICK_EVENT_ID";
    public static final String NEGATIVE_CLICK_EVENT_ID = "NEGATIVE_CLICK_EVENT_ID";
    public static final String NEUTRAL_CLICK_EVENT_ID = "NEUTRAL_CLICK_EVENT_ID";

    public static final String DATA = "DATA";
    public static final String IS_CANCELABLE = "IS_CANCELABLE";
    public static final String DIALOG_TAG = "DIALOG_TAG";

    public static final String IS_NEGATIVE_ENABLED = "IS_NEGATIVE_ENABLED";

    private DialogInterface.OnClickListener dialogOnClickListener;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());

        builder.setTitle(getArgumentString(TITLE, " "));
        builder.setMessage(getArgumentString(MESSAGE, ""));
        setCancelable(getArgumentBoolean(IS_CANCELABLE, true));

        dialogOnClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == Dialog.BUTTON_POSITIVE) {
                    if (getArgumentInt(POSITIVE_CLICK_EVENT_ID) > 0) {
                        dispatchActivityEvent(getArgumentInt(POSITIVE_CLICK_EVENT_ID), getArguments());
                    }
                } else if (which == Dialog.BUTTON_NEGATIVE) {
                    if (getArgumentInt(NEGATIVE_CLICK_EVENT_ID) > 0) {
                        dispatchActivityEvent(getArgumentInt(NEGATIVE_CLICK_EVENT_ID), getArguments());
                    }
                }
            }
        };

        builder.setPositiveButton(getArgumentString(POSITIVE_BUTTON_TEXT, "Ok"), dialogOnClickListener);


        if (getArgumentBoolean(IS_NEGATIVE_ENABLED, false)) {
            builder.setNegativeButton(getArgumentString(NEGATIVE_BUTTON_TEXT, "Close"), dialogOnClickListener);
        }

        final AlertDialog dialog = builder.create();


        return dialog;
    }

    public void show(FragmentManager manager, Bundle bundle) {
        setArguments(bundle);
        Fragment f = manager.findFragmentByTag(getDialogTag());
        if (f == null || !f.isAdded()) {
            show(manager, getDialogTag());
        }
    }

    public static void hide(FragmentManager fm, String tag) {
        if (fm != null && tag != null) {
            Fragment fr = fm.findFragmentByTag(tag);
            if (fr != null && fr.isAdded() && fr instanceof BaseDialog) {
                try {
                    ((BaseDialog) fr).dismiss();
                } catch (Exception e) {
                }
            }
        }
    }

    protected String getArgumentString(String key, int defValId) {
        return getArgumentString(key, getString(defValId));
    }

    protected String getArgumentString(String key, String defValue) {
        if (getArguments() != null && getArguments().getString(key) != null) {
            return getArguments().getString(key);
        }

        return defValue;
    }

    protected boolean getArgumentBoolean(String key, boolean defVal) {
        return getArguments() != null && getArguments().getBoolean(key, defVal);
    }

    protected boolean getArgumentBoolean(String key) {
        return getArguments() != null && getArguments().getBoolean(key, false);
    }

    protected int getArgumentInt(String key) {
        if (getArguments() != null) {
            return getArguments().getInt(key, -1);
        }
        return -1;
    }

    protected String getDialogTag() {
        return getArgumentString(DIALOG_TAG, ((Object)this).getClass().getName());
    }

    public static class Builder {
        private android.support.v4.app.FragmentManager fm;
        private String tag;
        private String title;
        private String message;
        private Integer positiveClickEventId;
        private Boolean cancelable;
        private String positiveText;
        private String negativeText;
        private Boolean negativeButtonEnabled;
        private Bundle bundle;
        private Integer dismissEventId;
        private Boolean disableDismiss;

        public Builder(FragmentManager fm, String tag) {
            this(fm, tag, null);
        }

        public Builder(FragmentManager fm, String tag, Bundle bundle) {
            this.fm = fm;
            this.tag = tag;
            this.bundle = bundle;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setPositiveClickEventId(Integer positiveClickEventId) {
            this.positiveClickEventId = positiveClickEventId;
            return this;
        }

        public Builder setCancelable(Boolean cancelable) {
            this.cancelable = cancelable;
            return this;
        }

        public Builder setPositiveText(String positiveText) {
            this.positiveText = positiveText;
            return this;
        }

        public Builder setNegativeText(String negativeText) {
            this.negativeButtonEnabled = true;
            this.negativeText = negativeText;
            return this;
        }

        public Builder setNegativeButtonEnabled(Boolean negativeButtonEnabled) {
            this.negativeButtonEnabled = negativeButtonEnabled;
            return this;
        }

        public Builder setDismisDisabled(Boolean dismissDisabled) {
            this.disableDismiss = dismissDisabled;
            return this;
        }

        public Builder setDismissEventId(Integer dismissEventId) {
            this.dismissEventId = dismissEventId;
            return this;
        }

        public void show() {

            Bundle b = bundle != null ? bundle : new Bundle();

            if (tag != null) {
                b.putString(DIALOG_TAG, tag);
            }

            if (cancelable != null) {
                b.putBoolean(IS_CANCELABLE, cancelable);
            }

            if (title != null) {
                b.putString(TITLE, title);
            }

            if (message != null) {
                b.putString(MESSAGE, message);
            }

            if (positiveText != null) {
                b.putString(POSITIVE_BUTTON_TEXT, positiveText);
            }

            if (positiveClickEventId != null) {
                b.putInt(POSITIVE_CLICK_EVENT_ID, positiveClickEventId);
            }

            if (negativeButtonEnabled != null) {
                b.putBoolean(IS_NEGATIVE_ENABLED, negativeButtonEnabled);
            }

            if (negativeText != null) {
                b.putString(NEGATIVE_BUTTON_TEXT, negativeText);
            }

//            if (dismissEventId != null) {
//                b.putInt(ON_DISMISS_EVENT_ID, dismissEventId);
//            }

//            if (disableDismiss != null) {
//                b.putBoolean(DISABLE_DISMISS, disableDismiss);
//            }

            new AlertDialogFragment().show(fm, b);
        }
    }

}
