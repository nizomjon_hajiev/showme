package uz.droid.dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 11.06.2015 16:39.
 */
public class BaseDialog extends DialogFragment {

    protected void dispatchActivityEvent(int eventId, Bundle data) {
        if (getActivity() != null && getActivity() instanceof DialogEventListener) {
            ((DialogEventListener) getActivity()).onDialogEvent(eventId, data);
        }
    }
}
