package uz.fonus.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.Layout;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 13.04.2015 11:23.
 */
public class CountIndicatorEditText extends EditText{

    private int mLimit = 140;
    private int indicatorLeftMargin = 10;

    public CountIndicatorEditText(Context context) {
        super(context);
        init();
    }

    public CountIndicatorEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CountIndicatorEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void setLimit(int limit) {
        this.mLimit = limit;
    }

    private Paint mPaint;

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int pos = getText().length();
        if (pos == 0) {
            return;
        }
        Layout layout = getLayout();
        int line = layout.getLineForOffset(pos);
        int btm = layout.getLineBottom(line);
        int decent = layout.getLineDescent(line);
        float limitTextSize = getTextSize();
        float x = layout.getPrimaryHorizontal(pos) + indicatorLeftMargin + getPaddingLeft();
        float y = btm + decent;
        if (mPaint == null) {
            mPaint = new Paint(getPaint());
            mPaint.setTextSize(limitTextSize);
            mPaint.setColor(0xff7e7e7e);
        }

        // TODO crop over bottom padding
        int charsLeft = mLimit - getText().length();
        int clipLeft = 0;
        int clipRight = getRight() - getLeft();
        int clipTop = getScrollY() + getExtendedPaddingTop();
        int clipBottom = getBottom() - getTop() + getScrollY() - getExtendedPaddingBottom();

//        canvas.clipRect(clipLeft, clipRight, clipTop, clipBottom);

        canvas.drawText(String.valueOf(charsLeft), x, y, mPaint);
    }

    private void init() {
        indicatorLeftMargin = (int)(10 * getResources().getDisplayMetrics().density);
    }

}
