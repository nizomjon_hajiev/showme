package uz.fonus.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import uz.ishow.android.R;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 06.10.2015 16:30.
 */
public class CircularProgressBar extends View {

    private int mProgress = 0;
    private int layoutWidth;
    private int layoutHeight;

    private float strokeWidth;

    private float mDegree = 0f;

    private Paint mCirclePaint;
    private Paint mBackgroundCirclePaint;
    private RectF circleBounds;

    public CircularProgressBar(Context context) {
        super(context);
        init();
    }

    public CircularProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CircularProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public CircularProgressBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }


    private void init() {
        setWillNotDraw(false);
        strokeWidth = Math.round(2f * getResources().getDisplayMetrics().density);
        mCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCirclePaint.setColor(getResources().getColor(R.color.primary));
        mCirclePaint.setStyle(Paint.Style.STROKE);
        mCirclePaint.setStrokeWidth(strokeWidth);

        mBackgroundCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mBackgroundCirclePaint.setColor(Color.TRANSPARENT);
        mBackgroundCirclePaint.setStyle(Paint.Style.STROKE);
        mBackgroundCirclePaint.setStrokeWidth(strokeWidth);
    }

    public void setBackgroundCircleColor(int color) {
        mBackgroundCirclePaint.setColor(color);
    }

    public void setProgressColor(int color) {
        mCirclePaint.setColor(color);
    }

    public void setProgress(int progress) {
        mProgress = progress;
        mProgress = Math.min(mProgress, 100);
        mProgress = Math.max(mProgress, 0);
        mDegree = mProgress * 360f / 100;
        postInvalidate();
    }

    public int getProgress() {
        return mProgress;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawArc(circleBounds, 0, 360, false, mBackgroundCirclePaint);
        canvas.drawArc(circleBounds, -90, mDegree, false, mCirclePaint);
    }

    @Override
    protected void onSizeChanged(int newWidth, int newHeight, int oldWidth, int oldHeight) {
        super.onSizeChanged(newWidth, newHeight, oldWidth, oldHeight);
        layoutWidth = newWidth;
        layoutHeight = newHeight;
        setupBounds();
//        setupPaints();
        invalidate();
    }

    private void setupBounds() {
        int width = getWidth();
        int height = getHeight();
        float halfStroke = strokeWidth / 2f;
        circleBounds = new RectF(halfStroke, halfStroke, width - halfStroke, height - halfStroke);
    }


}
