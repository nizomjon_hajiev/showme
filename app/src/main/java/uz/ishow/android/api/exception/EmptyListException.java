package uz.ishow.android.api.exception;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 23.09.2015 12:11.
 */
public class EmptyListException extends RuntimeException {

    public EmptyListException() {
        super("");
    }
}
