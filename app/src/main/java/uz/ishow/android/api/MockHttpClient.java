package uz.ishow.android.api;

import android.content.Context;
import android.net.Uri;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import retrofit.client.Client;
import retrofit.client.Request;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import uz.ishow.android.util.AssetsUtil;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 02.07.2015 18:50.
 */
public class MockHttpClient implements Client {

    private Context context;

    public MockHttpClient(Context context) {
        this.context = context;
    }

    @Override
    public Response execute(Request request) throws IOException {
        Uri uri = Uri.parse(request.getUrl());

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        String responseString = "{}";

        String path = uri.getPath();

        if (path.equals("/feed/")) {
            responseString = AssetsUtil.fileToString(context.getAssets(), "sample/timeline.json");
        } else if (path.startsWith("/feed/")) {
            String id = path.substring("/feed/".length());
            responseString = AssetsUtil.fileToString(context.getAssets(), "sample/" + userFolder(id) + "/timeline.json");
        } else if (path.equals("/news/")) {
            responseString = AssetsUtil.fileToString(context.getAssets(), "sample/news.json");
        } else if (path.equals("/catalog/")) {
            responseString = AssetsUtil.fileToString(context.getAssets(), "sample/catalog.json");
        } else if (path.equals("/catalog/1/")) {
            responseString = AssetsUtil.fileToString(context.getAssets(), "sample/catalog.1.json");
        } else if (path.startsWith("/user/")) {
            String id = path.substring("/user/".length());
            responseString = AssetsUtil.fileToString(context.getAssets(), "sample/" + userFolder(id) + "/info.json");
        } else if (path.startsWith("/comment/")) {
            responseString = AssetsUtil.fileToString(context.getAssets(), "sample/comments.json");
        } else if (path.startsWith("/register")) {
        }

        return new Response(request.getUrl(), 200, "nothing", Collections.EMPTY_LIST,
                new TypedByteArray("application/json", responseString.getBytes()));
    }

    private String userFolder(String id) {
        Map<String, String> map = new HashMap<>();
        map.put("1/", "shahzoda");
        map.put("2/", "munisa");
        map.put("3/", "rayhon");
        map.put("4/", "bojalar");
        map.put("5/", "lola");
        map.put("6/", "ziyoda");
        map.put("7/", "shohruhxon");

        return map.get(id);
    }
}
