package uz.ishow.android.api.body;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 13.02.2016 21:11.
 */
public class LogoutBody {
    private String deviceId;

    public LogoutBody(String deviceId) {
        this.deviceId = deviceId;
    }
}
