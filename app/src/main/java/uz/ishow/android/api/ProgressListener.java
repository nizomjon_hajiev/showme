package uz.ishow.android.api;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 13.10.2015 14:36.
 */
public interface ProgressListener {
    void transferred(long num);
}
