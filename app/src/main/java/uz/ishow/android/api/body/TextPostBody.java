package uz.ishow.android.api.body;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 02.11.2015 18:29.
 */
public class TextPostBody {

    private String text;

    public TextPostBody(String text) {
        this.text = text;
    }
}
