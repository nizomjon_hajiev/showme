package uz.ishow.android.api.body;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 08.10.2015 10:39.
 */
public class CommentBody {
    private String text;

    public CommentBody(String text) {
        this.text = text;
    }
}
