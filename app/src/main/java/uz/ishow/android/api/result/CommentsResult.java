package uz.ishow.android.api.result;

import uz.ishow.android.model.Comment;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 07.07.2015 17:27.
 */
public class CommentsResult extends ListResult<Comment> {

    private Boolean canLoadComments;

    public Boolean getCanLoadComments() {
        return canLoadComments != null && canLoadComments;
    }
}
