package uz.ishow.android.api.body;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 13.10.2015 16:38.
 */
public class EditUserBody {

    private String name;
    private String photo;
    private String about;
    private String contactPhone;

    public EditUserBody(String name, String photo, String about, String contactPhone) {
        this.name = name;
        this.photo = photo;
        this.about = about;
        this.contactPhone = contactPhone;
    }
}
