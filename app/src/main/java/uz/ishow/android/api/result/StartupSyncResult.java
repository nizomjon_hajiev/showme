package uz.ishow.android.api.result;

import uz.ishow.android.model.User;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 09.10.2015 15:29.
 */
public class StartupSyncResult {

    private User user;

    public User getUser() {
        return user;
    }
}
