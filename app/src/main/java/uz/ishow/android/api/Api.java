package uz.ishow.android.api;

import java.util.List;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.PATCH;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import uz.ishow.android.api.body.AuthBody;
import uz.ishow.android.api.body.CodeCheckBody;
import uz.ishow.android.api.body.CommentBody;
import uz.ishow.android.api.body.EditUserBody;
import uz.ishow.android.api.body.LogoutBody;
import uz.ishow.android.api.body.PhotoPostBody;
import uz.ishow.android.api.body.SmsAuthBody;
import uz.ishow.android.api.body.StartupSyncBody;
import uz.ishow.android.api.body.TextPostBody;
import uz.ishow.android.api.callback.BaseCallback;
import uz.ishow.android.api.callback.ListCallback;
import uz.ishow.android.api.result.AddCommentResult;
import uz.ishow.android.api.result.AuthResult;
import uz.ishow.android.api.result.CatalogResult;
import uz.ishow.android.api.result.CommentsResult;
import uz.ishow.android.api.result.FileUploadResult;
import uz.ishow.android.api.result.FollowResult;
import uz.ishow.android.api.result.LikeResult;
import uz.ishow.android.api.result.PhotoPostResult;
import uz.ishow.android.api.result.StartupSyncResult;
import uz.ishow.android.api.result.TextPostResult;
import uz.ishow.android.model.CatalogContentResult;
import uz.ishow.android.model.Category;
import uz.ishow.android.model.FeedItem;
import uz.ishow.android.model.Nomination;
import uz.ishow.android.model.Post;
import uz.ishow.android.model.Profile;
import uz.ishow.android.model.User;

//import rx.Observable;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 19.06.2015 14:47.
 */

public interface Api {


    @GET("/feed/{owner_id}.json")
    void getSingerFeed(@Path("owner_id") long ownerId, Callback<List<Post>> callback);

    @GET("/timeline/")
    void getMyFeed(@Query("time") Long utcSeconds, ListCallback<FeedItem> callback);

    @GET("/news/")
    void getNews(@Query("time") Long utcSeconds, ListCallback<FeedItem> callback);

    @GET("/users/{user_id}/timeline/")
    void getProfileTimeline(@Path("user_id") String userId, @Query("time") Long time,
                            ListCallback<FeedItem> callback);

    @GET("/catalog/")
    void getCatalogs(Callback<CatalogResult> callback);

    @GET("/categories/")
    void getCategories(ListCallback<Category> callback);

    @GET("/categories/{category_id}/users/")
    void getCategoryUsers(@Path("category_id") String categoryId, ListCallback<Profile> callback);

//    @GET("/categories/{category_id}/users/")
//    Observable<List<Profile>> getCategoryUsers(@Path("category_id") String categoryId);

    @GET("/categories/{category_id}/famous/users/")
    void getCategoryFamousUsers(@Path("category_id") String categoryId, ListCallback<Profile> callback);

    @GET("/catalog/{catalog_id}/")
    void getCatalogUsers(@Path("catalog_id") Long catalogId, Callback<CatalogContentResult> callback);

    @GET("/posts/{post_id}/comments/")
    void getComments(@Path("post_id") String postId,
                     @Query("time") Long utcSeconds,
                     BaseCallback<CommentsResult> callback);

    @POST("/posts/{post_id}/comments/")
    void addComment(@Path("post_id") String postId, @Body CommentBody body, BaseCallback<AddCommentResult> callback);

    @DELETE("/comments/{comment_id}")
    void deleteComment(@Path("comment_id") String commentId, BaseCallback<Response> callback);

    @GET("/users/suggested/")
    void getSuggestedProfiles(ListCallback<Profile> callback);

    @POST("/auth/sms/")
    void codeRequest(@Body SmsAuthBody body, BaseCallback<Response> callback);

//    @POST("/auth/sms/")
//    Observable<Response> codeRequestRx(@Body SmsAuthBody body);

    @POST("/auth/sms/check/")
    void smsCodeCheck(@Body CodeCheckBody body, BaseCallback<AuthResult> callback);

    @POST("/auth/")
    void auth(
            @Header("X-Verify-Credentials-Authorization") String xVerifyCredentialsAuthorization,
            @Header("X-Auth-Service-Provider") String xAuthServiceProvider,
            @Body AuthBody authBody,
            BaseCallback<AuthResult> callback);

    @POST("/users/{user_id}/follow/")
    void follow(@Path("user_id") String userId, BaseCallback<FollowResult> callback);

    @DELETE("/users/{user_id}/follow/")
    void unfollow(@Path("user_id") String userId, BaseCallback<FollowResult> callback);

    @POST("/users/{user_id}/block/")
    void blockUser(@Path("user_id") String userId, BaseCallback<Response> callback);

    @POST("/posts/{post_id}/likes/")
    void like(@Path("post_id") String postId, BaseCallback<LikeResult> callback);

    @DELETE("/posts/{post_id}/likes/")
    void unlike(@Path("post_id") String postId, BaseCallback<LikeResult> callback);

    @POST("/startup/")
    void syncStartupData(@Body StartupSyncBody body, BaseCallback<StartupSyncResult> callback);

    @POST("/files/")
    FileUploadResult uploadFile(@Body MyMultipartTypedOutput myMultipart);

    @PATCH("/profile/")
    void editUser(@Body EditUserBody user, BaseCallback<User> callback);

    @POST("/photos/")
    void newPhotoPost(@Body PhotoPostBody photoPost, BaseCallback<PhotoPostResult> callback);

    @POST("/posts/")
    void newTextPost(@Body TextPostBody textPost, BaseCallback<TextPostResult> callback);

    @DELETE("/posts/{post_id}")
    void deletePost(@Path("post_id") String postId, BaseCallback<Response> callback);

    @GET("/users/follower/search/")
    void searchFollowerUsers(@Query("keyword") String keyword, ListCallback<Profile> callback);

    @GET("/users/search/")
    void searchUsers(@Query("keyword") String keyword, ListCallback<Profile> callback);

    @GET("/posts/search/")
    void searchByContent(@Query("keyword") String keyword, ListCallback<Post> callback);

    @GET("/posts/{post_id}")
    void getPost(@Path("post_id") String postId, BaseCallback<Post> callback);

    @POST("/logout/")
    void logout(@Body LogoutBody body, BaseCallback<Response> callback);

//    @POST("/logout/")
//    Observable<Response> logout(@Body LogoutBody body);

    @GET("/nominations/")
    void getNominations(ListCallback<Nomination> callback);

    @GET("/nominations/{nomination_id}")
    void getNomination(@Path("nomination_id") String nominationId, BaseCallback<Nomination> callback);

    @POST("/contributors/{nominee_id}")
    void vote(@Path("nominee_id") String nomineeId, BaseCallback<Response> callback);

    @GET("/users/{user_id}/follows/")
    void getFollowings(@Path("user_id") String userId, ListCallback<Profile> callback);

//    @GET("/users/{user_id}/follows/")
//    Observable<ListResult<Profile>> getFollowers(@Path("user_id") String userId);

//    @GET()
}
