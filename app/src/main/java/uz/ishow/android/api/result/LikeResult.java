package uz.ishow.android.api.result;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 07.10.2015 16:15.
 */
public class LikeResult {

    private Integer likesCount;

    public Integer getLikesCount() {
        return likesCount != null ? likesCount : 0;
    }
}
