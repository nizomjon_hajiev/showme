package uz.ishow.android.api.result;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 30.09.2015 14:23.
 */
public class FollowResult {

    private Integer followersCount;

    public Integer getFollowersCount() {
        return followersCount != null ? followersCount : 0;
    }
}
