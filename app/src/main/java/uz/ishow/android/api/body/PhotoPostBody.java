package uz.ishow.android.api.body;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 02.11.2015 18:29.
 */
public class PhotoPostBody {

    private String text;
    private String photo;

    public PhotoPostBody(String text, String photo) {
        this.text = text;
        this.photo = photo;
    }
}
