package uz.ishow.android.api.result;

import uz.ishow.android.model.User;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 30.09.2015 11:32.
 */
public class AuthResult {

    private String token;

    private User user;

    public String getToken() {
        return token;
    }

    public User getUser() {
        return user;
    }
}
