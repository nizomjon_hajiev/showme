package uz.ishow.android.api.callback;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 23.09.2015 14:56.
 */
public abstract class BaseCallback<T> implements Callback<T> {

    @Override
    public void success(T t, Response response) {
        success(t);
        complete();
    }

    @Override
    public final void failure(RetrofitError error) {
        if (error.getKind() == RetrofitError.Kind.NETWORK) {
            networkError();
        } else {
            if (error.getResponse() != null){
                int status = error.getResponse().getStatus();
                if (status == 404) {
                    error404();
                }
            }
            apiError(error);
        }
        error(error);
        complete();
    }

    public abstract void success(T result);

    public void complete() {
    }

    public void error(RetrofitError e) {
    }

    public void error404() {
    }

    public void apiError(RetrofitError e) {
    }

    public void networkError() {
    }
}
