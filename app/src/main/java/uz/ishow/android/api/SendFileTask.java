package uz.ishow.android.api;

import android.os.AsyncTask;

import java.io.File;

import retrofit.RetrofitError;
import retrofit.mime.TypedString;
import uz.ishow.android.api.body.CountingTypedFile;
import uz.ishow.android.api.result.FileUploadResult;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 13.10.2015 14:34.
 */
public abstract class SendFileTask extends AsyncTask<String, Integer, FileUploadResult> {

    private ProgressListener listener;
    private String filePath;
    private long totalSize;
    private Api api;
    private String type;

    private RetrofitError error;

    public SendFileTask(String filePath, Api api, String type) {
        this.filePath = filePath;
        this.api = api;
        this.type = type;
    }

    @Override
    protected FileUploadResult doInBackground(String... params) {
        File file = new File(filePath);
        totalSize = file.length();
        listener = new ProgressListener() {
            @Override
            public void transferred(long num) {
                publishProgress((int) ((num / (float) totalSize) * 100));
            }
        };

        try {
            String mimeType = "image/jpeg";
            if (filePath != null && filePath.toLowerCase().endsWith(".png")) {
                mimeType = "image/png";
            }

            MyMultipartTypedOutput body = new MyMultipartTypedOutput();
            body.addPart("file-type", new TypedString(type));
            body.addPart("photo", new CountingTypedFile(mimeType, file, listener));
            return api.uploadFile(body);
        } catch (RetrofitError e) {
            error = e;
        }

        return null;
    }

    @Override
    protected final void onPostExecute(FileUploadResult fileUploadResult) {
        if (error != null) {
            if (error.getKind() == RetrofitError.Kind.NETWORK) {
                networkError();
            }
            error(error);
        } else {
            success(fileUploadResult);
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        onProgress(values[0]);
    }

    protected void onProgress(int progress) {
    }

    protected void networkError() {
    }

    protected void error(RetrofitError error) {
    }

    protected abstract void success(FileUploadResult result);

}
