package uz.ishow.android.api.body;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 14.01.2016 11:59.
 */
public class CodeCheckBody {

    private String phone;
    private String code;
    private String deviceId;

    public CodeCheckBody(String phone, String code, String deviceId) {
        this.phone = phone;
        this.code = code;
        this.deviceId = deviceId;
    }
}
