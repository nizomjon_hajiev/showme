package uz.ishow.android.api.result;

import java.util.List;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 02.07.2015 17:39.
 */
public class ListResult<T> {

    private Integer count;
    private List<T> results;

    public Integer getCount() {
        return count;
    }

    public List<T> getResults() {
        return results;
    }
}
