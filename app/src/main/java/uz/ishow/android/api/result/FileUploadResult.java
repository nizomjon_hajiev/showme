package uz.ishow.android.api.result;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 13.10.2015 14:41.
 */
public class FileUploadResult {

    private String id;
    private String url;

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }
}
