package uz.ishow.android.api.body;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 30.09.2015 11:11.
 */
public class AuthBody {

    private String phone;
    private String name;
    private String deviceId;

    public AuthBody(String phone, String name, String deviceId) {
        this.phone = phone;
        this.name = name;
        this.deviceId = deviceId;
    }
}
