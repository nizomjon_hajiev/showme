package uz.ishow.android.api.result;

import uz.ishow.android.model.Comment;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 17.03.2016 22:08.
 */
public class AddCommentResult {

    private String result;
    private String message;
    private Integer count;
    private Comment data;

    public String getMessage() {
        return message;
    }

    public Integer getCount() {
        return count;
    }

    public Comment getData() {
        return data;
    }

    public boolean isSuccess() {
        return "success".equals(result);
    }
}
