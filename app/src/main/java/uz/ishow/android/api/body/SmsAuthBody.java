package uz.ishow.android.api.body;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 14.01.2016 11:57.
 */
public class SmsAuthBody {

    private String name;
    private String phone;

    public SmsAuthBody(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }
}
