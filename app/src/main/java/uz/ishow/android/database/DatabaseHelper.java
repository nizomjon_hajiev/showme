package uz.ishow.android.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import uz.ishow.android.model.AudioOffline;

/**
 * Created by User on 02.04.2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "audio";

    private static final String TABLE_AUDIO = "table_audio";
    private static final String ID = "id";
    private static final String AUDIO_ID = "audio_id";
    private static final String AUDIO_NAME = "audio_name";
    private static final String AUDIO_TITLE = "audio_title";
    private static final String AUDIO_DURATION = "audio_duration";
    private static final String AUDIO_IMAGE = "audio_image";
    private static final String AUDIO_PATH = "audio_path";

    private static final String CREATE_AUDIO = "create table " + TABLE_AUDIO
            + " (" + ID + " integer primary key autoincrement, "
            + AUDIO_ID + " text,"
            + AUDIO_NAME + " text,"
            + AUDIO_TITLE + " text,"
            + AUDIO_DURATION + " text,"
            + AUDIO_IMAGE + " blob,"
            + AUDIO_PATH + " text);";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    private static DatabaseHelper instance;

    public static synchronized DatabaseHelper getInstance(Context context) {
        instance = new DatabaseHelper(context);
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_AUDIO);

    }

    public void addAudio(AudioOffline audioOffline) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(AUDIO_ID, audioOffline.audio_id);
        values.put(AUDIO_NAME, audioOffline.audio_name);
        values.put(AUDIO_TITLE, audioOffline.audio_title);
        values.put(AUDIO_DURATION, audioOffline.audio_duration);
        values.put(AUDIO_IMAGE, audioOffline.audio_image);
        values.put(AUDIO_PATH, audioOffline.audio_path);

        db.insert(TABLE_AUDIO, null, values);
        db.close();

    }


    public AudioOffline getAudioItem(String id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_AUDIO + " WHERE audio_id = '" + id + "'", null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        AudioOffline dateItem = new AudioOffline();
        dateItem.setAudio_id(cursor.getString(cursor.getColumnIndex(AUDIO_ID)));
        dateItem.setAudio_name(cursor.getString(cursor.getColumnIndex(AUDIO_NAME)));
        dateItem.setAudio_title(cursor.getString(cursor.getColumnIndex(AUDIO_TITLE)));
        dateItem.setAudio_duration(cursor.getString(cursor.getColumnIndex(AUDIO_DURATION)));
        dateItem.setAudio_path(cursor.getString(cursor.getColumnIndex(AUDIO_PATH)));

        return dateItem;
    }

    public boolean isExistById(String audio_id) {
        SQLiteDatabase sqldb = this.getReadableDatabase();
        Cursor cursor = sqldb.rawQuery("SELECT * FROM " + TABLE_AUDIO + " WHERE audio_id = '" + audio_id + "'", null);
        boolean exist = (cursor.getCount() > 0);
        cursor.close();
        sqldb.close();
        return exist;

    }

    public boolean isExistByPath(String audio_path) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_AUDIO + " WHERE audio_path = '" + audio_path + "'", null);
        boolean exist = (cursor.getCount() > 0);
        cursor.close();
        db.close();
        return exist;
    }

    public void deleteAudio(String path) {
        SQLiteDatabase db = this.getWritableDatabase();

        //String id=String.valueOf(ID);
        String where = "audio_path=?";
        int numberOFEntriesDeleted = db.delete(TABLE_AUDIO, where, new String[]{path});
        // Toast.makeText(context, "Number fo Entry Deleted Successfully : "+numberOFEntriesDeleted, Toast.LENGTH_LONG).show();
    }

    public void removeDublicateAudios() {

        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("delete from table_audio where id not in (SELECT MIN(id)FROM table_audio GROUP BY audio_id)");
    }

    public void deleteAll() {
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("delete from " + TABLE_AUDIO);
    }


    public List<AudioOffline> getAllAudios() {
        List<AudioOffline> audioItems = new ArrayList<AudioOffline>();
        String selectQuery = "SELECT  * FROM " + TABLE_AUDIO;

        SQLiteDatabase db = this.getWritableDatabase();
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);

            try {
                if (cursor.moveToFirst()) {
                    do {
                        AudioOffline audioOffline = new AudioOffline();
                        audioOffline.setAudio_id(cursor.getString(cursor.getColumnIndex(AUDIO_ID)));
                        audioOffline.setAudio_name(cursor.getString(cursor.getColumnIndex(AUDIO_NAME)));
                        audioOffline.setAudio_title(cursor.getString(cursor.getColumnIndex(AUDIO_TITLE)));
                        audioOffline.setAudio_duration(cursor.getString(cursor.getColumnIndex(AUDIO_DURATION)));
                        audioOffline.setAudio_image(cursor.getBlob(cursor.getColumnIndex(AUDIO_IMAGE)));
                        audioOffline.setAudio_path(cursor.getString(cursor.getColumnIndex(AUDIO_PATH)));
                        audioItems.add(audioOffline);
                    } while (cursor.moveToNext());
                }
            } finally {

                try {
                    cursor.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } finally {
            try {
                db.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return audioItems;
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if (oldVersion <= 1) {
            db.execSQL("ALTER TABLE " + TABLE_AUDIO + " ADD COLUMN " + AUDIO_IMAGE + " integer");

        }
    }

}
