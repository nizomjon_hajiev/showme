package uz.ishow.android.util;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import uz.ishow.android.event.FeedItemEvent;
import uz.ishow.android.model.Category;
import uz.ishow.android.model.FeedItem;
import uz.ishow.android.model.Nomination;
import uz.ishow.android.model.Profile;
import uz.ishow.android.model.User;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 22.01.2016 16:29.
 */
public class CacheUtils {

    private static final String CURRENT_USER_JSON = "current_user.json";
    private static final String CATEGORIES_JSON = "categories.json";
    private static final String MY_TIMELINE_JSON = "my_timeline.json";
    private static final String NEWS_TIMELINE_JSON = "news_timeline.json";
    private static final String MY_PROFILE_TIMELINE_JSON = "my_profile_timeline.json";
    private static final String SUGGESTIONS_JSON = "suggestions.json";
    private static final String NOMINATIONS_JSON = "nominations.json";
    private static final String MY_FOLLOWERS_JSON = "my_followers.json";

    private Context context;
    private Gson gson;

    private User currentUser;

    private List<FeedItem> mMyTimeline;
    private List<FeedItem> mNewsTimeline;
    private List<FeedItem> mMyProfileTimeline;
    private List<Nomination> mNominations;
    private List<Category> mCategories;
    private List<Profile> mMyFlowwers;

    public CacheUtils(Context context, Gson gson) {
        this.context = context;
        this.gson = gson;
    }

    public void saveUser(User user) {
        try {
            currentUser = user;
            String userJson = gson.toJson(user);
            FileUtils.stringToFile(context, userJson, CURRENT_USER_JSON);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public User getUser() {
        if (currentUser == null) {
            try {
                String json = FileUtils.fileToString(context, CURRENT_USER_JSON);
                if (!TextUtils.isEmpty(json)) {
                    currentUser = gson.fromJson(json, User.class);
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
        return currentUser;
    }

    public void saveMyTimeline(List<FeedItem> items) {
        try {
            if (items.size() > 32) {
                items = items.subList(0, 32);
            }
            mMyTimeline = items;
            FileUtils.stringToFile(context, gson.toJson(mMyTimeline), MY_TIMELINE_JSON);
        } catch (Throwable e) {
            //
        }
    }

    public List<FeedItem> getMyTimeline() {
        if (mMyTimeline == null) {
            mMyTimeline = getFeedItemList(MY_TIMELINE_JSON);
        }
        return mMyTimeline;
    }

    public void saveNewsTimeline(List<FeedItem> items) {
        try {
            if (items.size() > 32) {
                items = items.subList(0, 32);
            }
            mNewsTimeline = items;
            FileUtils.stringToFile(context, gson.toJson(items), NEWS_TIMELINE_JSON);
        } catch (Throwable e) {
            //
        }
    }

    public void saveMyFollowers(List<Profile> items) {

        try {
            mMyFlowwers = items;
            FileUtils.stringToFile(context, gson.toJson(items), MY_FOLLOWERS_JSON);
        } catch (Throwable e) {

        }
    }

    public List<Profile> getMyFollowers() {
        if (mMyFlowwers == null) {
            try {
                String json = FileUtils.fileToString(context, MY_FOLLOWERS_JSON);
                if (!TextUtils.isEmpty(json)) {
                    Type type = new TypeToken<ArrayList<Profile>>() {
                    }.getType();
                    mMyFlowwers = gson.fromJson(json, type);
                }

            } catch (Throwable e) {

            }
        }
        return mMyFlowwers;
    }


    public List<FeedItem> getNewsTimeline() {
        if (mNewsTimeline == null) {
            mNewsTimeline = getFeedItemList(NEWS_TIMELINE_JSON);
        }
        return mNewsTimeline;
    }

    public void saveOwnProfileTimeline(List<FeedItem> items) {
        try {
            if (items.size() > 32) {
                items = items.subList(0, 32);
            }
            mMyProfileTimeline = items;
            FileUtils.stringToFile(context, gson.toJson(items), MY_PROFILE_TIMELINE_JSON);
        } catch (Throwable e) {
            //
        }
    }

    public List<FeedItem> getOwnProfileTimeline() {
        if (mMyProfileTimeline == null) {
            mMyProfileTimeline = getFeedItemList(MY_PROFILE_TIMELINE_JSON);
        }
        return mMyProfileTimeline;
    }

    private List<FeedItem> getFeedItemList(String name) {
        try {
            String json = FileUtils.fileToString(context, name);
            if (!TextUtils.isEmpty(json)) {
                Type type = new TypeToken<ArrayList<FeedItem>>() {
                }.getType();
                return gson.fromJson(json, type);
            }
        } catch (Throwable e) {
            //
        }
        return null;
    }

    public void saveSuggestions(List<Profile> items) {
        try {
            if (items.size() > 32) {
                items = items.subList(0, 32);
            }
            FileUtils.stringToFile(context, gson.toJson(items), SUGGESTIONS_JSON);
        } catch (Throwable e) {
            //
        }
    }

    public List<Profile> getSuggestions() {
        try {
            String json = FileUtils.fileToString(context, SUGGESTIONS_JSON);
            if (!TextUtils.isEmpty(json)) {
                Type type = new TypeToken<ArrayList<Profile>>() {
                }.getType();
                return gson.fromJson(json, type);
            }
        } catch (Throwable e) {
            //
        }

        return null;
    }

    public void saveCategories(List<Category> categories) {
        try {
            if (categories.size() > 32) {
                categories = categories.subList(0, 32);
            }
            mCategories = categories;
            FileUtils.stringToFile(context, gson.toJson(mCategories), CATEGORIES_JSON);
        } catch (Throwable e) {
            //
        }
    }

    public List<Category> getCategories() {
        if (mCategories == null) {
            try {
                String json = FileUtils.fileToString(context, CATEGORIES_JSON);
                if (!TextUtils.isEmpty(json)) {
                    Type type = new TypeToken<ArrayList<Category>>() {
                    }.getType();
                    mCategories = gson.fromJson(json, type);
                }
            } catch (Throwable e) {
            }
        }

        return mCategories;
    }

    public List<Nomination> getNominations() {
        if (mNominations == null) {
            try {
                String json = FileUtils.fileToString(context, NOMINATIONS_JSON);
                if (!TextUtils.isEmpty(json)) {
                    Type type = new TypeToken<ArrayList<Nomination>>() {
                    }.getType();
                    mNominations = gson.fromJson(json, type);
                }
            } catch (Throwable e) {
                //
            }
        }
        return mNominations;
    }

    public void saveNominations(List<Nomination> items) {
        mNominations = items;
        try {
            if (items.size() > 32) {
                items = items.subList(0, 32);
            }
            FileUtils.stringToFile(context, gson.toJson(mNominations), NOMINATIONS_JSON);
        } catch (Throwable e) {
            //
        }
    }

    public void onEvent(FeedItemEvent event) {
        if (changeTimelineCache(event, getMyTimeline())) {
            saveMyTimeline(getMyTimeline());
        }

        if (changeTimelineCache(event, getNewsTimeline())) {
            saveNewsTimeline(getNewsTimeline());
        }

        if (changeTimelineCache(event, getOwnProfileTimeline())) {
            saveOwnProfileTimeline(getOwnProfileTimeline());
        }

        if (changeCategoryCache(event, getCategories())) {
            saveCategories(getCategories());
        }
//        if (changeProfileCache(event, getMyFollowers())) {
//            saveMyFollowers(getMyFollowers());
//        }
        FeedItemEvent.Type type = event.getType();

        if (getMyFollowers() != null) {
            for (Profile user : getMyFollowers()) {
                if (user != null && user.getId() != null && !user.getId().equals(event.getProfileId())) {
                    if (type == FeedItemEvent.Type.FOLLOWING) {
                        List<Profile> mFollowers = getMyFollowers();
                        mFollowers.add(event.getUser());
                        saveMyFollowers(mFollowers);
                    } else if (type == FeedItemEvent.Type.DISFOLLOWING) {
                        List<Profile> mFollowers = getMyFollowers();
                        mFollowers.remove(event.getUser());
                        saveMyFollowers(mFollowers);
                    }
                }
            }
        }
    }

    public boolean changeTimelineCache(FeedItemEvent event, List<FeedItem> timeline) {
        boolean hasChanges = false;
        FeedItemEvent.Type type = event.getType();
        FeedItem itemForDelete = null;
        if (timeline != null) {
            for (FeedItem item : timeline) {
                if (item == null) {
                    continue;
                }
                if (type == FeedItemEvent.Type.COMMENT) {
                    if (item.postIdEquals(event.getPostId())) {
                        item.getPost().setCommentsCount(event.getCommentCount());
                        hasChanges = true;
                    }
                } else if (type == FeedItemEvent.Type.LIKE) {
                    if (item.postIdEquals(event.getPostId())) {
                        item.getPost().setLikesCount(event.getLikeCount());
                        hasChanges = true;
                    }
                } else if (type == FeedItemEvent.Type.FOLLOWING) {
                    if (item.postAuthorIdEquals(event.getProfileId())) {
                        item.getPost().getAuthor().setIsFollowing(event.isFollowing());
                        item.getPost().getAuthor().setFollowersCount(event.getFollowersCount());
                        hasChanges = true;
                    }
                } else if (type == FeedItemEvent.Type.USER_DATA_CHANGED) {
                    if (event.getUser() != null && item.postAuthorIdEquals(event.getUser().getId())) {
                        Profile author = item.getPost().getAuthor();
                        author.setName(event.getUser().getName());
                        author.setMiniImage(event.getUser().getMiniImage());
                        author.setGeneralImage(event.getUser().getGeneralImage());
                        author.setBackgroundImage(event.getUser().getBackgroundImage());
                        hasChanges = true;
                    }
                } else if (type == FeedItemEvent.Type.POST_DELETED) {
                    if (event.getPost() != null && item.postIdEquals(event.getPostId())) {
                        itemForDelete = item;
                        hasChanges = true;
                    }
                }
            }

            if (itemForDelete != null) {
                timeline.remove(itemForDelete);
            }
        }

        return hasChanges;
    }

    public boolean changeCategoryCache(FeedItemEvent event, List<Category> categories) {
        boolean hasChanges = false;
        FeedItemEvent.Type type = event.getType();
        for (Category category : categories) {
            if (category != null && category.getUsers() != null) {
                for (Profile user : category.getUsers()) {
                    if (user != null && user.getId() != null && user.getId().equals(event.getProfileId())) {
                        if (type == FeedItemEvent.Type.FOLLOWING) {
                            user.setIsFollowing(event.isFollowing());
                            user.setFollowersCount(event.getFollowersCount());
                            hasChanges = true;
                        } else if (type == FeedItemEvent.Type.USER_DATA_CHANGED) {
                            if (event.getUser() != null) {
                                user.setName(event.getUser().getName());
                                user.setMiniImage(event.getUser().getMiniImage());
                                user.setGeneralImage(event.getUser().getGeneralImage());
                                user.setBackgroundImage(event.getUser().getBackgroundImage());
                                hasChanges = true;
                            }
                        }
                    }
                }
            }
        }

        return hasChanges;
    }

    public boolean changeProfileCache(FeedItemEvent event, List<Profile> profiles) {
        boolean hasChanges = false;
        FeedItemEvent.Type type = event.getType();
        if (profiles != null) {
            for (Profile user : profiles) {
                if (user != null && user.getId() != null && !user.getId().equals(event.getProfileId())) {
                    if (type == FeedItemEvent.Type.FOLLOWING) {
                        List<Profile> mFollowers = getMyFollowers();
                        mFollowers.add(event.getUser());
                        saveMyFollowers(mFollowers);
                        hasChanges = true;
                    }
                }
            }
        }
        return hasChanges;
    }


    public void clear() {
        currentUser = null;
        mMyTimeline = null;
        mNewsTimeline = null;
        mMyProfileTimeline = null;
        FileUtils.delete(context,
                MY_TIMELINE_JSON,
                MY_PROFILE_TIMELINE_JSON,
                NEWS_TIMELINE_JSON,
                SUGGESTIONS_JSON,
                CURRENT_USER_JSON,
                CATEGORIES_JSON,
                MY_FOLLOWERS_JSON,
                NOMINATIONS_JSON);
    }
}
