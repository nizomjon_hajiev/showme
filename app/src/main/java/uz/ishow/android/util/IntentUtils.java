package uz.ishow.android.util;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 21.09.2015 8:11.
 */
public class IntentUtils {

    public static void openDialer(String phone, Activity context) {
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.fromParts("tel", phone, null));
            context.startActivity(intent);
        } catch (Exception e) {
            //
        }
    }

    public static void openSMS(String phone, Activity context) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.fromParts("sms", phone, null));
            context.startActivity(intent);
        } catch (Exception e) {
            //
        }
    }

    public static void mailTo(String email, Activity context) {
        try {
            Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", email, null));
            context.startActivity(intent);
        } catch (Exception e) {
            //
        }
    }

    public static void openTwitterProfile(String twitterProfile, Activity context) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("twitter://user?screen_name=" + twitterProfile)));
        } catch (Exception e) {
            // open in browser
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://twitter.com/#!/" + twitterProfile)));
        }
    }

    public static void addToContacts(String name, String phone, String email, Activity context) {
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType(ContactsContract.Contacts.CONTENT_TYPE);

        intent.putExtra(ContactsContract.Intents.Insert.NAME, name);
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, phone);
        intent.putExtra(ContactsContract.Intents.Insert.EMAIL, email);

        try {
            context.startActivity(intent);
        } catch (Exception e) {
            //
        }
    }

    public static void openWeb(String url, Activity context) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            //
        }
    }
}
