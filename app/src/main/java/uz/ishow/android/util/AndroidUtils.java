package uz.ishow.android.util;

import android.os.Build;

/**
 * Created by User on 28.04.2016.
 */
public final class AndroidUtils {
    public static boolean isLollipop() {
        return android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }
}