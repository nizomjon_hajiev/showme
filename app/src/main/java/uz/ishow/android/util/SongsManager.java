package uz.ishow.android.util;

/**
 * Created by User on 20.03.2016.
 */

import android.content.Context;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import uz.ishow.android.model.Audio;

public class SongsManager {
    // SDCard Path
    Context context;
    private ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();
    private ArrayList<String> mPaths = new ArrayList<>();
    private ArrayList<Audio> mAudios = new ArrayList<>();
//    final String MEDIA_PATH =context.getExternalFilesDir();

    // Constructor
    public SongsManager(Context context) {

        this.context = context;
    }


    public ArrayList<HashMap<String, String>> getPlayList() {
        File home = new File(context.getExternalFilesDir(null), "audio");
        HashSet<HashMap<String, String>> maps = new HashSet<>();
        for (File file : home.listFiles(new FileExtensionFilter())) {
            if (file != null && file.length() > 0) {
                HashMap<String, String> song = new HashMap<String, String>();
                song.put("songPath", file.getPath());
                songsList.add(song);
                mPaths.add(file.getPath());
            }
//            maps.addAll(songsList);
//            songsList.clear();
//            songsList.addAll(maps);
        }
        return songsList;
    }

    public ArrayList<String> getPaths() {
        File home = new File(context.getExternalFilesDir(null), "audio");
        if (home.listFiles(new FileExtensionFilter()).length > 0) {
            for (File file : home.listFiles(new FileExtensionFilter())) {
                if (file != null && file.length() > 0) {
                    mPaths.add(file.getPath());
                }
            }

        }
        return mPaths;
    }

    public ArrayList<String> getTitles() {
        File home = new File(context.getExternalFilesDir(null), "audio");

//        if (home.listFiles()) {
//            for (File file : home.listFiles(new FileExtensionFilter())) {
//                String[] split = file.getName().split("&&&");
//                mPaths.add(split[1]);
//            }
//        }
        return mPaths;
    }

    class FileExtensionFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            return (name.endsWith(".mp3") || name.endsWith(".MP3") || name.endsWith(""));
        }
    }

    public String milliSecondsToTimer(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }

    public int getProgressPercentage(long currentDuration, long totalDuration) {
        Double percentage = (double) 0;

        long currentSeconds = (int) (currentDuration / 1000);
        long totalSeconds = (int) (totalDuration / 1000);

        // calculating percentage
        percentage = (((double) currentSeconds) / totalSeconds) * 100;

        // return percentage
        return percentage.intValue();
    }

}