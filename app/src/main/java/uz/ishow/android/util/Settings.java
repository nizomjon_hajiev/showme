package uz.ishow.android.util;

import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import uz.ishow.android.model.PushNotification;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 19.06.2015 14:31.
 */

@Singleton
public class Settings {

    private SharedPreferences preferences;

    public Settings(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    public void saveAuthToken(String token) {
        preferences.edit().putString("TOKEN", token).apply();
    }

    public String getAuthToken() {
        return preferences.getString("TOKEN", null);
    }

    public boolean isAuthorized() {
        return getAuthToken() != null;
    }

    public void setLanguage(String lang) {
        preferences.edit().putString("LANGUAGE", lang).apply();
    }

    public String getLanguage() {
        return preferences.getString("LANGUAGE", "uz");
    }

    public boolean isLanguageUz() {
        return "uz".equals(getLanguage());
    }

    public boolean showIntro() {
        return preferences.getString("LANGUAGE", null) == null;
    }

    public void clearWithoutLanguage() {
        String lang = preferences.getString("LANGUAGE", "uz");
        preferences.edit().clear().putString("LANGUAGE", lang).apply();
    }


    public void setPushAsShowed(PushNotification push) {
        getPushHashes().add(push.hashCode());
        if (getPushHashes().size() > 32) {
            getPushHashes().remove(0);
        }

        savePushHashes();
    }
    public boolean isPushShowed(PushNotification push) {
        return getPushHashes().contains(push.hashCode());
    }

    private List<Integer> pushHashes;
    private List<Integer> getPushHashes() {
        if (pushHashes == null) {
            pushHashes = new ArrayList<>(32);
            String ser = preferences.getString("PUSH_HASHES", "");
            String[] hashes = ser.split("|");
            for (String hash : hashes) {
                try {
                    pushHashes.add(Integer.valueOf(hash));
                } catch (NumberFormatException e) {
                }
            }
        }

        return pushHashes;
    }

    private void savePushHashes() {
        StringBuilder sb = new StringBuilder();
        for (Integer p : pushHashes) {
            if (sb.length() > 0) {
                sb.append("|");
            }

            sb.append(p);
        }

        preferences.edit().putString("PUSH_HASHES", sb.toString()).apply();
    }

    public void saveAppVersionCode(int versionCode) {
        preferences.edit().putInt("VERSION_CODE", versionCode).apply();
    }

    public int getAppVersionCode() {
        return preferences.getInt("VERSION_CODE", 0);
    }

}
