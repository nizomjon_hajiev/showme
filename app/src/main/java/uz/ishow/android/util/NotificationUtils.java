package uz.ishow.android.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import uz.ishow.android.R;
import uz.ishow.android.model.PushNotification;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 16.01.2016 15:44.
 */
public class NotificationUtils {

    public static final String OPEN_POST_FROM_NOTIFICATION = "uz.ishow.android.OPEN_POST_FROM_NOTIFICATION";

    public static void show(PushNotification push, Context context) {

        if (TextUtils.isEmpty(push.getTitle())) {
            return;
        }

        Intent localBroadcast = new Intent(OPEN_POST_FROM_NOTIFICATION);
        localBroadcast.putExtra("POST_ID", push.getPostId());


        NotificationCompat.Builder nb = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_stat_notification)
                .setAutoCancel(true)
                .setTicker(push.getTitle())
                .setContentText(push.getTitle())
                .setContentIntent(PendingIntent.getBroadcast(context, push.hashCode(),
                        localBroadcast, PendingIntent.FLAG_UPDATE_CURRENT))
                .setWhen(System.currentTimeMillis())
                .setContentTitle(context.getString(R.string.app_name))
                .setDefaults(Notification.DEFAULT_ALL);

        Notification notification = nb.build();

        NotificationManager mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(push.hashCode(), notification);
    }
}
