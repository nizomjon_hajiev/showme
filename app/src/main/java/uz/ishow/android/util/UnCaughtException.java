package uz.ishow.android.util;

/**
 * Created by User on 04.05.2016.
 */

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import android.util.Log;
import android.view.View;

import java.lang.Thread.UncaughtExceptionHandler;

import uz.ishow.android.R;

public class UnCaughtException implements UncaughtExceptionHandler {


    private Context context;

    public UnCaughtException(Context ctx) {
        context = ctx;
    }

    public void uncaughtException(Thread t, Throwable e) {
        try {

            sendErrorMail();
        } catch (Throwable ignore) {
            Log.e(UnCaughtException.class.getName(),
                    "Error while sending error e-mail", ignore);
        }
    }

    /**
     * This method for call alert dialog when application crashed!
     */

    public void sendErrorMail() {
        final Dialog builder = new Dialog(context, R.style.AppAlertDialogTheme);
        new Thread() {
            @Override
            public void run() {
                Looper.prepare();
                builder.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
                builder.setContentView(R.layout.custom_error_dialog);


                builder.findViewById(R.id.close_btn).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(intent);
                    }
                });

//                builder.setPositiveButton("Ok",
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog,
//                                                int which) {
////                                System.exit(0);
//                                Intent intent = new Intent(Intent.ACTION_MAIN);
//                                intent.addCategory(Intent.CATEGORY_HOME);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                context.startActivity(intent);
//
//                            }
//                        });
//
//                builder.setMessage("Oops,Your application has crashed");
                builder.show();
                Looper.loop();
            }
        }.start();
//        ErrorAlertDialog errorAlertDialog = new ErrorAlertDialog(context);
//        errorAlertDialog.show();
    }
}