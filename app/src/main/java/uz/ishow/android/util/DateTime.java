package uz.ishow.android.util;

import android.content.res.Resources;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import uz.ishow.android.R;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 18.06.2015 17:05.
 */
public class DateTime {

    public static String timesAgo(Date date, Resources res) {
        if (date == null) {
            return null;
        }

        Calendar now = Calendar.getInstance();
        Calendar dateCal = Calendar.getInstance();
        dateCal.setTime(date);

        long ms = (now.getTimeInMillis() - dateCal.getTimeInMillis());
        long sec = ms / 1000;
        if (sec < 60) {
            return res.getString(R.string.just_now);
        }

        int min = (int) sec / 60;
        if (min == 1) {
            return res.getString(R.string.minute_ago);
        }

        if (min < 60) {
            return res.getQuantityString(R.plurals.minutes_ago, min, min);
        }

        Calendar startOfDay = Calendar.getInstance();
        startOfDay.setTime(now.getTime());
        startOfDay.set(Calendar.MILLISECOND, 0);
        startOfDay.set(Calendar.SECOND, 0);
        startOfDay.set(Calendar.MINUTE, 0);
        startOfDay.set(Calendar.HOUR_OF_DAY, 0);

        String format = null;
        boolean monthsRequired = false;
        if (dateCal.after(startOfDay)) {
            format = res.getString(R.string.today_hh_mm);
        } else {
            long mss = startOfDay.getTimeInMillis() - dateCal.getTimeInMillis();
            if (mss < 24 * 3600000) {
                format = res.getString(R.string.yesterday_hh_mm);
            } else {
                int nowYear = now.get(Calendar.YEAR);
                int dateYear = dateCal.get(Calendar.YEAR);
                monthsRequired = true;
                if (nowYear == dateYear) {
                    format = res.getString(R.string.date_month_hh_mm);
                } else {
                    format = res.getString(R.string.date_month_year_hh_mm);
                }
            }
        }

        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            if (monthsRequired) {
                DateFormatSymbols dfs = new DateFormatSymbols();
                dfs.setMonths(res.getStringArray(R.array.date_time_months));
                sdf.setDateFormatSymbols(dfs);
            }
            return sdf.format(date);
        } catch (Resources.NotFoundException e) {
            // ignore
        }
        return null;
    }
}
