package uz.ishow.android.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 30.09.2015 11:41.
 */
public class Utils {

    public static int getVersionCode(Context context) {
        int version = 1;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return version;
    }

    public static String getVersionName(Context context) {
        String version = "";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return version;
    }

    public static String sanitize(String text) {
        if (text == null) {
            return null;
        }

        while(text.contains("\n\n")) {
            text = text.replace("\n\n", "\n");
        }

        return text;
    }
}
