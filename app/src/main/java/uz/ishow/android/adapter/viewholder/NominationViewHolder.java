package uz.ishow.android.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uz.ishow.android.R;
import uz.ishow.android.adapter.PollNominationAdapter;
import uz.ishow.android.model.Nomination;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 21.02.2016 14:32.
 */
public class NominationViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.icon)
    View icon;

    @Bind(R.id.title)
    TextView title;

    @Bind(R.id.description)
    TextView description;

    @Bind(R.id.label_continued)
    View labelContinued;

    @Bind(R.id.label_finished)
    View labelFinished;

    private boolean isLangRu;
    private Nomination mNomination;

    private PollNominationAdapter.ItemClickListener mItemClickListener;

    public NominationViewHolder(View itemView, PollNominationAdapter.ItemClickListener itemClickListener,
                                boolean isLangRu) {
        super(itemView);
        this.mItemClickListener = itemClickListener;
        ButterKnife.bind(this, itemView);
        this.isLangRu = isLangRu;
    }

    public void fill(Nomination n) {
        this.mNomination = n;
        if (n.isTypeAudio()) {
            icon.setBackgroundResource(R.drawable.ic_musictype);
        } else if (n.isTypeVideo()) {
            icon.setBackgroundResource(R.drawable.ic_movietype);
        } else if (n.isTypePhoto()) {
            icon.setBackgroundResource(R.drawable.ic_phototype);
        } else {
            icon.setBackgroundResource(R.drawable.ic_startype);
        }

        title.setText(isLangRu ? n.getNameRu() : n.getName());
        description.setText(isLangRu ? n.getDescriptionRU() : n.getDescription());

        labelContinued.setVisibility(n.isActive() ? View.VISIBLE : View.GONE);
        labelFinished.setVisibility(n.isActive() ? View.GONE : View.VISIBLE);
    }

    @OnClick(R.id.item)
    void onItemClick() {
        if (mItemClickListener != null && mNomination != null) {
            mItemClickListener.onItemClick(mNomination);
        }
    }

}
