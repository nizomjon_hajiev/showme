package uz.ishow.android.adapter.viewholder;

import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.Bind;
import butterknife.OnClick;
import uz.ishow.android.R;
import uz.ishow.android.listener.FeedItemListener;
import uz.ishow.android.model.FeedItem;
import uz.ishow.android.model.Post;
import uz.ishow.android.util.DateTime;

/**
 * Created by User on 19.03.2016.
 */
public class HeaderViewHolder extends FeedItemViewHolder {

    @Bind(R.id.profile_image)
    ImageView profileImage;
    @Bind(R.id.name)
    TextView nameT;
    @Bind(R.id.times_ago)
    public TextView timesAgo;
//    @Bind(R.id.header_holder)
//    LinearLayout headerHolder;
//    private boolean isCallAndRefresh;

    public HeaderViewHolder(View itemView, FeedItemListener feedItemListener, boolean openProfileDisabled) {
        super(itemView, feedItemListener);

    }

    @Override
    public void fill(FeedItem feedItem) {
        super.fill(feedItem);
        if (feedItem != null) {
            Post post = feedItem.getPost();
            if (post != null) {
                nameT.setText(post.getAuthor().getName());
                timesAgo.setText(DateTime.timesAgo(post.getPublishedTime(), timesAgo.getResources()));
                ImageLoader.getInstance().displayImage(post.getAuthor().getMiniImage(), profileImage);
            }

        }

    }

    @Nullable
    @OnClick(R.id.header_holder)
    void clickHeader() {
        if (getFeedItemListener() != null && getFeedItem() != null) {
            getFeedItemListener().onHeaderClick(getFeedItem());
        }
    }
}
