package uz.ishow.android.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uz.ishow.android.R;
import uz.ishow.android.model.Profile;
import uz.ishow.android.model.SuggestionHeader;

/**
 * Created by User on 18.03.2016.
 */
public class StarsAdapter extends RecyclerView.Adapter<StarsAdapter.ViewHolder> {

    private List<Profile> mUsers;
    private List<Profile> mFamousUsers;
    private Context context;

    public static final int HORIZONTAL_SCROLL_VIEW = 2;
    public static final int HORIZONTAL_VIEW_PAGER = 3;
    public static final int DEFAULT = 0;

    private int placeholderColor;
    private OnUserClickListener onUserClickListener;


    public StarsAdapter(Context context, List<Profile> mUsers, List<Profile> mFamousUsers, OnUserClickListener onUserClickListener) {
        this.mUsers = mUsers;
        this.mFamousUsers = mFamousUsers;
        this.context = context;
        this.onUserClickListener = onUserClickListener;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        placeholderColor = recyclerView.getResources().getColor(R.color.image_placeholder);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HORIZONTAL_VIEW_PAGER;
        } else {

            return DEFAULT;
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {

            case HORIZONTAL_VIEW_PAGER:
                View view = LayoutInflater.from(context).inflate(R.layout.view_pager, parent, false);
                return new ViewHolder_ViewPager(view);
            case HORIZONTAL_SCROLL_VIEW:
                View view1 = LayoutInflater.from(context).inflate(R.layout.section_list_item, parent, false);
                return new ViewHolder_Horizontal(view1);
            default:
                View view3 = LayoutInflater.from(context).inflate(R.layout.user_grid_item, parent, false);
                return new ViewHolder2(view3);
        }
    }

    public Context getContext() {
        return context;
    }

    public Profile getItem(int position) {
        if (mUsers != null && position >= 0 && position < getItemCount()) {
            return mUsers.get(position);
        }
        return null;
    }

    public void followingChanged(String profileId, int followersCount, boolean isFollowing) {
        for (int i = 0; i < getItemCount(); i++) {
            Profile p = getItem(i);
            if (p instanceof SuggestionHeader) {
                continue;
            }

            if (p.getId().equals(profileId) && p.getIsFollowing() ^ isFollowing) {
                p.setIsFollowing(isFollowing);
                p.setFollowersCount(followersCount);
                notifyItemChanged(i);
            }
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {


        if (holder instanceof ViewHolder_ViewPager) {
            ViewPagerAdapter adapter = new ViewPagerAdapter(mFamousUsers, context);
            ((ViewHolder_ViewPager) holder).viewPager.setAdapter(adapter);
            ((ViewHolder_ViewPager) holder).mViews.get(0).setSelected(true);
            ((ViewHolder_ViewPager) holder).mViews.get(1).setSelected(false);
            ((ViewHolder_ViewPager) holder).mViews.get(2).setSelected(false);

            if (mFamousUsers.size() > 3 && mFamousUsers.size() <= 6) {
                adapter.setPageLimit(2);
                adapter.notifyDataSetChanged();
                ((ViewHolder_ViewPager) holder).mViews.get(2).setVisibility(View.GONE);
            } else if (mFamousUsers.size() > 0 && mFamousUsers.size() <= 3) {
                adapter.setPageLimit(1);
                adapter.notifyDataSetChanged();
                ((ViewHolder_ViewPager) holder).mViews.get(2).setVisibility(View.GONE);
                ((ViewHolder_ViewPager) holder).mViews.get(1).setVisibility(View.GONE);
            } else if (mFamousUsers.size() == 0) {
                ((ViewHolder_ViewPager) holder).dotsContainer.setVisibility(View.GONE);
                ((ViewHolder_ViewPager) holder).viewPager.setVisibility(View.GONE);
                ((ViewHolder_ViewPager) holder).famousUsers.setVisibility(View.VISIBLE);

            }


            ((ViewHolder_ViewPager) holder).viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    if (position == 0) {
                        ((ViewHolder_ViewPager) holder).mViews.get(0).setSelected(true);
                        ((ViewHolder_ViewPager) holder).mViews.get(1).setSelected(false);
                        ((ViewHolder_ViewPager) holder).mViews.get(2).setSelected(false);
                    } else if (position == 1) {
                        ((ViewHolder_ViewPager) holder).mViews.get(0).setSelected(false);
                        ((ViewHolder_ViewPager) holder).mViews.get(1).setSelected(true);
                        ((ViewHolder_ViewPager) holder).mViews.get(2).setSelected(false);
                    } else if (position == 2) {
                        ((ViewHolder_ViewPager) holder).mViews.get(0).setSelected(false);
                        ((ViewHolder_ViewPager) holder).mViews.get(1).setSelected(false);
                        ((ViewHolder_ViewPager) holder).mViews.get(2).setSelected(true);
                    }

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });


        } else if (holder instanceof ViewHolder2) {
            Profile user = getItem(position - 1);

            //reset image
            ((ViewHolder2) holder).profileImage.setImageDrawable(new ColorDrawable(placeholderColor));
            if (user.getMiniImage() == null)
                if (((ViewHolder2) holder).profileImage != null) {
                    ((ViewHolder2) holder).profileImage.setBackground(context.getResources().getDrawable(R.drawable.circle_image_view));
                }
            ImageLoader.getInstance().displayImage(user.getMiniImage(), ((ViewHolder2) holder).profileImage);

            ((ViewHolder2) holder).followedSticker.setVisibility(user.getIsFollowing() ? View.VISIBLE : View.GONE);
            ((ViewHolder2) holder).name.setText(user.getName());

            int sb = user.getFollowersCount();
            String text = sb == 0 ? getContext().getString(R.string.no_subscribers) :
                    getContext().getResources().getQuantityString(R.plurals.subscribers, sb, sb);
            ((ViewHolder2) holder).subscribers.setText(text);
        }

    }


    @Override
    public int getItemCount() {
        return mUsers != null ? mUsers.size() + 1 : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    public class ViewHolder2 extends ViewHolder {
        @Nullable
        @Bind(R.id.profile_image)
        ImageView profileImage;

        @Nullable
        @Bind(R.id.followedSticker)
        View followedSticker;

        @Nullable
        @Bind(R.id.name)
        TextView name;

        @Nullable
        @Bind(R.id.subscribers)
        TextView subscribers;


        public ViewHolder2(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Nullable
        @OnClick(R.id.item_root)
        void onItemClick(View v) {
            if (onUserClickListener != null) {
                onUserClickListener.onUserClick(getItem(getAdapterPosition() - 1));
            }
        }
    }

    public class ViewHolder_Horizontal extends ViewHolder {

        @Nullable
        @Bind(R.id.recycler_view)
        RecyclerView recyclerView;

        public ViewHolder_Horizontal(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class ViewHolder_ViewPager extends ViewHolder {


        @Nullable
        @Bind(R.id.view_pager)
        ViewPager viewPager;

        @Nullable
        @Bind({R.id.slider_1, R.id.slider_2, R.id.slider_3})
        List<View> mViews;

        @Nullable
        @Bind(R.id.no_famousUsers)
        TextView famousUsers;

        @Nullable
        @Bind(R.id.dots_container)
        LinearLayout dotsContainer;

        public ViewHolder_ViewPager(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        @Nullable
        @OnClick({R.id.slider_1, R.id.slider_2, R.id.slider_3})
        void clickViews(View view) {
            switch (view.getId()) {
                case R.id.slider_1:
                    viewPager.setCurrentItem(0);
                    break;
                case R.id.slider_2:
                    viewPager.setCurrentItem(1);
                    break;
                case R.id.slider_3:
                    viewPager.setCurrentItem(2);
                    break;
            }
        }


    }


    public interface OnUserClickListener {
        void onUserClick(Profile user);
    }

    private UsersGridAdapter.OnUserClickListener onUserClickListener1 = new UsersGridAdapter.OnUserClickListener() {
        @Override
        public void onUserClick(Profile user) {
            onUserClickListener.onUserClick(user);
        }
    };


}
