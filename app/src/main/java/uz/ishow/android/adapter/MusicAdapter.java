package uz.ishow.android.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uz.ishow.android.R;
import uz.ishow.android.event.AudioEvent;
import uz.ishow.android.model.AudioOffline;
import uz.ishow.android.model.Post;
import uz.ishow.android.util.DbBitmapUtility;
import uz.ishow.android.util.MediaCenter;

/**
 * Created by User on 20.03.2016.
 */
public class MusicAdapter extends RecyclerView.Adapter<MusicAdapter.ViewHolder> {


    private List<AudioOffline> mAudios;
    private Context context;
    OnItemClickListener onItemClickListener;
    private List<ViewHolder> mViewHolders = new ArrayList<>();
    int progress;
    public static final int FOOTER = 1;


    public MusicAdapter(List<AudioOffline> mAudios, Context context, OnItemClickListener onItemClickListener) {
        this.mAudios = mAudios;
        this.context = context;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case FOOTER:
                View view = LayoutInflater.from(context).inflate(R.layout.footer_music_adapter, parent, false);
                return new FooterViewHolder(view);
            default:
                View view1 = LayoutInflater.from(context).inflate(R.layout.new_offline_music_player, parent, false);

                MusicViewHolder viewHolder = new MusicViewHolder(view1);
                mViewHolders.add(viewHolder);
                return viewHolder;
        }


    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (holder instanceof MusicViewHolder) {

            final AudioOffline audio = getAudio(position);
            Post post = new Post();
            post.setAudioOffline(audio);
            boolean isPlaying = MediaCenter.getInstance().isPlayingDown(post);
            ((MusicViewHolder) holder).buttonPlayPause.setSelected(isPlaying);
            if (isPlaying) {
                ((MusicViewHolder) holder).activeSeekBar();
            } else {
                ((MusicViewHolder) holder).unActiveSeekBar();
            }
            ((MusicViewHolder) holder).audio_title.setText(audio.getAudio_title());
            if (audio.audio_image != null) {
                ((MusicViewHolder) holder).audioCover.setImageBitmap(DbBitmapUtility.getImage(audio.getAudio_image()));
            } else {
                ((MusicViewHolder) holder).audioCover.setImageResource(R.drawable.music_placeholder_new);
            }
            ((MusicViewHolder) holder).audioAuthor.setText(audio.getAudio_name());
            ((MusicViewHolder) holder).audioTime.setText(audio.getAudio_duration());
        }


    }

    @Override
    public int getItemViewType(int position) {
        if (position == mAudios.size()) {
            return FOOTER;
        }
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return mAudios != null ? mAudios.size() + 1 : 0;
    }

    public AudioOffline getAudio(int position) {
        if (mAudios != null && position >= 0 && position < getItemCount()) {
            return mAudios.get(position);
        }
        return null;
    }

    public void deleteItem(int position) {
        mAudios.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mAudios.size());
    }

    public void onEvent(AudioEvent event) {
        switch (event.getType()) {
            case START:
            case RESUME: {
                for (ViewHolder viewHolder : mViewHolders) {
                    if (viewHolder instanceof MusicViewHolder) {

                        AudioOffline audio = getAudio(viewHolder.getAdapterPosition());
                        if (audio != null && audio.equals(event.getAudioPost().getAudioOffline())) {
                            ((MusicViewHolder) viewHolder).buttonPlayPause.setSelected(true);
                            ((MusicViewHolder) viewHolder).setSeekBarAsPlayed(true);
//                        viewHolder.setTime(event.getAudioPost().getAudioOffline().getAudio_duration());

                        }
                    }
                }
                break;
            }

            case PAUSE:
                for (ViewHolder viewHolder : mViewHolders) {
                    if (viewHolder instanceof MusicViewHolder) {
                        AudioOffline audio = getAudio(viewHolder.getAdapterPosition());
                        if (audio != null && audio.equals(event.getAudioPost().getAudioOffline())) {
                            ((MusicViewHolder) viewHolder).setSeekBarAsPlayed(false);
                            ((MusicViewHolder) viewHolder).buttonPlayPause.setSelected(false);
                        }

                    }
                }
                break;


            case STOP:
                for (ViewHolder viewHolder : mViewHolders) {
                    if (viewHolder instanceof MusicViewHolder) {

                        AudioOffline audio = getAudio(viewHolder.getAdapterPosition());
                        if (audio != null && audio.equals(event.getAudioPost().getAudioOffline())) {
                            ((MusicViewHolder) viewHolder).buttonPlayPause.setSelected(false);
                            ((MusicViewHolder) viewHolder).closeSeekBar(true);

                        }
                    }
                }


        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    public class FooterViewHolder extends ViewHolder {

        public FooterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    public class MusicViewHolder extends ViewHolder {

        @Bind(R.id.button_play_pause)
        View buttonPlayPause;

        @Bind(R.id.audio_cover)
        ImageView audioCover;

        @Bind(R.id.audio_author)
        TextView audioAuthor;

        @Bind(R.id.audio_title)
        TextView audio_title;

        @Bind(R.id.seek_bar)
        SeekBar seekBar;

        @Bind(R.id.music_holder)
        LinearLayout musicHolder;

        @Bind(R.id.audio_time)
        TextView audioTime;

        @Bind(R.id.add_to_music)
        View addToMusic;

        @Bind(R.id.delete_music)
        View deleteAudio;

        Handler handler;

        public MusicViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            seekBar.setOnSeekBarChangeListener(seekBarChangeListener);
            handler = new Handler();
            addToMusic.setVisibility(View.GONE);
            deleteAudio.setVisibility(View.VISIBLE);
        }

        @OnClick(R.id.button_play_pause)
        void clickHolder() {
            AudioOffline audio = getAudio(getAdapterPosition());
            if (onItemClickListener != null && audio != null) {
                onItemClickListener.playAudio(audio, getAdapterPosition());
            }
        }

        @OnClick(R.id.delete_music)
        void deleteAudio() {
            AudioOffline audio = getAudio(getAdapterPosition());
            if (onItemClickListener != null && audio != null) {
                onItemClickListener.delete(audio, getAdapterPosition());
            }
        }

        public void setSeekBarAsPlayed(boolean isSeekBar) {
            if (isSeekBar) {
//                layout.setVisibility(View.INVISIBLE);
                seekBar.setVisibility(View.VISIBLE);
                updateProgress();
            }
        }


        public void closeSeekBar(boolean isSeekBar) {
            if (isSeekBar) {
                seekBar.setVisibility(View.GONE);
//                layout.setVisibility(View.VISIBLE);
                handler.removeCallbacks(mUpdateTimeTask);
            }
        }

        private void activeSeekBar() {
            seekBar.setVisibility(View.VISIBLE);
//            layout.setVisibility(View.INVISIBLE);
            removeCallback();
            int position = MediaCenter.getInstance().getCurrentTimePosition();
            int total = MediaCenter.getInstance().totalDuration();
            if (position > 0) {
                progress = position * 100 / total;
            }
            seekBar.setProgress(progress);
            updateProgress();
        }

        private void unActiveSeekBar() {
//            layout.setVisibility(View.VISIBLE);
            seekBar.setVisibility(View.INVISIBLE);
            removeCallback();
        }

        public void removeCallback() {
            handler.removeCallbacks(mUpdateTimeTask);
        }

        public void updateProgress() {
            handler.postDelayed(mUpdateTimeTask, 100);
        }


        private Runnable mUpdateTimeTask = new Runnable() {
            public void run() {
                long totalDuration = MediaCenter.getInstance().totalDuration();
                long currentDuration = MediaCenter.getInstance().getCurrentTimePosition();

//                current_time.setText("" + milliSecondsToTimer(currentDuration));
                int progress = (getProgressPercentage(currentDuration, totalDuration));
                seekBar.setProgress(progress);
                handler.postDelayed(this, 100);
            }
        };


        public int getProgressPercentage(long currentDuration, long totalDuration) {
            Double percentage;

            long currentSeconds = (int) (currentDuration / 1000);
            long totalSeconds = (int) (totalDuration / 1000);

            // calculating percentage
            percentage = (((double) currentSeconds) / totalSeconds) * 100;

            // return percentage
            return percentage.intValue();
        }


        SeekBar.OnSeekBarChangeListener seekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                updateProgressPosition(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

                handler.removeCallbacks(mUpdateTimeTask);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                MediaCenter.getInstance().playCurrentPosition(seekBar);
                updateProgress();

            }
        };
    }


    public interface OnItemClickListener {
        void playAudio(AudioOffline audio, int position);

        void delete(AudioOffline audui, int position);
    }


}
