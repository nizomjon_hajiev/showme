package uz.ishow.android.adapter.util;

import android.view.View;

import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;

import java.util.HashMap;
import java.util.Map;

import uz.ishow.android.adapter.viewholder.FeedItemViewHolder;
import uz.ishow.android.adapter.viewholder.PhotoViewHolder;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 07.01.2016 15:42.
 */
public class ImageLoaderUtil {

    private Map<View, Boolean> loadedImages = new HashMap<>();
    private Map<View, String> imageUrls = new HashMap<>();
    private Map<String, Integer> imageProgress = new HashMap<>();
    private Map<String, FeedItemViewHolder> imagesViewHolders = new HashMap<>();

    public String getUrlForView(View view) {
        return imageUrls.get(view);
    }

    public boolean isLoaded(String url) {
        Boolean b = loadedImages.get(url);
        return b != null && b;
    }

    public int getProgress(String url) {
        Integer progress = imageProgress.get(url);
        return progress != null ? progress : 0;
    }

    public void setImageLoadedForView(View view, boolean isLoaded) {
        loadedImages.put(view, isLoaded);
    }

    public void setImageUrlForView(View view, String imageUrl) {
        imageUrls.put(view, imageUrl);
    }

    public void setViewHolderForUrl(FeedItemViewHolder vh, String url) {
        imagesViewHolders.put(url, vh);
    }

    public ImageLoadingProgressListener getImageLoadingProgressListener() {
        return imageLoadingProgressListener;
    }

    public void onImageLoaded(String url, View view) {
        String imageUrl = imageUrls.get(view);
        if (url.equals(imageUrl)) {
            loadedImages.put(view, true);
        }

        if (imagesViewHolders.containsKey(url)) {
            FeedItemViewHolder vh = imagesViewHolders.get(url);
            if (vh instanceof PhotoViewHolder) {
                ((PhotoViewHolder) vh).hideProgress();
            }
        }

        if (imageProgress.containsKey(url)) {
            imageProgress.remove(url);
        }
    }

    private ImageLoadingProgressListener imageLoadingProgressListener = new ImageLoadingProgressListener() {
        @Override
        public void onProgressUpdate(String imageUri, View view, int current, int total) {
            int progress = current * 100 / total;
            imageProgress.put(imageUri, progress);
            if (imagesViewHolders.containsKey(imageUri)) {
                FeedItemViewHolder vh = imagesViewHolders.get(imageUri);
                if (vh instanceof PhotoViewHolder) {
                    ((PhotoViewHolder) vh).setProgress(progress);
                }
            }
        }
    };
}
