package uz.ishow.android.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.Bind;
import uz.ishow.android.R;
import uz.ishow.android.adapter.NomineeAdapter;
import uz.ishow.android.model.Audio;
import uz.ishow.android.model.Nominee;
import uz.ishow.android.model.Post;
import uz.ishow.android.model.WinnerNominee;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 22.02.2016 8:32.
 */
public class AudioWinnerNomineeViewHolder extends PollItemViewHolder {

    @Bind(R.id.nomination_name)
    TextView nominationName;

    @Bind(R.id.cover)
    ImageView cover;

    @Bind(R.id.title)
    TextView title;

    @Bind(R.id.author_name)
    TextView authorName;

    @Bind(R.id.votes_count)
    TextView votesCount;


    public AudioWinnerNomineeViewHolder(View itemView, NomineeAdapter.ItemListener itemListener) {
        super(itemView, itemListener);
    }

    @Override
    public void fill(Nominee nominee) {
        super.fill(nominee);
        if (nominee instanceof WinnerNominee) {
            WinnerNominee n = (WinnerNominee)nominee;
            nominationName.setText(n.getNominationName());
            Post p = n.getPost();
            if (p != null && p.getAudio() != null) {
                Audio audio = p.getAudio();
                ImageLoader.getInstance().displayImage(audio.getCover(), cover);
                title.setText(audio.getTitle());
                authorName.setText(audio.getArtist());
                votesCount.setText(getVotesCount(n));
            }
        }
    }
}
