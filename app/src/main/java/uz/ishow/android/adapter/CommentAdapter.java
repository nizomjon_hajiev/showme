package uz.ishow.android.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnLongClick;
import uz.ishow.android.R;
import uz.ishow.android.model.Comment;
import uz.ishow.android.model.User;
import uz.ishow.android.util.DateTime;
import uz.ishow.android.util.Utils;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 07.07.2015 17:06.
 */
public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {
    private Context mContext;
    private List<Comment> mComments;
    private ItemListener mItemListener;

    private Comment selectedItem;

    private List<ViewHolder> viewHolders = new ArrayList<>();

    private int mPlaceholderColor;

    public CommentAdapter(Context mContext, List<Comment> mComments, ItemListener itemListener) {
        this.mContext = mContext;
        this.mComments = mComments;
        this.mItemListener = itemListener;
        this.mPlaceholderColor = mContext.getResources().getColor(R.color.image_placeholder);
    }

    public Context getContext() {
        return mContext;
    }

    public Comment getItem(int position) {
        if (mComments != null && position >= 0 && position < mComments.size()) {
            return mComments.get(position);
        }

        return null;
    }

    public Comment getLastItem() {
        return getItem(getItemCount() - 1);
    }

    public void addToStart(Comment comment) {
        if (mComments != null) {
            mComments.add(0, comment);
            notifyDataSetChanged();
        }
    }

    public void addAllToEnd(List<Comment> comments) {
        if (mComments != null) {
            mComments.addAll(comments);
            notifyDataSetChanged();
        }
    }

    @LayoutRes
    public int getItemLayout(int viewType) {
        return R.layout.comment_list_item;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(getItemLayout(viewType), parent, false);
        ViewHolder vh = new ViewHolder(itemLayoutView);
        viewHolders.add(vh);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder vh, int position) {
        Comment comment = getItem(position);

        vh.headerLayout.setVisibility(View.VISIBLE);
        boolean isSelectedForDelete = selectedItem != null && selectedItem.equals(comment);
        vh.deleteOverlay.setVisibility(isSelectedForDelete ? View.VISIBLE : View.GONE);


        vh.profileImage.setImageDrawable(new ColorDrawable(mPlaceholderColor));
        vh.name.setText("");
        vh.timesAgo.setText("");
        vh.text.setText("");

        if (comment != null) {
            User author = comment.getUser();
            if (author != null) {
                if (author.getPhotoMini() != null) {
                    ImageLoader.getInstance().displayImage(author.getPhotoMini(), vh.profileImage);
                } else {
                    vh.profileImage.setBackground(mContext.getResources().getDrawable(R.drawable.circle_image_view));
                }

                vh.name.setText(author.getName());
                vh.timesAgo.setText(DateTime.timesAgo(comment.getTime(), getContext().getResources()));
            }

            vh.text.setText(Utils.sanitize(comment.getText()));
        }
    }

    @Override
    public int getItemCount() {
        return mComments != null ? mComments.size() : 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.profile_image)
        ImageView profileImage;

        @Bind(R.id.name)
        TextView name;

        @Bind(R.id.times_ago)
        TextView timesAgo;

        @Bind(R.id.header)
        LinearLayout headerLayout;

        @Bind(R.id.text)
        TextView text;

        @Bind(R.id.delete_overlay)
        View deleteOverlay;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnLongClick(R.id.item_root)
        boolean onLongClick() {
            if (mItemListener == null) {
                return false;
            }

            Comment item = getItem(getAdapterPosition());

            if (item.getIsDeletable() || item.getIsBlockable()) {
                if (selectedItem != null) {
                    if (selectedItem.equals(item)) {
                        selectedItem = null;
                        notifyItemChanged(getAdapterPosition());
                        mItemListener.hideActionMode();
                        return true;
                    } else {
                        for (ViewHolder vh : viewHolders) {
                            Comment vhComment = getItem(vh.getAdapterPosition());
                            if (vhComment != null && vhComment.equals(selectedItem)) {
                                notifyItemChanged(vh.getAdapterPosition());
                            }
                        }
                    }
                }
                selectedItem = item;
                mItemListener.showActionMode();
                notifyItemChanged(getAdapterPosition());
                return true;
            }

            return false;
        }
    }

    public Comment getSelectedItem() {
        return selectedItem;
    }

    public void cancelSelection() {
        if (selectedItem != null) {
            for (ViewHolder vh : viewHolders) {
                Comment vhComment = getItem(vh.getAdapterPosition());
                if (vhComment != null && vhComment.equals(selectedItem)) {
                    notifyItemChanged(vh.getAdapterPosition());
                }
            }

            selectedItem = null;
        }
    }

    public void removeSelectedItem() {
        if (selectedItem != null) {
            mComments.remove(selectedItem);
            selectedItem = null;
            notifyDataSetChanged();
        }
    }

    public interface ItemListener {
        void showActionMode();

        void hideActionMode();
    }

}
