package uz.ishow.android.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uz.ishow.android.R;
import uz.ishow.android.model.Profile;
import uz.ishow.android.model.SuggestionFooter;
import uz.ishow.android.model.SuggestionHeader;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 04.07.2015 11:21.
 */
public class UsersGridAdapter extends RecyclerView.Adapter<UsersGridAdapter.ViewHolder> {

    public static final int TYPE_DEFAULT = 0;
    public static final int TYPE_HEADER = 1;
    public static final int TYPE_FOOTER = 2;


    private Context mContext;
    private List<Profile> mUsers;
    private OnUserClickListener onUserClickListener;

    private boolean isTypeSuggestion;
    private int placeholderColor;

    public UsersGridAdapter(Context context, List<Profile> users, OnUserClickListener onUserClickListener) {
        this.mContext = context;
        this.mUsers = new ArrayList<>();
        this.mUsers.addAll(users);
        this.mUsers.add(new SuggestionFooter());
        this.onUserClickListener = onUserClickListener;
    }

    public UsersGridAdapter(Context context, List<Profile> users) {
        this.mContext = context;
        this.mUsers = new ArrayList<>();
        this.mUsers.addAll(users);
        this.mUsers.add(new SuggestionFooter());
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        placeholderColor = recyclerView.getResources().getColor(R.color.image_placeholder);
    }

    public void setIsTypeSuggestion(boolean isTypeSuggestion) {
        this.isTypeSuggestion = isTypeSuggestion;
        this.mUsers.add(0, new SuggestionHeader());
    }

    public void followingChanged(String profileId, int followersCount, boolean isFollowing) {
        for (int i = 0; i < getItemCount(); i++) {
            Profile p = getItem(i);
            if (p instanceof SuggestionHeader) {
                continue;
            }

            if (p.getId().equals(profileId) && p.getIsFollowing() ^ isFollowing) {
                p.setIsFollowing(isFollowing);
                p.setFollowersCount(followersCount);
                notifyItemChanged(i);
            }
        }
    }

    public Context getContext() {
        return mContext;
    }

    @Override
    public int getItemCount() {
        return mUsers != null ? mUsers.size() : 0;
    }

    public Profile getItem(int position) {
        if (mUsers != null && position >= 0 && position < getItemCount()) {
            return mUsers.get(position);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        Profile user = getItem(position);
        if (user instanceof SuggestionHeader) {
            return TYPE_HEADER;
        } else if (user instanceof SuggestionFooter) {
            return TYPE_FOOTER;
        }
        return TYPE_DEFAULT;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout = R.layout.user_grid_item;
        switch (viewType) {
            case TYPE_HEADER:
                layout = R.layout.suggestion_header;
                break;
            case TYPE_FOOTER:
                layout = R.layout.feed_item_spacer;
                break;
        }
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(layout, parent, false);
        return new ViewHolder(itemLayoutView);


    }

    @Override
    public void onBindViewHolder(final ViewHolder vh, final int position) {
        if (getItemViewType(position) == TYPE_DEFAULT) {
            Profile user = getItem(position);

            //reset image
            vh.profileImage.setImageDrawable(new ColorDrawable(placeholderColor));
            ImageLoader.getInstance().displayImage(user.getMiniImage(), vh.profileImage);

            vh.followedSticker.setVisibility(user.getIsFollowing() ? View.VISIBLE : View.GONE);
            vh.name.setText(user.getName());

            int sb = user.getFollowersCount();
            String text = sb == 0 ? getContext().getString(R.string.no_subscribers) :
                    getContext().getResources().getQuantityString(R.plurals.subscribers, sb, sb);
            vh.subscribers.setText(text);
        }

    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @Nullable
        @Bind(R.id.profile_image)
        ImageView profileImage;

        @Nullable
        @Bind(R.id.followedSticker)
        View followedSticker;

        @Nullable
        @Bind(R.id.name)
        TextView name;

        @Nullable
        @Bind(R.id.subscribers)
        TextView subscribers;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Nullable
        @OnClick(R.id.item_root)
        void onItemClick(View v) {
            if (onUserClickListener != null) {
                onUserClickListener.onUserClick(getItem(getAdapterPosition()));
            }
        }
    }



    public interface OnUserClickListener {
        void onUserClick(Profile user);
    }
}
