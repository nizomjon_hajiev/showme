package uz.ishow.android.adapter.viewholder;

import android.view.View;
import android.widget.TextView;

import butterknife.Bind;
import uz.ishow.android.R;
import uz.ishow.android.model.NominationHeaderItem;
import uz.ishow.android.model.Nominee;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 22.02.2016 8:32.
 */
public class NomineeHeaderViewHolder extends PollItemViewHolder {

    @Bind(R.id.description)
    TextView description;

    public NomineeHeaderViewHolder(View itemView) {
        super(itemView, null);
    }

    @Override
    public void fill(Nominee nominee) {
        super.fill(nominee);
        if (nominee instanceof NominationHeaderItem) {
            description.setText(((NominationHeaderItem) nominee).getDescription());
        }
    }
}
