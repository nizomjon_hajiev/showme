package uz.ishow.android.adapter.viewholder;

import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.Bind;
import butterknife.OnClick;
import uz.ishow.android.R;
import uz.ishow.android.listener.FeedItemListener;
import uz.ishow.android.model.FeedItem;
import uz.ishow.android.model.Post;
import uz.ishow.android.model.Profile;
import uz.ishow.android.util.DateTime;
import uz.ishow.android.util.Utils;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 07.01.2016 14:59.
 */
public class PostViewHolder extends FeedItemViewHolder {

    @Nullable
    @Bind(R.id.profile_image)
    ImageView profileImage;

    @Nullable
    @Bind(R.id.name)
    TextView name;

    @Nullable
    @Bind(R.id.times_ago)
    TextView timesAgo;

    @Nullable
    @Bind(R.id.text)
    TextView text;

    @Nullable
    @Bind(R.id.likes_count)
    TextView likesCount;

    @Nullable
    @Bind(R.id.comments_count)
    TextView commentsCount;

    @Nullable
    @Bind(R.id.button_like)
    View buttonLike;

    @Nullable
    @Bind(R.id.button_options)
    View buttonOptions;

    @Nullable
    @Bind(R.id.header)
    LinearLayout header;

    private boolean openProfileDisabled;

    public PostViewHolder(View itemView, FeedItemListener feedItemListener,
                          boolean openProfileDisabled, boolean isCallAndRefresh) {
        super(itemView, feedItemListener);
        this.openProfileDisabled = openProfileDisabled;

        if (isCallAndRefresh && header != null) {
            header.setVisibility(View.VISIBLE);
        } else {
            if (header != null) {
                header.setVisibility(View.GONE);
            }
        }
    }


    @Override
    public void fill(FeedItem feedItem) {
        super.fill(feedItem);
        final Post post = feedItem.getPost();
        Profile owner = post.getAuthor();
        updateUserData(owner);
        if (timesAgo != null) {
            timesAgo.setText(DateTime.timesAgo(post.getPublishedTime(), timesAgo.getResources()));
        }
        updateLike(post.getHasLiked(), post.getLikesCount());
        updateComment(post.getCommentsCount());
        if (buttonOptions != null) {
            buttonOptions.setVisibility(post.getIsDeletable() ? View.VISIBLE : View.GONE);
        }

        if (TextUtils.isEmpty(post.getText())) {
            assert text != null;
            text.setVisibility(View.GONE);
        } else {
            assert text != null;
            text.setText(Utils.sanitize(post.getText()));
            text.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.header)
    void onHeaderClick() {
        if (!openProfileDisabled && getFeedItemListener() != null && getFeedItem() != null) {
            getFeedItemListener().onHeaderClick(getFeedItem());
        }
    }

    @OnClick(R.id.button_comment)
    void onCommentClick() {
        if (getFeedItemListener() != null && getFeedItem() != null) {
            getFeedItemListener().onCommentClick(getFeedItem());
        }
    }

    @OnClick(R.id.button_like)
    void onLikeClick(View likeButton) {
        if (getFeedItemListener() != null && getFeedItem() != null) {
            if (likeButton.isSelected()) {
                getFeedItemListener().unlike(getFeedItem());
            } else {
                getFeedItemListener().like(getFeedItem());
            }
        }
    }

    @OnClick(R.id.button_options)
    void onOptionsButtonClick() {
        if (getFeedItemListener() != null && getFeedItem() != null) {
            FeedItem item = getFeedItem();
            getFeedItemListener().showOptions(item);
        }
    }


    public void updateUserData(Profile owner) {
        if (profileImage != null) {
            ImageLoader.getInstance().displayImage(owner.getMiniImage(), profileImage);
        }
        if (name != null) {
            name.setText(owner.getName());
        }
    }

    public void updateUserData(Profile profile, Bitmap profileImageBitmap) {
        if (profileImageBitmap != null) {
            if (profileImage != null) {
                profileImage.setImageBitmap(profileImageBitmap);
            }
        }
        updateUserData(profile);
    }

    public void updateLike(boolean hasLiked, int likeCount) {
        if (buttonLike != null) {
            buttonLike.setSelected(hasLiked);
        }
        if (likesCount != null) {
            likesCount.setText(String.valueOf(likeCount));
        }
    }

    public void updateComment(int commentsCount) {
        if (this.commentsCount != null) {
            this.commentsCount.setText(String.valueOf(commentsCount));
        }
    }
}


