package uz.ishow.android.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.Bind;
import uz.ishow.android.R;
import uz.ishow.android.adapter.NomineeAdapter;
import uz.ishow.android.model.Nominee;
import uz.ishow.android.model.Profile;
import uz.ishow.android.model.WinnerNominee;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 22.02.2016 8:32.
 */
public class UserWinnerNomineeViewHolder extends PollItemViewHolder {

    @Bind(R.id.nomination_name)
    TextView nominationName;

    @Bind(R.id.profile_image)
    ImageView profileImage;

    @Bind(R.id.name)
    TextView name;

    @Bind(R.id.votes_count)
    TextView votesCount;

    public UserWinnerNomineeViewHolder(View itemView, NomineeAdapter.ItemListener itemListener) {
        super(itemView, itemListener);
    }

    @Override
    public void fill(Nominee nominee) {
        super.fill(nominee);
        if (nominee instanceof WinnerNominee) {
            WinnerNominee n = (WinnerNominee)nominee;
            nominationName.setText(n.getNominationName());
            Profile user = n.getUser();
            if (user != null) {
                ImageLoader.getInstance().displayImage(user.getGeneralImage(), profileImage);
                name.setText(user.getName());
                votesCount.setText(getVotesCount(n));
            }
        }
    }
}
