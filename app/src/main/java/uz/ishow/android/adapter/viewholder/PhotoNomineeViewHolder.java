package uz.ishow.android.adapter.viewholder;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.Bind;
import butterknife.OnClick;
import uz.ishow.android.R;
import uz.ishow.android.adapter.NomineeAdapter;
import uz.ishow.android.model.Nominee;
import uz.ishow.android.model.Photo;
import uz.ishow.android.model.Post;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 22.02.2016 8:32.
 */
public class PhotoNomineeViewHolder extends VotableNomineeViewHolder {

    @Bind(R.id.photo)
    ImageView photoImageView;

    @Bind(R.id.title)
    TextView title;

    @Bind(R.id.author_name)
    TextView authorName;

    @Bind(R.id.votes_count)
    TextView votesCount;

    @Bind(R.id.your_vote)
    View yourVote;

    public PhotoNomineeViewHolder(View itemView, NomineeAdapter.ItemListener itemListener) {
        super(itemView, itemListener);
    }

    @Override
    public void fill(Nominee nominee) {
        super.fill(nominee);
        if (nominee != null && nominee.getPost() != null &&
                nominee.getPost().getPhoto() != null) {
            Post post = nominee.getPost();
            Photo photo = post.getPhoto();
            photoImageView.setImageDrawable(new ColorDrawable(Color.TRANSPARENT));
            ImageLoader.getInstance().displayImage(photo.getUrl(), photoImageView);
            title.setText(post.getText());
            if (post.getAuthor() != null) {
                authorName.setText(post.getAuthor().getName());
            }
            votesCount.setVisibility(nominee.isVotesCountVisible() ? View.VISIBLE : View.GONE);
            votesCount.setText(getVotesCount(nominee));
            buttonVote.setVisibility(nominee.isVotingEnabled() ? View.VISIBLE : View.GONE);
            yourVote.setVisibility(nominee.isMyChoice() ? View.VISIBLE : View.GONE);
        }
    }

    @OnClick(R.id.item)
    void onItemClick() {
        if (getItemListener() != null && getNominee() != null) {
            getItemListener().openPost(getNominee().getPost());
        }
    }
}
