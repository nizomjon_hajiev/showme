package uz.ishow.android.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.Bind;
import butterknife.OnClick;
import uz.ishow.android.R;
import uz.ishow.android.adapter.NomineeAdapter;
import uz.ishow.android.model.Nominee;
import uz.ishow.android.model.Profile;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 22.02.2016 8:32.
 */
public class UserNomineeViewHolder extends VotableNomineeViewHolder {

    @Bind(R.id.profile_image)
    ImageView profileImage;

    @Bind(R.id.name)
    TextView name;

    @Bind(R.id.votes_count)
    TextView votesCount;

    @Bind(R.id.your_vote)
    View yourVote;

    public UserNomineeViewHolder(View itemView, NomineeAdapter.ItemListener itemListener) {
        super(itemView, itemListener);
    }

    @Override
    public void fill(Nominee nominee) {
        super.fill(nominee);
        if (nominee != null && nominee.getUser() != null) {
            Profile user = nominee.getUser();
            ImageLoader.getInstance().displayImage(user.getGeneralImage(), profileImage);
            name.setText(user.getName());
            votesCount.setVisibility(nominee.isVotesCountVisible() ? View.VISIBLE : View.GONE);
            votesCount.setText(getVotesCount(nominee));
            buttonVote.setVisibility(nominee.isVotingEnabled() ? View.VISIBLE : View.GONE);
            yourVote.setVisibility(nominee.isMyChoice() ? View.VISIBLE : View.GONE);
        }
    }

    @OnClick(R.id.item)
    void onItemClick() {
        if (getItemListener() != null && getNominee() != null) {
            getItemListener().openProfile(getNominee().getUser());
        }
    }
}
