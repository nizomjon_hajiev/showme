package uz.ishow.android.adapter.viewholder;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.View;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import butterknife.Bind;
import butterknife.OnClick;
import uz.ishow.android.R;
import uz.ishow.android.adapter.util.ImageLoaderUtil;
import uz.ishow.android.listener.FeedItemListener;
import uz.ishow.android.model.FeedItem;
import uz.ishow.android.model.Post;
import uz.ishow.android.model.Video;
import uz.ishow.android.view.ProportionalImageView;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 08.01.2016 10:57.
 */
public class VideoViewHolder extends PostViewHolder {

    @Bind(R.id.cover)
    ProportionalImageView coverImageView;

    private ImageLoaderUtil mImageLoaderUtil;
    private DisplayImageOptions mImageLodingOptions;
    public VideoViewHolder(View itemView, FeedItemListener feedItemListener, boolean openProfileDisabled,boolean isCallAndRefresh,
                           DisplayImageOptions imageLoadingOptions, ImageLoaderUtil imageLoaderUtil) {
        super(itemView, feedItemListener, openProfileDisabled,isCallAndRefresh);
        this.mImageLoaderUtil = imageLoaderUtil;
        this.mImageLodingOptions = imageLoadingOptions;
    }

    @Override
    public void fill(FeedItem feedItem) {
        super.fill(feedItem);

        Post post = feedItem.getPost();
        Video video = post.getVideo();
        if (video != null && video.getCover() != null) {
            coverImageView.setProportion(video.getCover().getWidth(), video.getCover().getHeight());
            String url = video.getCover().getUrl();
            if (!TextUtils.isEmpty(url)) {
                final String imageUrl = mImageLoaderUtil.getUrlForView(coverImageView);
                if (!url.equals(imageUrl) || mImageLoaderUtil.isLoaded(imageUrl)) {
                    mImageLoaderUtil.setImageLoadedForView(coverImageView, false);
                    mImageLoaderUtil.setImageUrlForView(coverImageView, url);

                    coverImageView.setImageDrawable(new ColorDrawable(Color.TRANSPARENT));
                    mImageLoaderUtil.setViewHolderForUrl(this, url);

                    ImageLoader.getInstance().displayImage(url, coverImageView,
                            mImageLodingOptions, new ImageLoadingListener() {
                                @Override
                                public void onLoadingStarted(String imageUri, View view) {
                                }

                                @Override
                                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                }

                                @Override
                                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                    mImageLoaderUtil.onImageLoaded(imageUri, view);
                                }

                                @Override
                                public void onLoadingCancelled(String imageUri, View view) {
                                }
                            });
                }
            } else {
                coverImageView.setProportion(16, 9);
                coverImageView.setImageDrawable(new ColorDrawable(Color.TRANSPARENT));
            }
        } else {
            coverImageView.setProportion(16, 9);
            coverImageView.setImageDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @OnClick(R.id.video_cover_holder)
    void openVideo() {
        if (getFeedItemListener() != null && getFeedItem() != null) {
            getFeedItemListener().openVideo(getFeedItem());
        }
    }
}
