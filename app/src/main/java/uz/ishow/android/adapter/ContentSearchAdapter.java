package uz.ishow.android.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uz.ishow.android.R;
import uz.ishow.android.model.Post;
import uz.ishow.android.model.Profile;

/**
 * Created by User on 19.04.2016.
 */
public class ContentSearchAdapter extends RecyclerView.Adapter<ContentSearchAdapter.ViewHolder> {

    private Context mContext;
    private List<Post> mPosts;
    private int imagePlaceholderColor;

    public static final int USERS_TYPE = 0;
    public static final int CONTENT_TYPE = 1;
    ItemListener itemListener;
    PostItemListener postItemListener;

    public ContentSearchAdapter(Context mContext, List<Post> mPosts, ItemListener itemListener, PostItemListener postItemListener) {
        this.mContext = mContext;
        this.mPosts = mPosts;
        this.itemListener = itemListener;
        this.postItemListener = postItemListener;
        imagePlaceholderColor = mContext.getResources().getColor(R.color.image_placeholder);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case CONTENT_TYPE:
                View view = LayoutInflater.from(mContext).inflate(R.layout.search_content_item, parent, false);
                return new ContentViewHolder(view);
            default:
                View view1 = LayoutInflater.from(mContext).inflate(R.layout.search_user_item, parent, false);
                return new UsersViewHolder(view1);
        }

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Post post = getItem(position);

        if (holder instanceof ContentViewHolder) {

            ((ContentViewHolder) holder).post = post;
            ((ContentViewHolder) holder).contentTitle.setText(post.getText());
            ((ContentViewHolder) holder).contentPostByUser.setText(post.getAuthor().getName() + " " + mContext.getString(R.string.post_by_user));

        } else if (holder instanceof UsersViewHolder) {

            ((UsersViewHolder) holder).profile = post.getAuthor();
            ((UsersViewHolder) holder).profileImage.setImageDrawable(new ColorDrawable(imagePlaceholderColor));
            ImageLoader.getInstance().displayImage(post.getAuthor().getMiniImage(), ((UsersViewHolder) holder).profileImage);
            ((UsersViewHolder) holder).name.setText(post.getAuthor().getName());

            int followersCount = post.getAuthor().getFollowersCount();
            String text = followersCount == 0 ? mContext.getString(R.string.no_subscribers) :
                    mContext.getResources().getQuantityString(R.plurals.subscribers, followersCount, followersCount);
            ((UsersViewHolder) holder).followersCount.setText(text);


        }

    }

    @Override
    public int getItemViewType(int position) {
        Post post = getItem(position);
        if (post.isContentView()) {
            return CONTENT_TYPE;
        }
        return USERS_TYPE;
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void addAll(List<Post> posts) {
        if (mPosts == null) {
            mPosts = new ArrayList<>();
        }

        if (posts != null) {
            for (Post post : posts) {

                mPosts.add(post);

            }
        }

        notifyDataSetChanged();


    }


    private Post getItem(int position) {
        if (mPosts != null && position >= 0 && position < getItemCount()) {
            return mPosts.get(position);
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return mPosts != null ? mPosts.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class UsersViewHolder extends ViewHolder {


        @Nullable
        @Bind(R.id.profile_image)
        ImageView profileImage;

        @Nullable
        @Bind(R.id.name)
        TextView name;

        @Nullable
        @Bind(R.id.followers_count)
        TextView followersCount;

        @Nullable
        @Bind(R.id.divider)
        View dividerView;
        private Profile profile;

        public UsersViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            dividerView.setVisibility(View.VISIBLE);
        }

        @OnClick(R.id.root_view)
        void onItemClick() {
            if (profile != null && itemListener != null) {
                itemListener.onItemClick(profile);
            }
        }
    }

    class ContentViewHolder extends ViewHolder {

        @Nullable
        @Bind(R.id.content_title)
        TextView contentTitle;

        @Nullable
        @Bind(R.id.content_post_by_user)
        TextView contentPostByUser;

        private Post post;

        public ContentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.content_post_container)
        void onPostItemClick() {
            if (post != null && postItemListener != null) {
                postItemListener.onPostItemClick(post);
            }
        }
    }

    public interface ItemListener {
        void onItemClick(Profile profile);
    }

    public interface PostItemListener {
        void onPostItemClick(Post post);
    }
}
