package uz.ishow.android.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import uz.ishow.android.R;
import uz.ishow.android.activity.BaseActivity;
import uz.ishow.android.model.Profile;

/**
 * Created by User on 08.04.2016.
 */
public class ViewPagerAdapter extends PagerAdapter {

    private List<Profile> mCategories;
    private Context mContext;
    List<List<Profile>> mPrLists;
    List<Profile> firstList;
    List<Profile> secondList;
    List<Profile> thirdList;

    @Nullable
    @Bind({R.id.cell_0, R.id.cell_1, R.id.cell_2})
    List<View> cells;

    @Nullable
    @Bind({R.id.name_0, R.id.name_1, R.id.name_2})
    List<TextView> mNames;

    @Nullable
    @Bind({R.id.subscribers_0, R.id.subscribers_1, R.id.subscribers_2})
    List<TextView> mSubscribers;

    @Nullable
    @Bind({R.id.profile_image_0, R.id.profile_image_1, R.id.profile_image_2})
    List<ImageView> profileImages;

    @Nullable
    @Bind({R.id.followed_sticker_0, R.id.followed_sticker_1, R.id.followed_sticker_2})
    List<View> followStickers;

    int pageLimit = 3;

    public ViewPagerAdapter(List<Profile> mCategories, Context mContext) {
        this.mCategories = mCategories;

        mPrLists = new ArrayList<>();
        firstList = new ArrayList<>();
        secondList = new ArrayList<>();
        thirdList = new ArrayList<>();

        for (int i = 0; i < mCategories.size(); i++) {
            if (i <= 2) {
                firstList.add(mCategories.get(i));
            } else if (i >= 3 && i <= 5) {
                secondList.add(mCategories.get(i));
            } else if (i >= 6 && i <= 8) {
                thirdList.add(mCategories.get(i));
            }
        }
        this.mContext = mContext;
        mPrLists.add(firstList);
        mPrLists.add(secondList);
        mPrLists.add(thirdList);
    }

    public void setPageLimit(int number) {
        this.pageLimit = number;
    }

    @Override
    public int getCount() {
        return pageLimit;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) container.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.stars_category, container, false);
        ButterKnife.bind(this, view);
        List<Profile> mProfile = mPrLists.get(position);

        for (int i = 0; i < mProfile.size(); i++) {
            if (mProfile != null && mProfile.size() > i && mProfile.get(i) != null) {
                cells.get(i).setVisibility(View.VISIBLE);
                final Profile p = mProfile.get(i);
                cells.get(i).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getBaseActivity().openProfileFeed(p);
                    }
                });
                if (p.getMiniImage() == null)
                    profileImages.get(i).setBackground(mContext.getResources().getDrawable(R.drawable.circle_image_view));
                ImageLoader.getInstance().displayImage(p.getMiniImage(), profileImages.get(i));
                followStickers.get(i).setVisibility(p.getIsFollowing() ? View.VISIBLE : View.GONE);
                mNames.get(i).setText(p.getName());
                int sb = p.getFollowersCount();
                String text = sb == 0 ? mContext.getString(R.string.no_subscribers) :
                        mContext.getResources().getQuantityString(R.plurals.subscribers, sb, sb);
                mSubscribers.get(i).setText(text);
            } else {
                cells.get(i).setVisibility(View.GONE);
            }

        }


        container.addView(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public interface OnUserClickListener {
        void onUserClick(Profile user);
    }

    public BaseActivity getBaseActivity() {
        return (BaseActivity) mContext;
    }


}
