package uz.ishow.android.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import uz.ishow.android.R;
import uz.ishow.android.adapter.viewholder.NominationViewHolder;
import uz.ishow.android.model.Nomination;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 21.02.2016 14:30.
 */
public class PollNominationAdapter extends RecyclerView.Adapter<NominationViewHolder> {

    private List<Nomination> mNominations;
    private boolean isLangRu;
    private ItemClickListener mItemClickListener;

    public PollNominationAdapter(List<Nomination> nominations, ItemClickListener itemClickListener,
                                 boolean isLangRu) {
        this.mItemClickListener = itemClickListener;
        this.isLangRu = isLangRu;
        mNominations = new ArrayList<>();
        for (Nomination n : nominations) {
            if (n != null) {
                mNominations.add(n);
            }
        }
    }

    @Override
    public NominationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.nomination_item, parent, false);
        NominationViewHolder vh = new NominationViewHolder(view, mItemClickListener, isLangRu);
        return vh;
    }

    @Override
    public void onBindViewHolder(NominationViewHolder vh, int position) {
        Nomination n = getItem(position);
        if (n != null) {
            vh.fill(n);
        }
    }

    @Override
    public int getItemCount() {
        return mNominations != null ? mNominations.size() : 0;
    }

    public Nomination getItem(int position) {
        if (mNominations != null && mNominations.size() > position) {
            return mNominations.get(position);
        }

        return null;
    }

    public interface ItemClickListener {
        void onItemClick(Nomination nomination);
    }
}
