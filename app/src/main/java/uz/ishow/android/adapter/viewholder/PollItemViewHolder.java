package uz.ishow.android.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;
import uz.ishow.android.R;
import uz.ishow.android.adapter.NomineeAdapter;
import uz.ishow.android.model.Nominee;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 21.02.2016 18:59.
 */
public class PollItemViewHolder extends RecyclerView.ViewHolder {

    private NomineeAdapter.ItemListener mItemListener;
    private Nominee nominee;

    public PollItemViewHolder(View itemView, NomineeAdapter.ItemListener itemListener) {
        super(itemView);
        this.mItemListener = itemListener;
        ButterKnife.bind(this, itemView);
    }


    public void fill(Nominee nominee) {
        this.nominee = nominee;
    }

    protected String getVotesCount(Nominee nominee) {
        int votesCount = nominee != null ? nominee.getVotesCount() : 0;
        if (votesCount <= 0) {
            return itemView.getResources().getString(R.string.no_votes);
        } else {
            return itemView.getResources().getQuantityString(R.plurals.votes, votesCount, votesCount);
        }
    }

    public NomineeAdapter.ItemListener getItemListener() {
        return mItemListener;
    }

    public Nominee getNominee() {
        return nominee;
    }
}
