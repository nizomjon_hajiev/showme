package uz.ishow.android.adapter.viewholder;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import uz.ishow.android.R;
import uz.ishow.android.application.IShowApplication;
import uz.ishow.android.event.TimelineEvent;
import uz.ishow.android.fragment.TimelineFragment;
import uz.ishow.android.listener.FeedItemListener;
import uz.ishow.android.model.FeedItem;
import uz.ishow.android.model.Profile;
import uz.ishow.android.util.Settings;

/**
 * Created by User on 06.05.2016.
 */
public class CallAndRefreshViewHolder extends FeedItemViewHolder {
    @Nullable
    @Bind(R.id.call)
    Button callBtn;

    @Nullable
    @Bind(R.id.callAndRefreshContainer)
    LinearLayout mLinearLayout;

    @Inject
    Settings mSettings;
    TimelineFragment timelineFragment;


    private boolean canCall = false;
    private Profile mPost;
    private Context mContext;
    private Profile mProfile;

    public CallAndRefreshViewHolder(View itemView, FeedItemListener feedItemListener, Context context) {
        super(itemView, feedItemListener);
        this.mContext = context;
        timelineFragment = new TimelineFragment();
    }


    @Override
    public void fill(FeedItem feedItem) {
        super.fill(feedItem);


        mPost = IShowApplication.getProfile();
        if (mPost != null) {

//            if (mPost != null && mPost.getCanFollow() && !TextUtils.isEmpty(mPost.getContactPhone())) {
//                callBtn.setVisibility(View.VISIBLE);
//            } else {
//                callBtn.setVisibility(View.GONE);
//            }
        }
//        if (IShowApplication.isStar()) {
//            callBtn.setVisibility(View.GONE);
//        }
    }

//    @OnClick(R.id.call)
//    void call() {
//        EventBus.getDefault().post(TimelineProfileEvent.callToUser());
//    }

    @OnClick(R.id.refresh_content)
    void refreshContent() {

        if (timelineFragment != null) {
            EventBus.getDefault().post(TimelineEvent.refreshTimeline());
        }
    }
//
//    public void onEvent(TimelineProfileEvent event) {
//        if (event.canCall) {
//            callBtn.setVisibility(View.VISIBLE);
//        } else {
//            callBtn.setVisibility(View.GONE);
//        }
//
//        mLinearLayout.invalidate();
//    }

}
