package uz.ishow.android.adapter.viewholder;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import butterknife.Bind;
import butterknife.OnClick;
import uz.fonus.widget.CircularProgressBar;
import uz.ishow.android.R;
import uz.ishow.android.adapter.util.ImageLoaderUtil;
import uz.ishow.android.listener.FeedItemListener;
import uz.ishow.android.model.FeedItem;
import uz.ishow.android.model.Post;
import uz.ishow.android.view.ProportionalImageView;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 07.01.2016 15:22.
 */
public class PhotoViewHolder extends PostViewHolder {

    @Bind(R.id.photo)
    ProportionalImageView photo;

    @Bind(R.id.image_progress_bar)
    CircularProgressBar imageProgressBar;

    @Bind(R.id.tap_to_reload)
    View tapToReload;

    private ImageLoaderUtil mImageLoaderUtil;
    private DisplayImageOptions imageLoadingOptions;
    private boolean isCallAndRefresh;

    public PhotoViewHolder(View itemView, FeedItemListener feedItemListener, boolean openProfileEnabled,boolean isCallAndRefresh,
                           DisplayImageOptions imageLoadingOptions, ImageLoaderUtil imageLoaderUtil) {
        super(itemView, feedItemListener, openProfileEnabled,isCallAndRefresh);
        this.mImageLoaderUtil = imageLoaderUtil;
        this.imageLoadingOptions = imageLoadingOptions;

        imageProgressBar.setBackgroundCircleColor(imageProgressBar.getResources()
                .getColor(R.color.feed_image_progress_background_color));
        imageProgressBar.setProgressColor(imageProgressBar.getResources()
                .getColor(R.color.feed_image_progress_color));

        final GestureDetector.SimpleOnGestureListener doubleTapListener = new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }

            @Override
            public boolean onDoubleTap(MotionEvent e) {
                FeedItem item = getFeedItem();
                if (item != null && !item.getPost().getHasLiked()) {
                    onLikeClick(buttonLike);
                }
                return true;
            }
        };

        final GestureDetector doubleTapDetector = new GestureDetector(photo.getContext(), doubleTapListener);

        photo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return doubleTapDetector.onTouchEvent(event);
            }
        });
    }

    @Override
    public void fill(FeedItem feedItem) {
        super.fill(feedItem);
        Post post = feedItem.getPost();

        tapToReload.setVisibility(View.GONE);
        if (post.getPhoto() != null) {
            photo.setProportion(post.getPhoto().getWidth(), post.getPhoto().getHeight());
            imageProgressBar.setVisibility(View.GONE);

            String url = getPhotoUrl();
            if (TextUtils.isEmpty(url)) {
                photo.setImageDrawable(new ColorDrawable(Color.TRANSPARENT));
            } else {
                loadImage();
            }
        } else {
            photo.setImageDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @OnClick(R.id.tap_to_reload)
    void loadImage() {
        tapToReload.setVisibility(View.GONE);

        String url = getPhotoUrl();
        if (TextUtils.isEmpty(url)) {
            return;
        }

        final String imageUrl = mImageLoaderUtil.getUrlForView(photo);
        imageProgressBar.setProgress(mImageLoaderUtil.getProgress(imageUrl));
        if (!url.equals(imageUrl) || mImageLoaderUtil.isLoaded(imageUrl)) {
            mImageLoaderUtil.setImageLoadedForView(photo, false);
            mImageLoaderUtil.setImageUrlForView(photo, url);

            photo.setImageDrawable(new ColorDrawable(Color.TRANSPARENT));
            imageProgressBar.setVisibility(View.VISIBLE);
            mImageLoaderUtil.setViewHolderForUrl(this, url);
            ImageLoader.getInstance().displayImage(url, photo,
                    imageLoadingOptions, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            if (imageUri != null && imageUri.equals(mImageLoaderUtil.getUrlForView(view))) {
                                mImageLoaderUtil.setImageUrlForView(view, null);
                                hideProgress();
                                tapToReload.setVisibility(View.VISIBLE);
                            }
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            mImageLoaderUtil.onImageLoaded(imageUri, view);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {
                        }
                    }, mImageLoaderUtil.getImageLoadingProgressListener());
        }
    }

    private String getPhotoUrl() {
        if (getFeedItem() != null && getFeedItem().getPost() != null && getFeedItem().getPost().getPhoto() != null) {
            return getFeedItem().getPost().getPhoto().getUrl();
        }
        return null;
    }

    public void setProgress(int progress) {
        imageProgressBar.setProgress(progress);
    }

    public void hideProgress() {
        imageProgressBar.setVisibility(View.GONE);
    }
}
