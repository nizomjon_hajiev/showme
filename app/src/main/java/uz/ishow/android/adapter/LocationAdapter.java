package uz.ishow.android.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import uz.ishow.android.R;
import uz.ishow.android.model.Location;
import uz.ishow.android.model.Predictions;

/**
 * Created by User on 17.06.2016.
 */
public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.ViewHolder> {

//    List

    Location mLocation;
    Context mContext;

    public LocationAdapter(Location location, Context context) {
        mLocation = location;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.location_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Predictions predictions = mLocation.getPredictionses().get(position);

        holder.locName.setText(predictions.getDescription());


    }

    @Override
    public int getItemCount() {
        return mLocation.getPredictionses().size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @Nullable
        @Bind(R.id.location_name)
        TextView locName;
        @Nullable
        @Bind(R.id.location_orientation)
        TextView locOrient;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
