package uz.ishow.android.adapter;

import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uz.ishow.android.R;
import uz.ishow.android.event.FeedItemEvent;
import uz.ishow.android.model.Category;
import uz.ishow.android.model.CategorySpacer;
import uz.ishow.android.model.Profile;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 26.10.2015 11:29.
 */
public class    CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    public static final int TYPE_DEFAULT = 0;
    public static final int TYPE_SPACER = 1;

    private List<Category> categories;
    private CategoryItemListener categoryItemListener;
    private boolean isLangUz;
    private int placeholderColor;
    private List<ViewHolder> mViewHolders = new ArrayList<>();

    public CategoryAdapter(List<Category> categories, CategoryItemListener categoryItemListener, boolean isLangUz) {
        this.categories = new ArrayList<>();
        this.categories.addAll(categories);
        this.categories.add(new CategorySpacer());
        this.categoryItemListener = categoryItemListener;
        this.isLangUz = isLangUz;
    }

    public void onEvent(FeedItemEvent event) {
        switch (event.getType()) {
            case FOLLOWING: {
                followingChanged(event.getProfileId(), event.getFollowersCount(), event.isFollowing());
                break;
            }

            case USER_DATA_CHANGED: {
                onUserDataChanged(event.getUser(), event.getProfileImageBitmap());
                break;
            }
        }
    }

    private void onUserDataChanged(Profile user, Bitmap bmp) {
        for (int i = 0; i < getItemCount(); i++) {
            Category cat = getItem(i);
            if (cat != null && cat.getUsers() != null) {
                for (Profile p : cat.getUsers()) {
                    if (p != null && p.equals(user)) {
                        p.setMiniImage(user.getMiniImage());
                        p.setName(user.getName());

                        for (ViewHolder vh : mViewHolders) {
                            vh.updateUserIfContains(p, bmp);
                        }

                    }
                }
            }
        }
    }


    private void followingChanged(String profileId, int followersCount, boolean isFollowing) {
        boolean hasChanges = false;
        for (int i = 0; i < getItemCount(); i++) {
            Category cat = getItem(i);
            if (cat != null && cat.getUsers() != null) {
                for (Profile p : cat.getUsers()) {
                    if (p != null && p.getId() != null && p.getId().equals(profileId)) {
                        p.setFollowersCount(followersCount);
                        p.setIsFollowing(isFollowing);
                        hasChanges = true;
                    }
                }
            }
        }

        if (hasChanges) {
            notifyDataSetChanged();
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        placeholderColor = recyclerView.getResources().getColor(R.color.image_placeholder);
    }

    @Override
    public int getItemViewType(int position) {
        Category cat = getItem(position);
        if (cat != null) {
            return cat instanceof CategorySpacer ? TYPE_SPACER : TYPE_DEFAULT;
        }
        return TYPE_DEFAULT;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout = viewType == TYPE_SPACER ? R.layout.feed_item_spacer : R.layout.category;
        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        mViewHolders.add(vh);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder vh, int position) {
        if (getItemViewType(position) == TYPE_SPACER) {
            return;
        }

        Category cat = getItem(position);
        vh.fill(cat);
    }

    @Override
    public int getItemCount() {
        return categories != null ? categories.size() : 0;
    }

    public Category getItem(int position) {
        if (position >= 0 && position < categories.size()) {
            return categories.get(position);
        }
        return null;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @Nullable
        @Bind(R.id.title)
        TextView title;

        @Nullable
        @Bind(R.id.button_all)
        Button buttonAll;
        @Nullable
        @Bind({R.id.cell_0, R.id.cell_1, R.id.cell_2})
        List<View> cells;

        @Nullable
        @Bind({R.id.profile_image_0, R.id.profile_image_1, R.id.profile_image_2})
        List<ImageView> profileImages;

        @Nullable
        @Bind({R.id.followed_sticker_0, R.id.followed_sticker_1, R.id.followed_sticker_2})
        List<View> followStickers;

        private Category mCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            if (isLangUz) {
                if (buttonAll != null)
                    buttonAll.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
        }

        public void fill(Category cat) {
            this.mCategory = cat;
            title.setText(isLangUz ? cat.getName() : cat.getNameRu());
            for (int i = 0; i < 3; i++) {
                if (cat.getUsers() != null && cat.getUsers().size() > i && cat.getUsers().get(i) != null) {
                    profileImages.get(i).setImageDrawable(new ColorDrawable(placeholderColor));
                    cells.get(i).setVisibility(View.VISIBLE);
                    Profile p = cat.getUsers().get(i);
                    ImageLoader.getInstance().displayImage(p.getMiniImage(), profileImages.get(i));
                    if (p.getMiniImage() == null) {
                        profileImages.get(i).setImageDrawable(new ColorDrawable(placeholderColor));
                    }
                    followStickers.get(i).setVisibility(p.getIsFollowing() ? View.VISIBLE : View.GONE);
                } else {
                    cells.get(i).setVisibility(View.INVISIBLE);
                }
            }
        }

        public void updateUserIfContains(Profile p, Bitmap bmp) {
            if (mCategory != null && mCategory.getUsers() != null) {
                for (int i = 0; i < 3; i++) {
                    if (mCategory.getUsers() != null && mCategory.getUsers().size() > i
                            && mCategory.getUsers().get(i) != null
                            && mCategory.getUsers().get(i).getId() != null
                            && mCategory.getUsers().get(i).getId().equals(p.getId())) {

                        if (bmp != null) {
                            profileImages.get(i).setImageBitmap(bmp);
                        }
                        ImageLoader.getInstance().displayImage(p.getMiniImage(), profileImages.get(i));
                    }
                }
            }
        }

        @Nullable
        @OnClick({R.id.cell_0, R.id.cell_1, R.id.cell_2})
        void onCellClick(View v) {
            if (categoryItemListener != null) {
                int index = cells.indexOf(v);
                if (index != -1) {
                    Category cat = getItem(getAdapterPosition());
                    List<Profile> users = cat.getUsers();
                    if (users != null && users.size() > index) {
                        Profile p = users.get(index);
                        categoryItemListener.showUserProfile(p);
                    }
                }
            }
        }

        @Nullable
        @OnClick(R.id.button_all)
        void showAllUsers() {
            if (categoryItemListener != null) {

                categoryItemListener.showCategoryUsers(getItem(getAdapterPosition()));
            }
        }
    }

    public interface CategoryItemListener {
        void showUserProfile(Profile profile);

        void showCategoryUsers(Category cat);
    }
}
