package uz.ishow.android.adapter.viewholder;

import android.view.View;
import android.widget.Button;

import butterknife.Bind;
import butterknife.OnClick;
import uz.ishow.android.R;
import uz.ishow.android.adapter.NomineeAdapter;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 25.02.2016 11:25.
 */
public class VotableNomineeViewHolder extends PollItemViewHolder {

    @Bind(R.id.button_vote)
    Button buttonVote;

    public VotableNomineeViewHolder(View itemView, NomineeAdapter.ItemListener itemListener) {
        super(itemView, itemListener);
    }

    @OnClick(R.id.button_vote)
    void onClickVoteButton() {
        if (getItemListener() != null && getNominee() != null) {
            getItemListener().vote(getNominee());
        }
    }
}
