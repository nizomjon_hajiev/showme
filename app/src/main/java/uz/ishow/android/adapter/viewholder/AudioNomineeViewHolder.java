package uz.ishow.android.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.Bind;
import butterknife.OnClick;
import uz.ishow.android.R;
import uz.ishow.android.adapter.NomineeAdapter;
import uz.ishow.android.model.Audio;
import uz.ishow.android.model.Nominee;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 22.02.2016 8:32.
 */
public class AudioNomineeViewHolder extends VotableNomineeViewHolder {

    @Bind(R.id.cover)
    ImageView cover;

    @Bind(R.id.title)
    TextView title;

    @Bind(R.id.author_name)
    TextView authorName;

    @Bind(R.id.votes_count)
    TextView votesCount;

    @Bind(R.id.your_vote)
    View yourVote;

    public AudioNomineeViewHolder(View itemView, NomineeAdapter.ItemListener itemListener) {
        super(itemView, itemListener);
    }

    @Override
    public void fill(Nominee nominee) {
        super.fill(nominee);
        if (nominee != null && nominee.getPost() != null &&
                nominee.getPost().getAudio() != null) {
            Audio audio = nominee.getPost().getAudio();
            ImageLoader.getInstance().displayImage(audio.getCover(), cover);
            title.setText(audio.getTitle());
            authorName.setText(audio.getArtist());
            votesCount.setVisibility(nominee.isVotesCountVisible() ? View.VISIBLE : View.GONE);
            votesCount.setText(getVotesCount(nominee));
            buttonVote.setVisibility(nominee.isVotingEnabled() ? View.VISIBLE : View.GONE);
            yourVote.setVisibility(nominee.isMyChoice() ? View.VISIBLE : View.GONE);
        }
    }

    @OnClick(R.id.item)
    void onItemClick() {
        if (getItemListener() != null && getNominee() != null) {
            getItemListener().openPost(getNominee().getPost());
        }
    }
}
