package uz.ishow.android.adapter.viewholder;

import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.Bind;
import butterknife.OnClick;
import uz.ishow.android.R;
import uz.ishow.android.listener.FeedItemListener;
import uz.ishow.android.model.FeedItem;
import uz.ishow.android.model.Post;
import uz.ishow.android.util.MediaCenter;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 07.01.2016 16:09.
 */
public class AudioViewHolder extends PostViewHolder {

    @Nullable
    @Bind(R.id.button_play_pause)
    View buttonPlayPause;

    @Nullable
    @Bind(R.id.audio_cover)
    ImageView audioCover;

    @Nullable
    @Bind(R.id.audio_author)
    TextView audioAuthor;

    @Nullable
    @Bind(R.id.audio_title)
    TextView audio_title;

    @Nullable
    @Bind(R.id.seek_bar)
    SeekBar seekBar;
//
//    @Nullable
//    @Bind(R.id.seek_bar_container)
//    LinearLayout seek_bar_layout;
//
//    @Nullable
//    @Bind(R.id.current_time)
//    TextView current_time;
//
//    @Nullable
//    @Bind(R.id.music_length)
//    TextView music_length;

    @Nullable
    @Bind(R.id.audio_time)
    TextView audioTime;

    @Bind(R.id.delete_music)
    View deleteAudio;

//
//    @Nullable
//    @Bind(R.id.musicContextContainer)
//    LinearLayout layout;

    @Nullable
    @Bind(R.id.add_to_music)
    View addToMusic;

    int progress;
    Post specialPost;

    private Handler handler;
    private boolean isCallAndRefresh;

    public AudioViewHolder(View itemView, FeedItemListener feedItemListener, boolean openProfileEnabled, boolean isCallAndRefresh) {
        super(itemView, feedItemListener, openProfileEnabled, isCallAndRefresh);
        seekBar.setOnSeekBarChangeListener(seekBarChangeListener);
        handler = new Handler();
        deleteAudio.setVisibility(View.GONE);

    }


    @Override
    public void fill(FeedItem feedItem) {
        super.fill(feedItem);

        Post post = feedItem.getPost();
        specialPost = post;

        boolean isPlaying = MediaCenter.getInstance().isPlaying(feedItem.getPost());
        boolean isDownloaded = post.getAudio().isDownloaded();
        assert buttonPlayPause != null;
        buttonPlayPause.setSelected(isPlaying);
        assert addToMusic != null;
        addToMusic.setSelected(isDownloaded);
        addToMusic.setClickable(!isDownloaded);
        if (isPlaying) {
            activeSeekBar();
        } else {
            unActiveSeekBar();
        }
        audioCover.setImageResource(R.drawable.music_placeholder_new);

        if (post.getAudio() != null) {
            if (!TextUtils.isEmpty(post.getAudio().getCover())) {
                ImageLoader.getInstance().displayImage(post.getAudio().getCover(), audioCover);
            }
            audioAuthor.setText(post.getAudio().getArtist());
            audio_title.setText(post.getAudio().getTitle());
            audioTime.setText(post.getAudio().getHumanReadableDuration());
        } else {
            audioAuthor.setText("");
            audio_title.setText("");
            audioTime.setText("");
        }


    }

    @OnClick(R.id.button_play_pause)
    void onAudioClick() {
        if (getFeedItemListener() != null && getFeedItem() != null) {
            getFeedItemListener().onAudioClick(getFeedItem());
        }
    }

    @OnClick(R.id.add_to_music)
    void downloadSong() {
        if (getFeedItemListener() != null && getFeedItem() != null) {
            getFeedItemListener().downloadSong(getFeedItem());
        }
    }

    public void setButtonAsPlayed(boolean isPlaying) {
        buttonPlayPause.setSelected(isPlaying);
    }

    public void setViewAsDownloaded(boolean isDownloaded) {
        addToMusic.setSelected(isDownloaded);
//        addToMusic.setVisibility(View.VISIBLE);
    }

    public void setSeekBarAsPlayed(boolean isSeekBar) {
        if (isSeekBar) {
            seekBar.setVisibility(View.VISIBLE);
//            layout.setVisibility(View.INVISIBLE);
            updateProgress();

        }
    }

    public void closeSeekBar(boolean isSeekBar) {
        if (isSeekBar) {
//            layout.setVisibility(View.VISIBLE);
            seekBar.setVisibility(View.GONE);
            handler.removeCallbacks(mUpdateTimeTask);
//            current_time.setText("0:00");
            seekBar.setProgress(0);
        }
    }

//    public void setTime(String musicLength) {
//        music_length.setText(musicLength);
//
//    }

    private void activeSeekBar() {
        seekBar.setVisibility(View.VISIBLE);
//        layout.setVisibility(View.INVISIBLE);
        removeCallback();
        int position = MediaCenter.getInstance().getCurrentTimePosition();
        int total = MediaCenter.getInstance().totalDuration();
        if (position > 0) {
            progress = position * 100 / total;
        }
        seekBar.setProgress(progress);
        updateProgress();
    }

    private void unActiveSeekBar() {
//        layout.setVisibility(View.VISIBLE);
        seekBar.setVisibility(View.INVISIBLE);
        removeCallback();
    }

    public void removeCallback() {
        handler.removeCallbacks(mUpdateTimeTask);
    }

    public void updateProgress() {
        handler.postDelayed(mUpdateTimeTask, 100);
    }

    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = MediaCenter.getInstance().totalDuration();
            long currentDuration = MediaCenter.getInstance().getCurrentTimePosition();

//            current_time.setText("" + milliSecondsToTimer(currentDuration));
            int progress = getProgressPercentage(currentDuration, totalDuration);
            seekBar.setProgress(progress);

            handler.postDelayed(this, 100);
        }
    };


    SeekBar.OnSeekBarChangeListener seekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//            updateProgressPosition(progress);

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            MediaCenter.getInstance().playCurrentPosition(seekBar);
            updateProgress();
        }
    };

    public String milliSecondsToTimer(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        return finalTimerString;
    }

    public int getProgressPercentage(long currentDuration, long totalDuration) {
        Double percentage = (double) 0;

        long currentSeconds = (int) (currentDuration / 1000);
        long totalSeconds = (int) (totalDuration / 1000);

        percentage = (((double) currentSeconds) / totalSeconds) * 100;

        return percentage.intValue();
    }
}
