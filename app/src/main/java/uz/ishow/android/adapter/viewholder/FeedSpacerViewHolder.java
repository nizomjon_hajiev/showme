package uz.ishow.android.adapter.viewholder;

import android.view.View;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 08.01.2016 10:00.
 */
public class FeedSpacerViewHolder extends FeedItemViewHolder {

    public FeedSpacerViewHolder(View itemView) {
        super(itemView, null);
    }

}
