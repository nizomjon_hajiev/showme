package uz.ishow.android.adapter.viewholder;

import android.view.View;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.Bind;
import butterknife.OnClick;
import uz.ishow.android.R;
import uz.ishow.android.adapter.NomineeAdapter;
import uz.ishow.android.model.Nominee;
import uz.ishow.android.model.Photo;
import uz.ishow.android.model.Post;
import uz.ishow.android.model.WinnerNominee;
import uz.ishow.android.view.ProportionalImageView;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 22.02.2016 8:32.
 */
public class PhotoWinnerNomineeViewHolder extends PollItemViewHolder {

    @Bind(R.id.nomination_name)
    TextView nominationName;

    @Bind(R.id.photo)
    ProportionalImageView photoImageView;

    @Bind(R.id.title)
    TextView title;

    @Bind(R.id.author_name)
    TextView authorName;

    @Bind(R.id.votes_count)
    TextView votesCount;

    public PhotoWinnerNomineeViewHolder(View itemView, NomineeAdapter.ItemListener itemListener) {
        super(itemView, itemListener);
    }

    @Override
    public void fill(Nominee nominee) {
        super.fill(nominee);
        if (nominee instanceof WinnerNominee) {
            WinnerNominee n = (WinnerNominee) nominee;
            nominationName.setText(n.getNominationName());
            Post p = n.getPost();
            if (p != null && p.getPhoto() != null) {
                Photo photo = p.getPhoto();
                if (photo.getWidth() != null && photo.getHeight() != null) {
                    photoImageView.setProportion(photo.getWidth(), photo.getHeight());
                }
                ImageLoader.getInstance().displayImage(photo.getUrl(), photoImageView);
                title.setText(p.getText());
                if (p.getAuthor() != null) {
                    authorName.setText(p.getAuthor().getName());
                } else {
                    authorName.setText("");
                }
                votesCount.setText(getVotesCount(n));
            }
        }
    }

    @OnClick(R.id.item)
    void onItemClick() {
        if (getItemListener() != null && getNominee() != null) {
            getItemListener().openPost(getNominee().getPost());
        }
    }

}
