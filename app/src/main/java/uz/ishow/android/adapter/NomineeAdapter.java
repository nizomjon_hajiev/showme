package uz.ishow.android.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import uz.ishow.android.R;
import uz.ishow.android.adapter.viewholder.AudioNomineeViewHolder;
import uz.ishow.android.adapter.viewholder.AudioWinnerNomineeViewHolder;
import uz.ishow.android.adapter.viewholder.NomineeHeaderViewHolder;
import uz.ishow.android.adapter.viewholder.PhotoNomineeViewHolder;
import uz.ishow.android.adapter.viewholder.PhotoWinnerNomineeViewHolder;
import uz.ishow.android.adapter.viewholder.PollItemViewHolder;
import uz.ishow.android.adapter.viewholder.UserNomineeViewHolder;
import uz.ishow.android.adapter.viewholder.UserWinnerNomineeViewHolder;
import uz.ishow.android.adapter.viewholder.VideoNomineeViewHolder;
import uz.ishow.android.adapter.viewholder.VideoWinnerNomineeViewHolder;
import uz.ishow.android.model.Nomination;
import uz.ishow.android.model.NominationHeaderItem;
import uz.ishow.android.model.Nominee;
import uz.ishow.android.model.NomineeTextCentered;
import uz.ishow.android.model.Post;
import uz.ishow.android.model.Profile;
import uz.ishow.android.model.WinnerNominee;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 21.02.2016 14:30.
 */
public class NomineeAdapter extends RecyclerView.Adapter<PollItemViewHolder> {

    public static final int TYPE_UNKNOWN = 0;
    public static final int TYPE_AUDIO = 1;
    public static final int TYPE_VIDEO = 2;
    public static final int TYPE_PHOTO = 3;
    public static final int TYPE_USER = 4;
    public static final int TYPE_HEADER = 5;
    public static final int TYPE_WINNER_AUDIO = 6;
    public static final int TYPE_WINNER_VIDEO = 7;
    public static final int TYPE_WINNER_PHOTO = 8;
    public static final int TYPE_WINNER_USER = 9;
    public static final int TYPE_TEXT_CENTERED = 10;


    private Nomination mNomination;
    private List<Nominee> mNominees;
    private boolean isLangRu;
    private ItemListener mItemListener;

    public NomineeAdapter(Nomination nomination, ItemListener itemListener, boolean isLangRu) {
        this.mNomination = nomination;
        this.mItemListener = itemListener;
        this.isLangRu = isLangRu;
        if (nomination != null) {
            mNominees = new ArrayList<>();

            if (nomination.isActive()) {
                mNominees.add(new NominationHeaderItem(isLangRu ? nomination.getDescriptionRU() :
                        nomination.getDescription()));
            } else if (nomination.getWinner() != null) {
                mNominees.add(nomination.getWinner());
                mNominees.add(new NomineeTextCentered());
            }

            for (Nominee n : nomination.getNominees()) {
                if (n != null) {
                    if (nomination.isActive()) {
                        n.setVotesCountVisible(false);
                        n.setVotingEnabled(TextUtils.isEmpty(nomination.getVotedNomineeId()));
                    } else {
                        n.setVotesCountVisible(true);
                        n.setVotingEnabled(false);
                    }
                    n.setIsMyChoise(n.getId() != null && n.getId().equals(nomination.getVotedNomineeId()));

                    boolean isValid = (nomination.isTypeAudio() && n.getPost() != null
                            && n.getPost().getAudio() != null)
                            || (nomination.isTypeVideo() && n.getPost() != null
                            && n.getPost().getVideo() != null)
                            || (nomination.isTypePhoto() && n.getPost() != null
                            && n.getPost().getPhoto() != null)
                            || (nomination.isTypeUser() && n.getUser() != null);

                    if (isValid) {
                        mNominees.add(n);
                    }
                }
            }
        }
    }

    @Override
    public PollItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case TYPE_HEADER: {
                View v = inflater.inflate(R.layout.nominee_header, parent, false);
                return new NomineeHeaderViewHolder(v);
            }

            case TYPE_WINNER_AUDIO: {
                View v = inflater.inflate(R.layout.nominee_winner_audio, parent, false);
                return new AudioWinnerNomineeViewHolder(v, mItemListener);
            }

            case TYPE_WINNER_VIDEO: {
                View v = inflater.inflate(R.layout.nominee_winner_video, parent, false);
                return new VideoWinnerNomineeViewHolder(v, mItemListener);
            }

            case TYPE_WINNER_PHOTO: {
                View v = inflater.inflate(R.layout.nominee_winner_photo, parent, false);
                return new PhotoWinnerNomineeViewHolder(v, mItemListener);
            }

            case TYPE_WINNER_USER: {
                View v = inflater.inflate(R.layout.nominee_winner_user, parent, false);
                return new UserWinnerNomineeViewHolder(v, mItemListener);
            }

            case TYPE_AUDIO: {
                View v = inflater.inflate(R.layout.nominee_audio, parent, false);
                return new AudioNomineeViewHolder(v, mItemListener);
            }

            case TYPE_VIDEO: {
                View v = inflater.inflate(R.layout.nominee_video, parent, false);
                return new VideoNomineeViewHolder(v, mItemListener);
            }

            case TYPE_PHOTO: {
                View v = inflater.inflate(R.layout.nominee_photo, parent, false);
                return new PhotoNomineeViewHolder(v, mItemListener);
            }

            case TYPE_USER: {
                View v = inflater.inflate(R.layout.nominee_user, parent, false);
                return new UserNomineeViewHolder(v, mItemListener);
            }

            case TYPE_TEXT_CENTERED: {
                View v = inflater.inflate(R.layout.nominee_text_centered, parent, false);
                return new PollItemViewHolder(v, null);
            }
        }

        return null;
    }

    @Override
    public void onBindViewHolder(PollItemViewHolder vh, int position) {
        Nominee n = getItem(position);
        if (n != null && vh != null) {
            vh.fill(n);
        }
    }

    @Override
    public int getItemViewType(int position) {
        Nominee n = getItem(position);
        if (n != null) {
            if (n instanceof NominationHeaderItem) {
                return TYPE_HEADER;
            } else if (n instanceof NomineeTextCentered) {
                return TYPE_TEXT_CENTERED;
            } else if (n instanceof WinnerNominee) {
                if (mNomination.isTypeAudio()) {
                    return TYPE_WINNER_AUDIO;
                } else if (mNomination.isTypeVideo()) {
                    return TYPE_WINNER_VIDEO;
                } else if (mNomination.isTypePhoto()) {
                    return TYPE_WINNER_PHOTO;
                } else if (mNomination.isTypeUser()) {
                    return TYPE_WINNER_USER;
                }
            } else {
                if (mNomination.isTypeAudio()) {
                    return TYPE_AUDIO;
                } else if (mNomination.isTypeVideo()) {
                    return TYPE_VIDEO;
                } else if (mNomination.isTypePhoto()) {
                    return TYPE_PHOTO;
                } else if (mNomination.isTypeUser()) {
                    return TYPE_USER;
                }
            }
        }

        return TYPE_UNKNOWN;
    }

    @Override
    public int getItemCount() {
        return mNominees != null ? mNominees.size() : 0;
    }

    public Nominee getItem(int position) {
        if (mNominees != null && mNominees.size() > position) {
            return mNominees.get(position);
        }

        return null;
    }

    public interface ItemListener {
        void openProfile(Profile profile);

        void openPost(Post post);

        void vote(Nominee nominee);
    }
}
