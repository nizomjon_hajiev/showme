package uz.ishow.android.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.tonicartos.superslim.LayoutManager;
import com.tonicartos.superslim.LinearSLM;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import uz.ishow.android.R;
import uz.ishow.android.adapter.util.ImageLoaderUtil;
import uz.ishow.android.adapter.viewholder.AudioViewHolder;
import uz.ishow.android.adapter.viewholder.CallAndRefreshViewHolder;
import uz.ishow.android.adapter.viewholder.FeedItemViewHolder;
import uz.ishow.android.adapter.viewholder.FeedSpacerViewHolder;
import uz.ishow.android.adapter.viewholder.HeaderViewHolder;
import uz.ishow.android.adapter.viewholder.PhotoViewHolder;
import uz.ishow.android.adapter.viewholder.PostViewHolder;
import uz.ishow.android.adapter.viewholder.VideoViewHolder;
import uz.ishow.android.database.DatabaseHelper;
import uz.ishow.android.event.AudioEvent;
import uz.ishow.android.event.FeedItemEvent;
import uz.ishow.android.event.TimelineEvent;
import uz.ishow.android.listener.FeedItemListener;
import uz.ishow.android.model.AudioOffline;
import uz.ishow.android.model.FeedItem;
import uz.ishow.android.model.Post;
import uz.ishow.android.model.Profile;


/**
 * Created by Sarimsakov Bakhrom Azimovich on 18.06.2015 15:57.
 */
public class FeedAdapter extends RecyclerView.Adapter<FeedItemViewHolder> {
    public static final int TYPE_SPACER = 0;
    public static final int TYPE_TEXT = 1;
    public static final int TYPE_PHOTO = 2;
    public static final int TYPE_AUDIO = 3;
    public static final int TYPE_VIDEO = 4;
    public static final int TYPE_HEADER = 5;
    public static final int TYPE_CALL_AND_REFRESH = 6;
    private List<FeedItem> mFeedItems;

    private ArrayList<LineItem> mItems;
    private boolean isTimeLine = true;
    private boolean isCallAndRefresh;

    int sectionFirstPosition = 0;
    private DatabaseHelper databaseHelper;
    private Context mContext;
    private FeedItemListener mFeedItemListener;
    private DisplayImageOptions noRAMCacheOptions;
    private boolean openProfileDisabled;
    private List<AudioOffline> audioOfflines;
    private List<FeedItemViewHolder> viewHolders = new ArrayList<>();
    private ImageLoaderUtil mImageLoaderUtil = new ImageLoaderUtil();

    public FeedAdapter(List<FeedItem> feedItems, boolean isTimeLine, boolean isCallAndRefresh, Context context, FeedItemListener listener) {
        this.isTimeLine = isTimeLine;
        this.isCallAndRefresh = isCallAndRefresh;
        this.mContext = context;
        this.mFeedItemListener = listener;
        databaseHelper = new DatabaseHelper(context);
        audioOfflines = new ArrayList<>();
        audioOfflines = databaseHelper.getAllAudios();
        this.mFeedItems = new ArrayList<>();
        mItems = new ArrayList<>();
        if (feedItems != null) {
            for (FeedItem fi : feedItems) {
                if (isValidItem(fi)) {
                    if (fi.getType().equals("audio")) {
                        for (int i = 0; i < audioOfflines.size(); i++) {
                            if (audioOfflines.get(i).getAudio_id().equals(fi.getPost().getId())) {
                                fi.getPost().getAudio().setIsDownloaded(true);
                                break;
                            } else {
                                fi.getPost().getAudio().setIsDownloaded(false);

                            }
                        }
                    }

                    mFeedItems.add(fi);
                }
            }
        }

        int headerCount = 0;

        if (isCallAndRefresh) {
            mItems.add(new LineItem(new FeedItem()));
        }

        if (feedItems != null) {
            for (int i = 0; i < feedItems.size(); i++) {
                sectionFirstPosition = i + headerCount;
                headerCount += 1;
                if (!isCallAndRefresh)
                    mItems.add(new LineItem(feedItems.get(i), true, sectionFirstPosition));
                mItems.add(new LineItem(feedItems.get(i), false, sectionFirstPosition));

            }
        }

//        if (mFeedItems.size() == 0) {
//            this.mItems.add(new LineItem(new FeedItem()));
//        }

        // Add spacer to end
        this.mFeedItems.add(new FeedItem());
        this.mItems.add(new LineItem(new FeedItem()));
//        if (!isTimeLine) {
//            this.mItems.add(new LineItem(new FeedItem()));
//        }


        noRAMCacheOptions = new DisplayImageOptions.Builder()
                .cacheOnDisk(true)
                .cacheInMemory(false)
                .build();
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void onEvent(FeedItemEvent event) {
        switch (event.getType()) {
            case FOLLOWING: {
                onFollowingChanged(event.getProfileId(), event.getFollowersCount(), event.isFollowing());
                break;
            }

            case LIKE: {
                onLikeChanged(event.getPostId(), event.isLiked(), event.getLikeCount());
                break;
            }

            case COMMENT: {
                onCommentCountChanged(event.getPostId(), event.getCommentCount());
                break;
            }

            case USER_DATA_CHANGED: {
                onUserDataChanged(event.getUser(), event.getProfileImageBitmap());
                break;
            }

            case POST_DELETED: {
                onPostDeleted(event.getPost());
                break;
            }
        }
    }

    public void onEventBackgroundThread(AudioEvent event) {
        switch (event.getType()) {
            case START:
            case RESUME: {
                for (FeedItemViewHolder vh : viewHolders) {
//                    FeedItem item = getItem(vh.getAdapterPosition());
                    LineItem lineItem = getLineItem(vh.getAdapterPosition());

                    if (lineItem != null && lineItem.getFeedItem().isTypeAudio() && lineItem.getFeedItem().getPost().equals(event.getAudioPost()) &&
                            vh instanceof AudioViewHolder) {
                        ((AudioViewHolder) vh).setButtonAsPlayed(true);
                        ((AudioViewHolder) vh).setSeekBarAsPlayed(true);
//                        ((AudioViewHolder) vh).setTime(event.getAudioPost().getAudio().getHumanReadableDuration());
                    }
                }
                break;
            }

            case PAUSE: {
                for (FeedItemViewHolder vh : viewHolders) {
                    LineItem lineItem = getLineItem(vh.getAdapterPosition());

                    if (lineItem != null && lineItem.getFeedItem().isTypeAudio() && lineItem.getFeedItem().getPost().equals(event.getAudioPost()) &&
                            vh instanceof AudioViewHolder) {
                        ((AudioViewHolder) vh).setButtonAsPlayed(false);
                    }
                }
                break;
            }
            case STOP: {
                for (FeedItemViewHolder vh : viewHolders) {
                    LineItem lineItem = getLineItem(vh.getAdapterPosition());

                    if (lineItem != null && lineItem.getFeedItem().isTypeAudio() && lineItem.getFeedItem().getPost().equals(event.getAudioPost()) &&
                            vh instanceof AudioViewHolder) {
                        ((AudioViewHolder) vh).setButtonAsPlayed(false);
                        ((AudioViewHolder) vh).closeSeekBar(true);
                    }
                }
                break;
            }


            case DOWNLOADED:
                for (FeedItemViewHolder vh : viewHolders) {
                    LineItem lineItem = getLineItem(vh.getAdapterPosition());

                    if (lineItem != null && lineItem.getFeedItem().isTypeAudio() && lineItem.getFeedItem().getPost().getId().equals(event.getAudioPost().getId()) &&
                            vh instanceof AudioViewHolder) {
                        ((AudioViewHolder) vh).setViewAsDownloaded(true);
                        lineItem.getFeedItem().getPost().getAudio().setIsDownloaded(true);
                        notifyItemChanged(vh.getAdapterPosition());
                    }
                }
                break;

        }
    }

    private void onFollowingChanged(String profileId, int followersCount, boolean isFollowing) {
        for (int i = 0; i < getItemCount(); i++) {
            LineItem lineItem = getLineItem(i);
//            FeedItem item = getItem(i);
            if (lineItem != null && lineItem.getFeedItem().getPost() != null && lineItem.getFeedItem().getPost().getAuthor() != null) {
                Profile author = lineItem.getFeedItem().getPost().getAuthor();
                if (author.getId().equals(profileId)) {
                    author.setIsFollowing(isFollowing);
                    author.setFollowersCount(followersCount);
                }
            }
        }
    }

    private void onLikeChanged(String postId, boolean isLiked, int likesCount) {
        for (int i = 0; i < getItemCount(); i++) {
            Post post = getLineItem(i).getFeedItem().getPost();
            if (post != null && post.getId() != null && post.getId().equals(postId) &&
                    post.getHasLiked() ^ isLiked) {
                post.setLikesCount(likesCount);
                post.setHasLiked(isLiked);

                for (FeedItemViewHolder vh : viewHolders) {
                    FeedItem item = vh.getFeedItem();
                    if (item != null && item.getPost() != null && item.getPost().getId() != null &&
                            item.getPost().getId().equals(postId) && vh instanceof PostViewHolder) {
                        ((PostViewHolder) vh).updateLike(isLiked, likesCount);
                    }
                }
            }
        }
    }

    private void onCommentCountChanged(String postId, int commentCount) {
        for (int i = 0; i < getItemCount(); i++) {
            Post post = getLineItem(i).getFeedItem().getPost();
            if (post != null && post.getId() != null && post.getId().equals(postId)) {
                post.setCommentsCount(commentCount);
            }
        }

        for (FeedItemViewHolder vh : viewHolders) {
            FeedItem item = vh.getFeedItem();
            if (item != null && item.getPost() != null && item.getPost().getId() != null &&
                    item.getPost().getId().equals(postId) && vh instanceof PostViewHolder) {
                ((PostViewHolder) vh).updateComment(commentCount);
            }
        }
    }

    private void onUserDataChanged(Profile user, Bitmap profileImage) {
        for (int i = 0; i < getItemCount(); i++) {
            Post post = getLineItem(i).getFeedItem().getPost();
            if (post != null && post.getAuthor().equals(user)) {
                Profile p = post.getAuthor();
                p.setMiniImage(user.getMiniImage());
                p.setName(user.getName());
            }
        }

        for (FeedItemViewHolder vh : viewHolders) {
            FeedItem item = getLineItem(vh.getAdapterPosition()).getFeedItem();
            if (item != null && item.getPost() != null && item.getPost().getAuthor() != null &&
                    item.getPost().getAuthor().equals(user) && vh instanceof PostViewHolder) {
                ((PostViewHolder) vh).updateUserData(item.getPost().getAuthor(), profileImage);
            }
        }

    }

    private void onPostDeleted(Post deletedPost) {
        List<FeedItem> deletionList = new ArrayList<>();
        for (int i = 0; i < getItemCount(); i++) {
            FeedItem item = getLineItem(i).getFeedItem();
            if (item != null && item.getPost() != null && item.getPost().equals(deletedPost)) {
                deletionList.add(item);
            }
        }

        if (deletionList.size() > 0) {
            for (FeedItem item : deletionList) {
                mFeedItems.remove(item);
            }
            EventBus.getDefault().post(TimelineEvent.refreshTimeline());
            notifyDataSetChanged();
        }
    }

    public void setOpenProfileDisabled(boolean openProfileDisabled) {
        this.openProfileDisabled = openProfileDisabled;
    }

    public void addAll(List<FeedItem> items) {
        if (mFeedItems == null) {
            mFeedItems = new ArrayList<>();
        }

        int size = mFeedItems.size();


        FeedItem spacer = null;
        if (size > 0 && mFeedItems.get(size - 1).isTypeSpacer()) {
            spacer = mFeedItems.remove(size - 1);
        }

        LineItem spacerLineItem = null;
        if (mItems.size() > 0) {
            spacerLineItem = mItems.remove(mItems.size() - 1);
        }
        if (items != null) {
            for (FeedItem fi : items) {
                if (isValidItem(fi)) {
                    if (fi.getType().equals("audio")) {
                        for (int i = 0; i < audioOfflines.size(); i++) {
                            if (audioOfflines.get(i).getAudio_id().equals(fi.getPost().getId())) {
                                fi.getPost().getAudio().setIsDownloaded(true);
                                break;
                            } else {
                                fi.getPost().getAudio().setIsDownloaded(false);

                            }
                        }
                    }

                    mFeedItems.add(fi);
                }
            }
        }
//        Set<LineItem> set = new HashSet<>();
//        set.addAll(mItems);
//        mItems.clear();
//        mItems.addAll(set);

        if (mItems == null) {
            mItems = new ArrayList<>();
        }

        for (int i = 0; i < items.size(); i++) {
            sectionFirstPosition += 2;
            if (!isCallAndRefresh)
                mItems.add(new LineItem(items.get(i), true, sectionFirstPosition));
            mItems.add(new LineItem(items.get(i), false, sectionFirstPosition));

        }

        if (spacer != null) {
            mFeedItems.add(spacer);
        }
        if (spacerLineItem != null) {
            mItems.add(spacerLineItem);
        }

        notifyDataSetChanged();
    }

    public FeedItem getItem(int position) {
        if (mFeedItems != null && position >= 0 && position < getItemCount()) {
            return mFeedItems.get(position);
        }
        return null;
    }

    public LineItem getLineItem(int position) {
        if (mItems != null && position >= 0 && position < getItemCount()) {
            return mItems.get(position);
        }
        return null;
    }


    public FeedItem getLastItem() {
        if (getItemCount() > 0) {
            FeedItem item = getLineItem(getItemCount() - 1).getFeedItem();
            if (item != null && item.isTypeSpacer()) {
                if (getItemCount() > 1) {
                    return getLineItem(getItemCount() - 2).getFeedItem();
                }
            } else {
                return item;
            }
        }

        return null;
    }


    public Context getContext() {
        return mContext;
    }

    @Override
    public FeedItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext())
                .inflate(getLayoutResource(viewType), parent, false);

        FeedItemViewHolder vh;
        switch (viewType) {
            case TYPE_TEXT: {
                vh = new PostViewHolder(itemView, mFeedItemListener, openProfileDisabled, isCallAndRefresh);
                break;
            }

            case TYPE_PHOTO: {
                vh = new PhotoViewHolder(itemView, mFeedItemListener, openProfileDisabled, isCallAndRefresh,
                        noRAMCacheOptions, mImageLoaderUtil);
                break;
            }

            case TYPE_AUDIO: {
                vh = new AudioViewHolder(itemView, mFeedItemListener, openProfileDisabled, isCallAndRefresh);
                break;
            }

            case TYPE_VIDEO: {
                vh = new VideoViewHolder(itemView, mFeedItemListener, openProfileDisabled, isCallAndRefresh,
                        noRAMCacheOptions, mImageLoaderUtil);
                break;
            }
            case TYPE_HEADER: {
                vh = new HeaderViewHolder(itemView, mFeedItemListener, openProfileDisabled);
                break;
            }
            case TYPE_CALL_AND_REFRESH: {
                vh = new CallAndRefreshViewHolder(itemView, mFeedItemListener, mContext);
                break;
            }

            default: {
                vh = new FeedSpacerViewHolder(itemView);
            }
        }

        viewHolders.add(vh);
        return vh;
    }

    @Override
    public void onBindViewHolder(final FeedItemViewHolder vh, final int position) {


        LineItem lineItem = mItems.get(position);
        vh.fill(lineItem.getFeedItem());
        View itemView = vh.itemView;

        if (isTimeLine) {
            LayoutManager.LayoutParams params = (LayoutManager.LayoutParams) itemView.getLayoutParams();
            params.setSlm(LinearSLM.ID);
            params.setFirstPosition(lineItem.sectionFirstPosition);
            itemView.setLayoutParams(params);
        }

    }

    public static class LineItem {

        public int sectionFirstPosition;

        public boolean isHeader = false;
        public FeedItem feedItem;

        public LineItem(FeedItem feedItem) {
            this.feedItem = feedItem;
        }

        public FeedItem getFeedItem() {
            return feedItem;
        }

        public LineItem(FeedItem feedItem, boolean isHeader,
                        int sectionFirstPosition) {
            this.isHeader = isHeader;
            this.feedItem = feedItem;
            this.sectionFirstPosition = sectionFirstPosition;
        }

    }

    @Override
    public int getItemCount() {

        return mItems != null ? mItems.size() : 0;


    }

    @Override
    public int getItemViewType(int position) {


        LineItem lineItem = mItems.get(position);


        if (isCallAndRefresh && position == 0) {
            return TYPE_CALL_AND_REFRESH;
        } else if (!isCallAndRefresh && lineItem.isHeader) {
            return TYPE_HEADER;
        } else if (lineItem.getFeedItem().isTypeText()) {
            return TYPE_TEXT;
        } else if (lineItem.getFeedItem().isTypePhoto()) {
            return TYPE_PHOTO;
        } else if (lineItem.getFeedItem().isTypeAudio()) {
            return TYPE_AUDIO;
        } else if (lineItem.getFeedItem().isTypeVideo()) {
            return TYPE_VIDEO;
        } else {
            return TYPE_SPACER;
        }


    }

    @LayoutRes
    protected int getLayoutResource(int viewType) {
        switch (viewType) {
            case TYPE_PHOTO: {
                return R.layout.feed_item_photo;
            }
            case TYPE_AUDIO: {
                return R.layout.feed_item_audio_test;
            }

            case TYPE_TEXT: {
                return R.layout.feed_item_text;
            }

            case TYPE_VIDEO: {
                return R.layout.feed_item_video;
            }
            case TYPE_HEADER: {
                return R.layout.feed_header_test;
            }
            case TYPE_CALL_AND_REFRESH: {
                return R.layout.feed_item_call_and_refresh;
            }

            default: {
                return R.layout.feed_item_spacer;
            }
        }
    }

    private boolean isValidItem(FeedItem item) {
        if (item != null && item.getPost() != null) {
            if (item.isTypeVideo() && item.getPost().getVideo() == null) {
                return false;
            } else {
                return item.isTypeVideo() || item.isTypeText()
                        || item.isTypePhoto() || item.isTypeAudio()
                        || item.isTypeSpacer();
            }
        }

        return false;
    }

}
