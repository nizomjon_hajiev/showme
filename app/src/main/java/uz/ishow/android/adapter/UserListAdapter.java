package uz.ishow.android.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uz.ishow.android.R;
import uz.ishow.android.model.Profile;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 05.11.2015 15:19.
 */
public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder> {

    private Context context;
    private List<Profile> mProfiles;
    private int imagePlaceholderColor;
    private ItemListener itemListener;
    private int placeholderColor;


    public UserListAdapter(Context context, List<Profile> mProfiles, ItemListener itemListener) {
        this.context = context;
        this.mProfiles = mProfiles;
        this.itemListener = itemListener;
        imagePlaceholderColor = context.getResources().getColor(R.color.image_placeholder);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.search_user_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        placeholderColor = recyclerView.getResources().getColor(R.color.image_placeholder);
    }

    @Override
    public void onBindViewHolder(ViewHolder vh, int position) {
        Profile profile = getItem(position);
        vh.profile = profile;
        vh.profileImage.setImageDrawable(new ColorDrawable(imagePlaceholderColor));
        ImageLoader.getInstance().displayImage(profile.getMiniImage(), vh.profileImage);
        vh.name.setText(profile.getName());

        int followersCount = profile.getFollowersCount();
        String text = followersCount == 0 ? context.getString(R.string.no_subscribers) :
                context.getResources().getQuantityString(R.plurals.subscribers, followersCount, followersCount);
        vh.followersCount.setText(text);
    }

    @Override
    public int getItemCount() {
        return mProfiles != null ? mProfiles.size() : 0;
    }

    public Profile getItem(int position) {
        if (mProfiles != null && position >= 0 && position < getItemCount()) {
            return mProfiles.get(position);
        }

        return null;
    }

    public void clear() {
        if (mProfiles != null && !mProfiles.isEmpty()) {
            mProfiles.clear();
            notifyDataSetChanged();
        }
    }

    public void replaceAll(List<Profile> profiles) {
        if (profiles == null) {
            profiles = new ArrayList<>();
        }
        mProfiles = profiles;
        notifyDataSetChanged();
    }

    public void addAll(List<Profile> profiles) {
        if (mProfiles == null) {
            mProfiles = new ArrayList<>();
        }

        mProfiles.addAll(profiles);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.profile_image)
        ImageView profileImage;

        @Bind(R.id.name)
        TextView name;

        @Bind(R.id.followers_count)
        TextView followersCount;


        private Profile profile;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.root_view)
        void onItemClick() {
            if (profile != null && itemListener != null) {
                itemListener.onItemClick(profile);
            }
        }
    }

    public interface ItemListener {
        void onItemClick(Profile profile);
    }
}
