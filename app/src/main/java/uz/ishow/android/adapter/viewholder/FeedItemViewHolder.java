package uz.ishow.android.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;
import uz.ishow.android.listener.FeedItemListener;
import uz.ishow.android.model.FeedItem;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 07.01.2016 14:55.
 */
public class FeedItemViewHolder extends RecyclerView.ViewHolder {

    private FeedItemListener feedItemListener;
    private FeedItem feedItem;

    public FeedItemViewHolder(View itemView, FeedItemListener feedItemListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.feedItemListener = feedItemListener;
    }

    public void fill(FeedItem feedItem) {
        this.feedItem = feedItem;
    }

    public FeedItemListener getFeedItemListener() {
        return feedItemListener;
    }

    public FeedItem getFeedItem() {
        return feedItem;
    }
}
