package uz.ishow.android.activity;

import android.content.DialogInterface;
import android.content.Intent;

import com.cocosw.bottomsheet.BottomSheet;

import uz.ishow.android.R;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 22.01.2016 15:40.
 */
public class BottomSheetActivity extends PickImageActivity {


    public void showAddPostSheet() {
        new BottomSheet.Builder(this)
                .sheet(R.id.menu_post_text, R.drawable.ic_addstatus, R.string.post_a_text)
                .sheet(R.id.menu_post_photo, R.drawable.ic_addphoto, R.string.post_a_photo)
                .listener(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case R.id.menu_post_photo:
                                pickImage(REQUEST_PICK_IMAGE);
                                break;
                            case R.id.menu_post_text:
                                openCreatePostActivity(null);
                                break;
                        }
                    }
                }).show();
    }


    @Override
    protected void onPickedImageReady(String filePath) {
        openCreatePostActivity(filePath);
    }


    private void openCreatePostActivity(String pickedImagePath) {
        Intent intent = new Intent(getApplicationContext(), CreatePostActivity.class);
        if (pickedImagePath != null) {
            intent.putExtra("IMAGE_PATH", pickedImagePath);
        }

        startActivity(intent);
    }
}
