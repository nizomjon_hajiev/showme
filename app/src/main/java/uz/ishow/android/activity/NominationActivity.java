package uz.ishow.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit.client.Response;
import uz.ishow.android.R;
import uz.ishow.android.adapter.NomineeAdapter;
import uz.ishow.android.api.callback.BaseCallback;
import uz.ishow.android.model.Nomination;
import uz.ishow.android.model.Nominee;
import uz.ishow.android.model.Post;
import uz.ishow.android.model.Profile;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 21.02.2016 18:42.
 */
public class NominationActivity extends BottomSheetActivity {

    private Nomination nomination;

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    @Bind(R.id.progress_bar)
    ProgressBar progressBar;

    @Bind({R.id.tab_feed, R.id.tab_news, R.id.tab_poll, R.id.tab_catalog, R.id.tab_profile})
    List<View> tabs;

    private NomineeAdapter mAdapter;
    private String id;

    @Bind(R.id.tab_add)
    View tabAdd;

    @Bind(R.id.tab_poll)
    View tabPoll;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nomination);
        setHomeAsUp();
        id = getIntent().getStringExtra("ID");
//        setTitle(getIntent().getStringExtra("TITLE"));
        activityLabel.setText(getIntent().getStringExtra("TITLE"));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        reloadContent();
        invalidateTabs();
    }

    private void invalidateTabs() {
        tabAdd.setVisibility(getUser() != null && getUser().getHasProfile() ? View.VISIBLE : View.GONE);
        tabPoll.setVisibility(!settings.isAuthorized() || getUser() == null || !getUser().getHasProfile() ? View.VISIBLE : View.GONE);
        tabPoll.setSelected(true);
    }

    private void reloadContent() {
        api.getNomination(id, new BaseCallback<Nomination>() {
            @Override
            public void success(Nomination result) {
                recyclerView.setAdapter(new NomineeAdapter(result, mItemListener, !settings.isLanguageUz()));
            }
        });
    }

    @OnClick({R.id.tab_feed, R.id.tab_news, R.id.tab_catalog, R.id.tab_profile})
    void onTabClick(final View tab) {
        Intent data = new Intent();
        data.putExtra("SELECTED_TAB_INDEX", tabs.indexOf(tab));
        setResult(RESULT_OK, data);
        supportFinishAfterTransition();
    }

    @OnClick(R.id.tab_add)
    void onAddClick() {
        showAddPostSheet();
    }

    private NomineeAdapter.ItemListener mItemListener = new NomineeAdapter.ItemListener() {
        @Override
        public void openProfile(Profile profile) {
            openProfileFeed(profile);
        }

        @Override
        public void openPost(Post post) {
            if (post != null) {
                Intent activityIntent = new Intent(getApplicationContext(), PostActivity.class);
                activityIntent.putExtra("POST_JSON", gson.toJson(post));
                startActivityForResult(activityIntent, REQUEST_OPEN_POST);
            }
        }

        @Override
        public void vote(final Nominee nominee) {
            if (settings.isAuthorized()) {
                api.vote(nominee.getId(), new BaseCallback<Response>() {
                    @Override
                    public void success(Response result) {
                        reloadContent();
                    }
                });
            } else {
                auth(new RegisterCompleteListener() {
                    @Override
                    public void onComplete() {
                        vote(nominee);
                    }
                });
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_OPEN_POST) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK, data);
                supportFinishAfterTransition();
            }
        }
    }


}
