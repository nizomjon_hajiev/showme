package uz.ishow.android.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.MediaController;
import android.widget.ProgressBar;

import butterknife.Bind;
import uz.fonus.util.systemuivis.SystemUiHelper;
import uz.ishow.android.R;
import uz.ishow.android.view.VideoPlayer;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 21.11.2015 15:02.
 */

public class VideoPlayerActivity extends BaseActivity {

    @Bind(R.id.video_player)
    VideoPlayer videoPlayer;

    @Bind(R.id.touch_overlay)
    View touchOverlay;

    @Bind(R.id.progress_bar)
    ProgressBar progressBar;

    private MediaController mediaController;
    private SystemUiHelper systemUiHelper;
    private AudioManager mAudioManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        systemUiHelper = new SystemUiHelper(this, SystemUiHelper.LEVEL_HIDE_STATUS_BAR,
                0, onVisibilityChangeListener);

        setContentView(R.layout.activity_video_player);
        setHomeAsUp();

        String title = getIntent().getStringExtra("TITLE");
        String url = getIntent().getStringExtra("URL");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle(title);
//            activityLabel.setText(title);
        }

        Uri uri = Uri.parse(url);

        mediaController = new MediaController(this);
        mediaController.setAnchorView(videoPlayer);


        videoPlayer.setMediaController(mediaController);
        videoPlayer.setVideoURI(uri);

        videoPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                requestAudioFocus();
                progressBar.setVisibility(View.GONE);
                systemUiHelper.delayHide(3000);
            }
        });

        progressBar.setVisibility(View.VISIBLE);
        videoPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                progressBar.setVisibility(View.GONE);
                new AlertDialog.Builder(VideoPlayerActivity.this, R.style.AppAlertDialogTheme)
                        .setMessage(R.string.video_playing_error)
                        .setPositiveButton(R.string.alert_button_close, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .create()
                        .show();

                return true;
            }
        });

        videoPlayer.setPlaybackListener(new VideoPlayer.PlaybackListener() {
            @Override
            public void onStart() {
                requestAudioFocus();
            }

            @Override
            public void onPause() {

            }

            @Override
            public void onResume() {
                requestAudioFocus();
            }
        });

        videoPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                finish();
            }
        });

        videoPlayer.start();

        touchOverlay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (!systemUiHelper.isShowing()) {
                    systemUiHelper.show();
                }
                return false;
            }
        });
    }

    private void requestAudioFocus() {
        mAudioManager.requestAudioFocus(onAudioFocusChangeListener, AudioManager.STREAM_MUSIC,
                AudioManager.AUDIOFOCUS_GAIN);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem item = menu.add(Menu.NONE, R.id.menu_rotate, Menu.FIRST, R.string.rotate)
                .setIcon(R.drawable.ic_action_device_screen_rotation);
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_rotate) {
            switch (getResources().getConfiguration().orientation) {
                case Configuration.ORIENTATION_PORTRAIT:
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    break;
                case Configuration.ORIENTATION_LANDSCAPE:
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    break;
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private SystemUiHelper.OnVisibilityChangeListener onVisibilityChangeListener = new SystemUiHelper.OnVisibilityChangeListener() {
        @Override
        public void onVisibilityChange(boolean visible) {
            if (getSupportActionBar() != null) {
                if (visible) {
                    showControls();
                    systemUiHelper.delayHide(3000);
                } else {
                    hideControls();
                }
            }
        }
    };

    private void showControls() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().show();
        }

        if (mediaController != null && !(mediaController.isShowing() || mediaController.isShown())) {
            mediaController.show();
        }
    }

    private void hideControls() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        if (mediaController != null && (mediaController.isShowing() || mediaController.isShown())) {
            mediaController.hide();
        }
    }

    AudioManager.OnAudioFocusChangeListener onAudioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        @Override
        public void onAudioFocusChange(int focusChange) {
            if (videoPlayer != null) {
                switch (focusChange) {
                    case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK):
                        // Lower the volume while ducking.
                        videoPlayer.pause();
                        break;
                    case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT):
                        videoPlayer.pause();
                        break;

                    case (AudioManager.AUDIOFOCUS_LOSS):
                        videoPlayer.pause();
                        break;

                    default:
                        break;
                }
            }

        }
    };

}
