package uz.ishow.android.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.OnClick;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uz.ishow.android.R;
import uz.ishow.android.adapter.CommentAdapter;
import uz.ishow.android.api.body.CommentBody;
import uz.ishow.android.api.callback.BaseCallback;
import uz.ishow.android.api.result.AddCommentResult;
import uz.ishow.android.api.result.CommentsResult;
import uz.ishow.android.event.FeedItemEvent;
import uz.ishow.android.model.Comment;
import uz.ishow.android.model.FeedItem;
import uz.ishow.android.view.FakeProgressBar;
import uz.ishow.android.view.util.EndlessRecyclerViewOnScrollListener;
import uz.ishow.android.view.util.SimpleDividerItemDecoration;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 07.07.2015 16:33.
 */
public class CommentsActivity extends BaseActivity {

    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;

    @Bind(R.id.input_divider)
    View inputDivider;

    @Bind(R.id.comment_input_holder)
    View commentInputHolder;

    @Bind(R.id.comment_input)
    EditText commentInput;

    @Bind(R.id.progress_bar)
    ProgressBar progressBar;

    @Bind(R.id.button_send)
    Button buttonSend;

    @Bind(R.id.touch_overlay)
    View touchOverlay;

    @Bind(R.id.no_comments)
    TextView noComments;

    @Bind(R.id.send_progress_bar)
    FakeProgressBar sendProgressBar;

    @BindString(R.string.comments)
    String comments;
    private CommentAdapter mAdapter;
    private boolean commentsLoading;
    private boolean previousLoading;
    private boolean canLoadComments;
    private boolean commentSending;

    private FeedItem feedItem;

    private ActionMode mActionMode;

    private int listHeight;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comments_activity);
        setHomeAsUp();
//        setTitle(R.string.comments);
        activityLabel.setText(comments);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true);
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));

        EndlessRecyclerViewOnScrollListener endlessListener = new EndlessRecyclerViewOnScrollListener(layoutManager, false) {
            @Override
            public void onLoadMore() {
                if (canLoadComments && !commentsLoading && !previousLoading && !commentSending) {
                    getPreviousComments();
                }
            }
        };

        recyclerView.addOnScrollListener(endlessListener);
        sendProgressBar.setProgressDrawables(R.drawable.primary_progress_bar,
                R.drawable.success_progress_bar, R.drawable.error_progress_bar);

        String feedJson = getIntent().getStringExtra("FEED_ITEM");
        try {
            feedItem = gson.fromJson(feedJson, FeedItem.class);
        } catch (JsonSyntaxException e) {
            return;
        }

        if (feedItem.getPost().getIsBlocked()) {
            commentInputHolder.setVisibility(View.GONE);
            inputDivider.setVisibility(View.GONE);
        } else {
            commentInput.requestFocus();

            buttonSend.setEnabled(false);
            commentInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    buttonSend.setEnabled(!TextUtils.isEmpty(s) && !TextUtils.isEmpty(s.toString().trim()));
                }
            });

            commentInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEND) {
                        send();
                        return true;
                    }
                    return false;
                }
            });


        }


        recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (recyclerView.getHeight() != listHeight) {
                    listHeight = recyclerView.getHeight();
                    handler.removeCallbacks(onHeightChangedRunnable);
                    handler.postDelayed(onHeightChangedRunnable, 250);
                }
            }
        });

        getComments();
    }

    private void getComments() {
        commentsLoading = true;
        noComments.setVisibility(View.GONE);
        showProgress();
        api.getComments(feedItem.getPost().getId(), null, new BaseCallback<CommentsResult>() {
            @Override
            public void success(CommentsResult commentResult) {
                if (!isFinishing()) {
                    canLoadComments = commentResult.getCanLoadComments();
                    List<Comment> comments = commentResult.getResults();

                    if (comments == null) {
                        comments = new ArrayList<>();
                    }

                    mAdapter = new CommentAdapter(getApplicationContext(), comments, commentAdapterListener);
                    recyclerView.setAdapter(mAdapter);
                    recyclerView.scrollToPosition(0);

                    if (comments.isEmpty()) {
                        noComments.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void networkError() {
                showNoNetwork();
            }

            @Override
            public void complete() {
                commentsLoading = false;
                hideProgress();
            }
        });
    }

    private CommentAdapter.ItemListener commentAdapterListener = new CommentAdapter.ItemListener() {
        @Override
        public void showActionMode() {
            openActionMode();
        }

        @Override
        public void hideActionMode() {
            closeActionMode();
        }
    };

    private void getPreviousComments() {
        if (mAdapter == null) {
            return;
        }
        Comment lastComment = mAdapter.getLastItem();
        if (lastComment != null && lastComment.getTimeInSeconds() != null) {
            previousLoading = true;
            api.getComments(feedItem.getPost().getId(), lastComment.getTimeInSeconds(), new BaseCallback<CommentsResult>() {
                @Override
                public void success(CommentsResult commentResult) {
                    mAdapter.addAllToEnd(commentResult.getResults());
                    canLoadComments = commentResult.getCanLoadComments();
                }

                @Override
                public void complete() {
                    previousLoading = false;
                }
            });
        }

    }

    private void openActionMode() {
        if (mActionMode != null) {
            mActionMode.invalidate();
            return;
        }

        //noinspection ConstantConditions
        mActionMode = startSupportActionMode(new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                getMenuInflater().inflate(R.menu.menu_comment_action_mode, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                if (mAdapter != null && mAdapter.getSelectedItem() != null) {
                    Comment comment = mAdapter.getSelectedItem();
                    MenuItem menuBlock = menu.findItem(R.id.menu_block);
                    MenuItem menuDelete = menu.findItem(R.id.menu_delete);
                    menuBlock.setVisible(comment.getIsBlockable());
                    menuDelete.setVisible(comment.getIsDeletable());
                }

                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                if (item.getItemId() == R.id.menu_block) {
                    showBlockConfirmDialog();
                } else if (item.getItemId() == R.id.menu_delete) {
                    showDeleteConfirmDialog();
                }
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                if (mAdapter != null) {
                    mAdapter.cancelSelection();
                }

                mActionMode = null;
            }
        });
    }

    private void showBlockConfirmDialog() {
        new AlertDialog.Builder(this, R.style.AppAlertDialogTheme)
                .setTitle(R.string.attention)
                .setMessage(R.string.block_user_confirm)
                .setNegativeButton(R.string.alert_button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        closeActionMode();
                    }
                })
                .setPositiveButton(R.string.alert_button_block, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        blockSelectedItem();
                    }
                })
                .create().show();
    }

    private void showDeleteConfirmDialog() {
        new AlertDialog.Builder(this, R.style.AppAlertDialogTheme)
                .setMessage(R.string.delete_comment_confirm)
                .setNegativeButton(R.string.alert_button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        closeActionMode();
                    }
                })
                .setPositiveButton(R.string.alert_button_delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteSelectedItem();
                    }
                })
                .create().show();
    }

    private void closeActionMode() {
        if (mActionMode != null) {
            mActionMode.finish();
        }
    }

    private void deleteSelectedItem() {
        if (mAdapter != null && mAdapter.getSelectedItem() != null) {
            showProgress();
            api.deleteComment(mAdapter.getSelectedItem().getId(), new BaseCallback<Response>() {
                @Override
                public void success(Response result) {
                    mAdapter.removeSelectedItem();
                    closeActionMode();
                    // TODO use server result instead
                    int commentCount = feedItem.getPost().getCommentsCount() - 1;
                    feedItem.getPost().setCommentsCount(commentCount);
                    postEvent(FeedItemEvent.commentChanged(feedItem.getPost().getId(),
                            commentCount));
                }

                @Override
                public void networkError() {
                    showNoNetwork();
                }

                @Override
                public void complete() {
                    hideProgress();
                }
            });
        } else {
            closeActionMode();
        }
    }

    private void blockSelectedItem() {
        if (mAdapter != null && mAdapter.getSelectedItem() != null) {
            final Comment comment = mAdapter.getSelectedItem();
            showProgress();
            if (comment.getUser() != null && comment.getUser().getId() != null) {
                api.blockUser(comment.getUser().getId(), new BaseCallback<Response>() {
                    @Override
                    public void success(Response result) {
                        mAdapter.cancelSelection();
                        closeActionMode();
                        comment.setIsBlockable(false);
                        Toast.makeText(CommentsActivity.this, R.string.user_blocked_success,
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void networkError() {
                        showNoNetwork();
                    }

                    @Override
                    public void complete() {
                        hideProgress();
                    }

                });

            }
        }
    }

    @Override
    public void onBackPressed() {
        if (mActionMode != null && mAdapter != null) {
            mActionMode.finish();
        } else {
            super.onBackPressed();
        }
    }

    @OnClick(R.id.button_send)
    void send() {
        if (commentSending) {
            return;
        }
        String comment = commentInput.getText().toString().trim();
        if (!comment.isEmpty()) {
            commentSending = true;
            sendProgressBar.startProgress(75f);
            api.addComment(feedItem.getPost().getId(), new CommentBody(comment),
                    new BaseCallback<AddCommentResult>() {
                        @Override
                        public void success(AddCommentResult result) {
                            if (!isFinishing()) {
                                if (result.isSuccess()) {
                                    sendProgressBar.startSuccessProgress();
                                    commentInput.setText("");
                                    noComments.setVisibility(View.GONE);
                                    if (mAdapter != null && result.getData() != null) {
                                        mAdapter.addToStart(result.getData());
                                        recyclerView.scrollToPosition(0);
                                    } else {
                                        getComments();
                                    }

                                    if (result.getCount() != null) {
                                        feedItem.getPost().setCommentsCount(result.getCount());
                                        postEvent(FeedItemEvent.commentChanged(feedItem.getPost().getId(),
                                                result.getCount()));
                                    }
                                } else {
                                    if (!TextUtils.isEmpty(result.getMessage())) {
                                        Toast.makeText(CommentsActivity.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                                    }

                                    sendProgressBar.startErrorProgress();
                                }
                            }
                        }

                        @Override
                        public void networkError() {
                            showNoNetwork();
                        }

                        @Override
                        public void error(RetrofitError e) {
                            sendProgressBar.startErrorProgress();
                            if (e != null && e.getResponse() != null) {
                                int status = e.getResponse().getStatus();
                                if (status == 404) {
                                    onCommentAlreadyDeleted();
                                    finish();
                                }
                            }
                        }

                        @Override
                        public void complete() {
                            commentSending = false;
                        }
                    });
        }
    }

    private void onCommentAlreadyDeleted() {
        Toast.makeText(CommentsActivity.this, R.string.post_deleted, Toast.LENGTH_SHORT).show();
    }

    private Runnable onHeightChangedRunnable = new Runnable() {
        @Override
        public void run() {
            if (recyclerView != null) {
                recyclerView.scrollToPosition(0);
            }
        }
    };

    private void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        if (!isFinishing()) {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void finish() {
        hideKeyboard();
        super.finish();
    }
}
