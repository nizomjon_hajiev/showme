package uz.ishow.android.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import retrofit.RetrofitError;
import uz.droid.dialog.AlertDialogFragment;
import uz.droid.dialog.DialogEventListener;
import uz.fonus.util.BitmapUtils;
import uz.fonus.widget.CountIndicatorEditText;
import uz.ishow.android.R;
import uz.ishow.android.api.SendFileTask;
import uz.ishow.android.api.body.PhotoPostBody;
import uz.ishow.android.api.body.TextPostBody;
import uz.ishow.android.api.callback.BaseCallback;
import uz.ishow.android.api.result.FileUploadResult;
import uz.ishow.android.api.result.PhotoPostResult;
import uz.ishow.android.api.result.TextPostResult;
import uz.ishow.android.event.FeedItemEvent;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 11.08.2015 15:49.
 */
public class CreatePostActivity extends BaseActivity implements DialogEventListener {

    public static final int ON_EXIT_CONFIRM_YES = 1001;

    @Bind(R.id.postImage)
    ImageView postImage;

    @Bind(R.id.postTextInput)
    CountIndicatorEditText postTextInput;

    @Bind(R.id.progress_bar)
    ProgressBar progressBar;

    @Bind(R.id.button_send)
    View buttonSend;

    @Bind(R.id.profile_image)
    ImageView profileImage;

    @Bind(R.id.name)
    TextView profileName;

    @Bind(R.id.times_ago)
    TextView times_ago;

    @Bind(R.id.photo_post_container)
    LinearLayout photoPostContainer;

    @Bind(R.id.postTextContainer)
    View postTextContainer;

    @BindString(R.string.new_post)
    String newPost;
    private boolean hasImage;


    private String imagePath;
    private FileUploadResult fileUploadResult;
    private String resizedImagePath;
    boolean isEmptyPostText = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_post_activity);
        setHomeAsUp();
        activityLabel.setText(newPost);

        userDetails();
//        setTitle(R.string.new_post);

        imagePath = getIntent().getStringExtra("IMAGE_PATH");

        if (!TextUtils.isEmpty(imagePath)) {
            photoPostContainer.setVisibility(View.VISIBLE);
            postTextContainer.setVisibility(View.GONE);
            Bitmap bmp = BitmapUtils.decode(imagePath, 200, 200);
            if (bmp != null) {
                postImage.setVisibility(View.VISIBLE);
                postImage.setImageBitmap(bmp);
                hasImage = true;
            }
        } else {
            postTextContainer.setVisibility(View.VISIBLE);
            photoPostContainer.setVisibility(View.GONE);
        }

        postTextInput.setLimit(500);
        postTextInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    send();
                    return true;
                }
                return false;
            }
        });


        postTextInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                isEmptyPostText = TextUtils.isEmpty(postTextInput.getText().toString());
                invalidateOptionsMenu();
            }
        });
    }

    private void userDetails() {
        ImageLoader.getInstance().displayImage(getUser().getPhotoMini(), profileImage);
        profileName.setText(getUser().getName());
        times_ago.setVisibility(View.GONE);
    }


    @OnClick(R.id.button_send)
    void send() {
        if (!TextUtils.isEmpty(imagePath)) {
            hideKeyboard();
            if (fileUploadResult != null) {
                buttonSend.setEnabled(false);
                showProgress();
                newPhotoPost(fileUploadResult);
            } else {
                if (resizedImagePath == null) {
                    String imageName = "post_image.jpg";
                    if (imagePath.toLowerCase().endsWith(".png")) {
                        imageName = "post_image.png";
                    }
                    File output = new File(getFilesDir(), imageName);
                    boolean isChanged = BitmapUtils.resizeAndExifInvalidate(imagePath, output.getPath(), 1080, 1080);

                    resizedImagePath = output.getPath();
                    if (!isChanged) {
                        resizedImagePath = imagePath;
                    }
                }

                buttonSend.setEnabled(false);
                showProgress();
                new SendFileTask(resizedImagePath, api, "photo") {
                    @Override
                    protected void onProgress(int progress) {
                    }

                    @Override
                    protected void success(FileUploadResult result) {
                        newPhotoPost(result);
                    }

                    @Override
                    protected void error(RetrofitError error) {
                        if (error.getKind() == RetrofitError.Kind.NETWORK) {
                            showNoNetwork();
                        } else {
                            showError(error);
                        }
                        hideProgress();
                        buttonSend.setEnabled(true);
                    }
                }.execute();
            }
        } else {
            newTextPost();
        }
    }

    @Override
    public void showError(RetrofitError error) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppAlertDialogTheme);
        builder.setMessage(R.string.create_post_error)
                .setPositiveButton(R.string.button_ok, null)
                .create().show();
    }

    private void newPhotoPost(FileUploadResult uploadedFile) {
        String text = postTextInput.getText().toString().trim();
        String photo = uploadedFile.getId();
        api.newPhotoPost(new PhotoPostBody(text, photo), new BaseCallback<PhotoPostResult>() {
            @Override
            public void success(PhotoPostResult result) {
                EventBus.getDefault().post(FeedItemEvent.newPostAdded());
                finish();
            }

            @Override
            public void error(RetrofitError e) {
                if (e.getKind() != RetrofitError.Kind.NETWORK) {
                    showError(e);
                }
            }

            @Override
            public void networkError() {
                showNoNetwork();
            }

            @Override
            public void complete() {
                hideProgress();
                buttonSend.setEnabled(true);
            }
        });
    }

    private void newTextPost() {
        String text = postTextInput.getText().toString();
        if (TextUtils.isEmpty(text.trim())) {
            return;
        }

        buttonSend.setEnabled(false);
        hideKeyboard();
        showProgress();
        api.newTextPost(new TextPostBody(text), new BaseCallback<TextPostResult>() {
            @Override
            public void success(TextPostResult result) {
                EventBus.getDefault().post(FeedItemEvent.newPostAdded());
                finish();
            }

            @Override
            public void complete() {
                hideProgress();
                buttonSend.setEnabled(true);
            }

            @Override
            public void error(RetrofitError e) {
                if (e.getKind() != RetrofitError.Kind.NETWORK) {
                    showError(e);
                }
            }

            @Override
            public void networkError() {
                showNoNetwork();
            }
        });
    }

    @Override
    protected boolean interceptBackAction() {
        // show exit confirm popup
        if (hasImage || !TextUtils.isEmpty(postTextInput.getText().toString().trim())) {
            new AlertDialogFragment.Builder(getSupportFragmentManager(), "EXIT_CONFIRM_DIALOG")
                    .setTitle(getString(R.string.create_post_exit_title))
                    .setMessage(getString(R.string.create_post_exit_confirm))
                    .setPositiveText(getString(R.string.yes))
                    .setPositiveClickEventId(ON_EXIT_CONFIRM_YES)
                    .setNegativeText(getString(R.string.no))
                    .show();
            return true;
        }

        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem itemExit = menu.add(Menu.NONE, R.id.menu_post_text, Menu.FIRST, R.string.menu_exit);
        MenuItemCompat.setShowAsAction(itemExit, MenuItemCompat.SHOW_AS_ACTION_ALWAYS);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem itemExit = menu.findItem(R.id.menu_post_text);

        if (itemExit != null) {
            itemExit.setIcon(R.drawable.ic_done_white_18dp);
            itemExit.setEnabled(!isEmptyPostText);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_post_text) {
            send();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDialogEvent(int eventId, Bundle data) {
        if (eventId == ON_EXIT_CONFIRM_YES) {
            finish();
        }
    }

    @OnClick(R.id.add_hash_tags)
    void addHashTags() {

    }

    @OnClick(R.id.add_location)
    void addLocation() {

    }

    private void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

}
