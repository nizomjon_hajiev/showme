package uz.ishow.android.activity;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.digits.sdk.android.AuthCallback;
import com.digits.sdk.android.Digits;
import com.digits.sdk.android.DigitsAuthConfig;
import com.digits.sdk.android.DigitsException;
import com.digits.sdk.android.DigitsOAuthSigning;
import com.digits.sdk.android.DigitsSession;
import com.google.i18n.phonenumbers.AsYouTypeFormatter;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.OnClick;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uz.ishow.android.R;
import uz.ishow.android.api.body.AuthBody;
import uz.ishow.android.api.body.CodeCheckBody;
import uz.ishow.android.api.body.SmsAuthBody;
import uz.ishow.android.api.callback.BaseCallback;
import uz.ishow.android.api.result.AuthResult;
import uz.ishow.android.event.ProfileEvent;
import uz.ishow.android.event.TimelineEvent;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 11.06.2015 14:23.
 */
public class RegisterActivity extends BaseActivity {

    public static final int TICK_WHAT = 1;

    public static final String HEADER_VERIFY_CREDENTIALS = "X-Verify-Credentials-Authorization";
    public static final String HEADER_SERVICE_PROVIDER = "X-Auth-Service-Provider";

    @Bind(R.id.phone_input_container)
    ViewGroup phoneInputContainer;

    @Bind(R.id.code_input_container)
    ViewGroup codeInputContainer;

    @Bind(R.id.message)
    TextView message;

    @Bind(R.id.error_message)
    TextView error;

    @Bind(R.id.name_input)
    EditText nameInput;

    @Bind(R.id.phone_input)
    EditText phoneInput;

    @Bind(R.id.code_input)
    EditText codeInput;

    @Bind(R.id.button_continue)
    Button buttonContinue;

    @Bind(R.id.button_done)
    Button buttonDone;

    @Bind(R.id.progress_bar)
    ProgressBar progressBar;
    @BindString(R.string.register)
    String register;


    private String countryCode;
    private Handler mHandler;
    private long mStartTime;

    private boolean registerWithDigits;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);
        setHomeAsUp();
        activityLabel.setText(register);
        mHandler = new Handler(new Handler.Callback() {
            public boolean handleMessage(Message m) {
                updateTimer();
                return true;
            }
        });

        buttonContinue.setEnabled(false);


        nameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                invalidateButtonState();
            }
        });

        phoneInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                formatPhoneInput(s.toString());
                invalidateButtonState();
            }
        });

        codeInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                buttonDone.setEnabled(!TextUtils.isEmpty(s));
            }
        });
    }

    private void invalidateButtonState() {
        buttonContinue.setEnabled(!TextUtils.isEmpty(nameInput.getText().toString().trim())
                && !TextUtils.isEmpty(phoneInput.getText()));
    }

    // TODO optimize;
    private void formatPhoneInput(@NonNull String phone) {
        if (!phone.startsWith("+") && !TextUtils.isEmpty(phone)) {
            phone = phone.replaceAll("\\+", "").replaceAll("-", "");
            phone = "+" + ("0".equals(phone) ? "" : phone);
            if (phone.length() <= 2) {
                phoneInput.setText(phone);
                Selection.setSelection(phoneInput.getText(), phoneInput.getText().length());
                return;
            }
        }

        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
            Phonenumber.PhoneNumber phoneProto = phoneUtil.parse(phone, null);
            countryCode = phoneUtil.getRegionCodeForCountryCode(phoneProto.getCountryCode());
            AsYouTypeFormatter formatter = phoneUtil.getAsYouTypeFormatter(countryCode);
            formatter.clear();
            String out = phone;
            List<Character> allowed = Arrays.asList('+', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
            for (char digit : phone.toCharArray()) {
                if (allowed.contains(digit)) {
                    out = formatter.inputDigit(digit);
                }
            }

            if (!phone.equals(out)) {
                phoneInput.setText(out);
                Selection.setSelection(phoneInput.getText(), phoneInput.getText().length());
            }
        } catch (NumberParseException ex) {
            ex.printStackTrace();
        }
    }


    @OnClick(R.id.button_continue)
    void onButtonContinueClick() {
        hideKeyboard();
        if ("UZ".equalsIgnoreCase(countryCode)) {
            new AlertDialog.Builder(this, R.style.AppAlertDialogTheme)
                    .setMessage(getString(R.string.phone_confirm_message, phoneInput.getText().toString()))
                    .setNegativeButton(R.string.alert_button_cancel, null)
                    .setPositiveButton(R.string.alert_button_continue, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            register();
                        }
                    }).create().show();
        } else {
            twitterDigitsRegister();
        }
    }


    private void register() {
        registerWithDigits = false;
        buttonContinue.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        api.codeRequest(new SmsAuthBody(nameInput.getText().toString().trim(), getRawPhone()),
                new BaseCallback<Response>() {
                    @Override
                    public void success(Response result) {
                        switchToCodeInputMode();
                    }

                    @Override
                    public void networkError() {
                        showNoNetwork();
                    }

                    @Override
                    public void complete() {
                        progressBar.setVisibility(View.GONE);
                        buttonContinue.setVisibility(View.VISIBLE);
                        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        imm.showSoftInput(codeInput, InputMethodManager.SHOW_IMPLICIT);
                    }
                });

//        api.codeRequestRx(new SmsAuthBody(nameInput.getText().toString().trim(), getRawPhone()))
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Observer<Response>() {
//                    @Override
//                    public void onCompleted() {
//                        complateTask();
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        showNoNetwork();
//                    }
//
//                    @Override
//                    public void onNext(Response response) {
//                        switchToCodeInputMode();
//                    }
//                });


    }

    private void complateTask() {
        progressBar.setVisibility(View.GONE);
        buttonContinue.setVisibility(View.VISIBLE);
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.showSoftInput(codeInput, InputMethodManager.SHOW_IMPLICIT);
    }

    private void twitterDigitsRegister() {
        registerWithDigits = true;
        String phone = phoneInput.getText().toString().replaceAll("\\s", "");
        DigitsAuthConfig config = new DigitsAuthConfig.Builder()
                .withThemeResId(R.style.AppTheme)
                .withAuthCallBack(digitsAuthCallback)
                .withPhoneNumber(phone).build();
        Digits.authenticate(config);
    }

    @OnClick(R.id.button_done)
    void onButtonDoneClick() {
        buttonDone.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        api.smsCodeCheck(new CodeCheckBody(getRawPhone(), codeInput.getText().toString(),
                Build.SERIAL), authApiCallback);
    }

    private void switchToPhoneInputMode() {
        codeInputContainer.setVisibility(View.GONE);
        phoneInputContainer.setVisibility(View.VISIBLE);
    }

    private void switchToCodeInputMode() {
        phoneInputContainer.setVisibility(View.GONE);
        codeInputContainer.setVisibility(View.VISIBLE);
        codeInput.requestFocus();
        message.setText(getString(R.string.code_input_text, phoneInput.getText().toString()));
        buttonDone.setEnabled(false);
        mStartTime = SystemClock.elapsedRealtime();
        updateTimer();
    }


    private void updateTimer() {
        long interval = SystemClock.elapsedRealtime() - mStartTime;
        long sec = interval / 1000;
        long past = 120 - sec;
        if (past <= 0) {
            mHandler.removeMessages(TICK_WHAT);
            switchToPhoneInputMode();
        } else {
            long min = past / 60;
            long pastSec = past % 60;
            buttonDone.setText(getString(R.string.button_done_timer, min, pastSec));
            mHandler.sendMessageDelayed(Message.obtain(mHandler, TICK_WHAT), 1000);
        }
    }


    private void showError() {
        message.setVisibility(View.INVISIBLE);
        error.setVisibility(View.VISIBLE);
    }

    private void hideError() {
        error.setVisibility(View.INVISIBLE);
        message.setVisibility(View.VISIBLE);
    }

    private String getRawPhone() {
        return phoneInput.getText().toString()
                .replaceAll("\\s", "")
                .replaceAll("-", "");
    }

    private AuthCallback digitsAuthCallback = new AuthCallback() {
        @Override
        public void success(DigitsSession session, String s) {
            if (session.getAuthToken() instanceof TwitterAuthToken) {

                TwitterAuthConfig authConfig = TwitterCore.getInstance().getAuthConfig();
                final TwitterAuthToken authToken = (TwitterAuthToken) session.getAuthToken();
                DigitsOAuthSigning oauthSigning = new DigitsOAuthSigning(authConfig, authToken);
                // FOR server
                Map<String, String> authHeaders = oauthSigning.getOAuthEchoHeadersForVerifyCredentials();
                if (authHeaders.containsKey(HEADER_VERIFY_CREDENTIALS) && authHeaders.containsKey(HEADER_SERVICE_PROVIDER)) {
                    api.auth(authHeaders.get(HEADER_VERIFY_CREDENTIALS),
                            authHeaders.get(HEADER_SERVICE_PROVIDER),
                            new AuthBody(phoneInput.getText().toString(),
                                    nameInput.getText().toString().trim(),
                                    Build.SERIAL),
                            authApiCallback
                    );
                }
            }
        }

        @Override
        public void failure(DigitsException error) {
            Toast.makeText(RegisterActivity.this, error.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }
    };

    private BaseCallback<AuthResult> authApiCallback = new BaseCallback<AuthResult>() {
        @Override
        public void success(AuthResult result) {
            saveUser(result.getUser());

            settings.saveAuthToken(result.getToken());
            postEvent(ProfileEvent.loggedIn());
            postEvent(TimelineEvent.reloadMyTimeline());
            setResult(RESULT_OK);
            finish();
        }

        @Override
        public void networkError() {
            showNoNetwork();
        }

        @Override
        public void error(RetrofitError e) {
            Selection.setSelection(codeInput.getText(), codeInput.getText().length());
            buttonDone.setVisibility(View.VISIBLE);
        }

        @Override
        public void apiError(RetrofitError e) {
            if (registerWithDigits) {
                Toast.makeText(getApplicationContext(),
                        R.string.auth_error_message,
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(RegisterActivity.this, R.string.wrong_code, Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void complete() {
            progressBar.setVisibility(View.GONE);
        }
    };

}
