package uz.ishow.android.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;

import uz.fonus.util.ImagePickHelper;
import uz.ishow.android.R;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 22.01.2016 15:26.
 */
public abstract class PickImageActivity extends BaseActivity {

    public static final int REQUEST_READ_STORAGE_PERMISSION = 1;

    private ImagePickHelper mImagePickerHelper;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (mImagePickerHelper != null && mImagePickerHelper.getRequestCode() == requestCode) {
                mImagePickerHelper.onActivityResult(requestCode, resultCode, data);
                if (mImagePickerHelper.isAccessDenied() && Build.VERSION.SDK_INT >= 23
                        && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_DENIED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            REQUEST_READ_STORAGE_PERMISSION);
                } else {
                    onPickedImageReady(mImagePickerHelper.getPickedImageFilePath());
                    mImagePickerHelper = null;
                }
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull
                                           String permissions[],
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_READ_STORAGE_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (mImagePickerHelper != null) {
                        mImagePickerHelper.accessGranted();
                        onPickedImageReady(mImagePickerHelper.getPickedImageFilePath());
                        mImagePickerHelper = null;
                    }
                }
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save image picker for restore (Some times activity removed on open camera)
        if (mImagePickerHelper != null) {
            outState.putBoolean("RESTORE_CREATE_POST_IMAGE_PICKER", true);
            mImagePickerHelper.onSaveInstanceState(outState);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null
                && savedInstanceState.getBoolean("RESTORE_CREATE_POST_IMAGE_PICKER")) {
            mImagePickerHelper = new ImagePickHelper(this, savedInstanceState);
        }
    }

    protected void pickImage(int requestCode) {
        mImagePickerHelper = new ImagePickHelper(this, requestCode, getString(R.string.photo));
        mImagePickerHelper.pick();
    }

    protected abstract void onPickedImageReady(String filePath);
}
