package uz.ishow.android.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.RadioGroup;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.OnClick;
import retrofit.client.Response;
import uz.fonus.gcm.GcmHelper;
import uz.ishow.android.R;
import uz.ishow.android.api.body.LogoutBody;
import uz.ishow.android.api.callback.BaseCallback;
import uz.ishow.android.application.IShowApplication;
import uz.ishow.android.util.IntentUtils;
import uz.ishow.android.util.MediaCenter;
import uz.ishow.android.util.Utils;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 11.11.2015 5:16.
 */
public class AboutActivity extends BaseActivity {

    public static final int RESULT_LOGOUT = 102;

    @Bind(R.id.app_version)
    TextView appVersion;

    @Bind(R.id.about_text_uz)
    TextView aboutTextUz;

    @Bind(R.id.about_text_ru)
    TextView aboutTextRu;

    @Bind(R.id.lang_radio_group)
    RadioGroup langRadioGroup;

    @Bind(R.id.language_label)
    TextView languageLabel;

    @Bind(R.id.our_contacts_label)
    TextView ourContactsLabel;

    @Bind(R.id.our_socials_label)
    TextView ourSocialsLabel;

    @BindString(R.string.about_app)
    String about_app;
    private String lastLang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        setHomeAsUp();
        activityLabel.setText(about_app);


        lastLang = settings.getLanguage();
        fillData();
        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem itemExit = menu.add(Menu.NONE, R.id.menu_exit, Menu.FIRST, R.string.menu_exit);
        MenuItemCompat.setShowAsAction(itemExit, MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem itemExit = menu.findItem(R.id.menu_exit);
        if (itemExit != null) {
            itemExit.setIcon(R.drawable.ic_action_navigation_close);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_exit) {
            new AlertDialog.Builder(this, R.style.AppAlertDialogTheme)
                    .setMessage(R.string.are_you_sure_exit)
                    .setNegativeButton(R.string.alert_button_cancel, null)
                    .setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            logout();
                        }
                    })
                    .create()
                    .show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        GcmHelper gcmHelper = new GcmHelper(IShowApplication.GCM_SENDER_ID, this, null);
        String registerId = gcmHelper.getRegisterKey();
        if (!TextUtils.isEmpty(registerId)) {
            api.logout(new LogoutBody(registerId), new BaseCallback<Response>() {
                @Override
                public void success(Response result) {

                }
            });

//            api.logout(new LogoutBody(registerId))
//                    .subscribeOn(Schedulers.newThread())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(new Action1<Response>() {
//                        @Override
//                        public void call(Response response) {
//
//                        }
//                    });

        }
        MediaCenter.getInstance().closePlayer();
        cacheUtils.clear();
        settings.clearWithoutLanguage();
        setResult(RESULT_LOGOUT);
        finish();
    }

    private void fillData() {
        setTitle(R.string.about_app);
        appVersion.setText(getString(R.string.version, Utils.getVersionName(this), Utils.getVersionCode(this)));
        languageLabel.setText(R.string.language);
        ourContactsLabel.setText(R.string.our_contacts);
        ourSocialsLabel.setText(R.string.our_socials);
    }

    private void init() {
        boolean isUz = "uz".equals(settings.getLanguage());
        langRadioGroup.check(isUz ? R.id.lang_uz : R.id.lang_ru);

        aboutTextUz.setVisibility(isUz ? View.VISIBLE : View.INVISIBLE);
        aboutTextRu.setVisibility(!isUz ? View.VISIBLE : View.INVISIBLE);

        langRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                boolean isLangUz = checkedId == R.id.lang_uz;
                settings.setLanguage(isLangUz ? "uz" : "ru");
                invalidateLocale();
                fillData();
                supportInvalidateOptionsMenu();
                if (isLangUz) {
                    aboutTextRu.startAnimation(getFadeOutAnimation(true));
                    aboutTextUz.startAnimation(getFadeInAnimation());
                } else {
                    aboutTextUz.startAnimation(getFadeOutAnimation(true));
                    aboutTextRu.startAnimation(getFadeInAnimation());
                }
            }
        });
    }

    @OnClick(R.id.phone)
    void call(TextView tv) {
        String phone = tv.getText().toString();
        IntentUtils.openDialer(phone, this);
    }

    @OnClick(R.id.email)
    void email(TextView tv) {
        String email = tv.getText().toString();
        IntentUtils.mailTo(email, this);
    }

    @OnClick(R.id.web)
    void openWeb() {
        IntentUtils.openWeb(getString(R.string.ishow_web), this);
    }

    @OnClick(R.id.button_facebook)
    void openFacebook() {
        IntentUtils.openWeb(getString(R.string.ishow_facebook), this);
    }

    @OnClick(R.id.button_instagram)
    void openInstagram() {
        IntentUtils.openWeb(getString(R.string.ishow_instagram), this);
    }

    private Animation.AnimationListener animationListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            boolean isLangUzbek = langRadioGroup.getCheckedRadioButtonId() == R.id.lang_uz;
            aboutTextUz.setVisibility(isLangUzbek ? View.VISIBLE : View.INVISIBLE);
            aboutTextRu.setVisibility(isLangUzbek ? View.INVISIBLE : View.VISIBLE);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    };

    public AlphaAnimation getFadeOutAnimation(boolean withCallback) {
        AlphaAnimation fadeOutAnimation = new AlphaAnimation(0.75f, 0f);
        fadeOutAnimation.setDuration(250);
        if (withCallback) {
            fadeOutAnimation.setAnimationListener(animationListener);
        }
        return fadeOutAnimation;
    }

    public AlphaAnimation getFadeInAnimation() {
        AlphaAnimation fadeInAnimation = new AlphaAnimation(0f, 1f);
        fadeInAnimation.setDuration(500);
        return fadeInAnimation;
    }

    @Override
    public void finish() {
        if (lastLang != null && !lastLang.equals(settings.getLanguage())) {
            setResult(RESULT_OK);
        }
        super.finish();
    }
}
