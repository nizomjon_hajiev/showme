package uz.ishow.android.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import retrofit.RetrofitError;
import uz.ishow.android.R;
import uz.ishow.android.api.callback.BaseCallback;
import uz.ishow.android.api.result.FollowResult;
import uz.ishow.android.application.IShowApplication;
import uz.ishow.android.event.FeedItemEvent;
import uz.ishow.android.event.TimelineEvent;
import uz.ishow.android.event.TimelineProfileEvent;
import uz.ishow.android.fragment.TimelineFragment;
import uz.ishow.android.model.FeedItem;
import uz.ishow.android.model.Profile;
import uz.ishow.android.util.IntentUtils;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 15.06.2015 17:38.
 */
public class ProfileActivity extends BottomSheetActivity {

    @Bind(R.id.profile_image)
    ImageView profileImage;

    @Bind(R.id.parallaxImage)
    ImageView parallaxImage;

    @Bind(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;

    @Bind(R.id.subscribers)
    TextView followers;

    @Bind(R.id.headerContent)
    View headerContent;

    @Bind(R.id.name)
    TextView nameTextView;

    @Bind(R.id.button_follow)
    Button buttonFollow;

    @Bind(R.id.button_unfollow)
    Button buttonUnfollow;

    @Bind(R.id.bio)
    TextView bio;

    @Bind({R.id.tab_feed, R.id.tab_news, R.id.tab_poll, R.id.tab_catalog, R.id.tab_profile})
    List<View> tabs;

    @Bind(R.id.tab_add)
    View tabAdd;

    @Bind(R.id.tab_poll)
    View tabPoll;


    private Profile mProfile;

    private TimelineFragment timelineFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);
        setHomeAsUp();

        String profileJson = getIntent().getStringExtra("PROFILE");
        mProfile = gson.fromJson(profileJson, Profile.class);

        IShowApplication.setProfile(mProfile);
        IShowApplication.setIsStar(false);
        fillProfileInfo();
        invalidateTabs();

        if (timelineFragment == null) {
            timelineFragment = (TimelineFragment) getSupportFragmentManager().findFragmentByTag("TIMELINE");
            if (timelineFragment == null) {
                timelineFragment = new TimelineFragment();
                Bundle args = new Bundle();
                args.putInt("TYPE", TimelineFragment.TYPE_PROFILE);
                args.putString("PROFILE_ID", mProfile.getId());
                args.putBoolean("CAN_FOLLOW", mProfile.getCanFollow());
                args.putBoolean("IS_FOLLOWING", mProfile.getIsFollowing());
                timelineFragment.setArguments(args);

                getSupportFragmentManager().beginTransaction()
                        .add(R.id.timeline_container, timelineFragment, "TIMELINE").commit();
            }
        }

    }


    public void onEvent(FeedItemEvent event) {
        if (event.getType() == FeedItemEvent.Type.USER_DATA_CHANGED && mProfile != null) {
            Profile user = event.getUser();
            mProfile.setName(user.getName());
            mProfile.setMiniImage(user.getMiniImage());
            mProfile.setGeneralImage(user.getGeneralImage());
            if (event.getProfileImageBitmap() != null) {
                profileImage.setImageBitmap(event.getProfileImageBitmap());
                parallaxImage.setImageBitmap(event.getProfileImageBitmap());
            }
            fillProfileInfo();
        } else if (event.getType() == FeedItemEvent.Type.FOLLOWING) {
            if (mProfile != null && mProfile.getId() != null
                    && mProfile.getId().equals(event.getProfileId())) {
                mProfile.setFollowersCount(event.getFollowersCount());
                invalidateFollowersCountView();
            }
        }
    }

    public void onEvent(TimelineProfileEvent event) {

        if (event.getType() == TimelineProfileEvent.Type.CALL_TO_USER) {
            callToUser();
        } else {
            Profile p = event.getProfile();
            if (mProfile != null && p != null && mProfile.equals(p)) {
                if (!TextUtils.equals(mProfile.getName(), p.getName())
                        || !TextUtils.equals(mProfile.getMiniImage(), p.getMiniImage())
                        || !TextUtils.equals(mProfile.getGeneralImage(), p.getGeneralImage())
                        || !TextUtils.equals(mProfile.getBackgroundImage(), p.getBackgroundImage())
                        ) {
                    mProfile = p;
                    fillProfileInfo();
                }
            }
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_profile_feed, menu);
//        return super.onCreateOptionsMenu(menu);
//    }
//
//    @Override
//    public boolean onPrepareOptionsMenu(Menu menu) {
//        // TODO check with canCall or isOwnProfile
//        MenuItem callItem = menu.findItem(R.id.menu_call);
//        callItem.setVisible(mProfile != null && mProfile.getCanFollow() && !TextUtils.isEmpty(mProfile.getContactPhone()));
//        return super.onPrepareOptionsMenu(menu);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.menu_call: {
//                IntentUtils.openDialer(mProfile.getContactPhone(), this);
//                break;
//            }
//
//            case R.id.menu_refresh: {
//                if (timelineFragment != null) {
//                    timelineFragment.refresh();
//                }
//                break;
//            }
//        }
//        return super.onOptionsItemSelected(item);
//    }

    public void callToUser() {
        IntentUtils.openDialer(mProfile.getContactPhone(), this);
    }


    private void fillProfileInfo() {
        String name = mProfile.getName();
        invalidateFollowersCountView();
        collapsingToolbarLayout.setTitle(name);
        nameTextView.setText(name);

        bio.setText(mProfile.getAbout());
        buttonFollow.setVisibility(!mProfile.getIsFollowing() && mProfile.getCanFollow() ? View.VISIBLE : View.GONE);
        buttonUnfollow.setVisibility(mProfile.getIsFollowing() ? View.VISIBLE : View.GONE);

        ImageLoader.getInstance().displayImage(mProfile.getGeneralImage(), profileImage);
        ImageLoader.getInstance().displayImage(mProfile.getBackgroundImage(), parallaxImage);

        IShowApplication.setProfile(mProfile);
        if (mProfile != null && mProfile.getCanFollow() && !TextUtils.isEmpty(mProfile.getContactPhone())) {
            EventBus.getDefault().post(TimelineProfileEvent.giveOptionToCall(true));
        } else {
            EventBus.getDefault().post(TimelineProfileEvent.giveOptionToCall(false));
        }

    }

    private void invalidateFollowersCountView() {
        int followersCount = mProfile.getFollowersCount();
        String text = followersCount == 0 ? getString(R.string.no_subscribers) :
                getResources().getQuantityString(R.plurals.subscribers, followersCount, followersCount);
        followers.setText(text);
    }

    @OnClick(R.id.tab_add)
    void onAddClick() {
        showAddPostSheet();
    }

    @OnClick(R.id.button_follow)
    void follow() {
        if (settings.isAuthorized()) {
            apiFollow();
        } else {
            auth(new RegisterCompleteListener() {
                @Override
                public void onComplete() {
                    apiFollow();
                }
            });
        }
    }

    @OnClick(R.id.button_unfollow)
    void unfollow() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppAlertDialogTheme);
        builder.setTitle(R.string.unsubscribe_alert_title)
                .setMessage(getString(R.string.unsubscribe_alert_message, mProfile.getName()))
                .setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        showFollowButton(true);
                        api.unfollow(mProfile.getId(), new BaseCallback<FollowResult>() {
                            @Override
                            public void success(FollowResult result) {
                                mProfile.setIsFollowing(false);
                                postEvent(FeedItemEvent.followingChangedToDis(mProfile, mProfile.getId(),
                                        result.getFollowersCount(), false));
                                postEvent(TimelineEvent.reloadMyTimeline());
                            }

                            @Override
                            public void error(RetrofitError e) {
                                showFollowButton(false);
                            }
                        });
                    }
                }).setNegativeButton(R.string.button_close, null)
                .create().show();
    }

    @OnClick({R.id.tab_feed, R.id.tab_news, R.id.tab_poll, R.id.tab_catalog, R.id.tab_profile})
    void onTabClick(final View tab) {
        Intent data = new Intent();
        data.putExtra("SELECTED_TAB_INDEX", tabs.indexOf(tab));
        setResult(RESULT_OK, data);
        supportFinishAfterTransition();
    }

    private void apiFollow() {
        showFollowButton(false);
        // TODO button states, on error
        api.follow(mProfile.getId(), new BaseCallback<FollowResult>() {
            @Override
            public void success(FollowResult result) {
                mProfile.setIsFollowing(true);
                postEvent(FeedItemEvent.followingChanged(mProfile, mProfile.getId(),
                        result.getFollowersCount(), true));
                postEvent(TimelineEvent.reloadMyTimeline());
            }

            @Override
            public void error(RetrofitError e) {
                showFollowButton(true);
            }
        });
    }

    private void showFollowButton(boolean showFollowButton) {
        buttonFollow.setVisibility(showFollowButton ? View.VISIBLE : View.GONE);
        buttonUnfollow.setVisibility(!showFollowButton ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onHeaderClick(FeedItem item) {
        if (!item.getPost().getAuthor().equals(mProfile)) {
            super.onHeaderClick(item);
        }
    }


    private void invalidateTabs() {
        tabAdd.setVisibility(getUser() != null && getUser().getHasProfile() ? View.VISIBLE : View.GONE);
        tabPoll.setVisibility(!settings.isAuthorized() || getUser() == null || !getUser().getHasProfile() ? View.VISIBLE : View.GONE);
    }
}
