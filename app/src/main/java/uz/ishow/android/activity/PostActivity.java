package uz.ishow.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.BindString;
import retrofit.RetrofitError;
import uz.ishow.android.R;
import uz.ishow.android.adapter.FeedAdapter;
import uz.ishow.android.adapter.viewholder.FeedItemViewHolder;
import uz.ishow.android.api.callback.BaseCallback;
import uz.ishow.android.event.AudioEvent;
import uz.ishow.android.event.FeedItemEvent;
import uz.ishow.android.listener.FeedItemListener;
import uz.ishow.android.model.FeedItem;
import uz.ishow.android.model.Post;
import uz.ishow.android.util.MediaCenter;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 18.01.2016 11:42.
 */
public class PostActivity extends BaseActivity {

    @Bind(R.id.post_container)
    ViewGroup postContainer;

    @Bind(R.id.progress_bar)
    ProgressBar progressBar;

    private FeedAdapter mAdapter;

    @BindString(R.string.app_name)
    String appName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        setHomeAsUp();
        activityLabel.setText(appName);
        init();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        init();
    }

    private void init() {
        postContainer.removeAllViews();
        mAdapter = null;
        String json = getIntent().getStringExtra("POST_JSON");
        if (!TextUtils.isEmpty(json)) {
            try {
                Post post = gson.fromJson(json, Post.class);
                if (post != null && post.getId() != null) {
                    showPost(post);
                } else {
                    finish();
                }
            } catch (Exception e) {
                //
                finish();
            }
        } else {
            String postId = getIntent().getStringExtra("POST_ID");
            if (!TextUtils.isEmpty(postId)) {
                showProgress();
                api.getPost(postId, new BaseCallback<Post>() {
                    @Override
                    public void success(Post post) {
                        showPost(post);
                    }

                    @Override
                    public void networkError() {
                        showNoNetwork();
                    }

                    @Override
                    public void error(RetrofitError e) {
                        finish();
                    }

                    @Override
                    public void complete() {
                        hideProgress();
                    }
                });
            } else {
                finish();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_OPEN_PROFILE) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK, data);
                supportFinishAfterTransition();
            }
        }
    }

    public void onEvent(FeedItemEvent event) {
        if (mAdapter != null) {
            mAdapter.onEvent(event);
        }
    }

    @Override
    public void onEvent(AudioEvent event) {
        super.onEvent(event);
        if (mAdapter != null) {
            mAdapter.onEventBackgroundThread(event);
        }
    }

    @Override
    protected void deletePostFromTimeline(Post post) {
        super.deletePostFromTimeline(post);
        supportFinishAfterTransition();
    }

    private void showPost(Post post) {
        List<FeedItem> items = Arrays.asList(new FeedItem(post));
        mAdapter = new FeedAdapter(items, false,false, this, feedItemListener);
        FeedItemViewHolder vh = mAdapter.onCreateViewHolder(postContainer, mAdapter.getItemViewType(0));
        postContainer.removeAllViews();
        postContainer.addView(vh.itemView);
        mAdapter.onBindViewHolder(vh, 0);
    }

    private void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    protected FeedItemListener feedItemListener = new FeedItemListener() {
        @Override
        public void onHeaderClick(FeedItem item) {
            openProfileFeed(item.getPost().getAuthor());
        }

        @Override
        public void onCommentClick(FeedItem item) {
            commentItem(item);
        }

        @Override
        public void like(FeedItem item) {
            PostActivity.this.like(item);
        }

        @Override
        public void unlike(FeedItem item) {
            PostActivity.this.unlike(item);
        }

        @Override
        public void showOptions(FeedItem item) {
            showFeedItemOptions(item);
        }

        @Override
        public void onAudioClick(FeedItem item) {
            MediaCenter mc = MediaCenter.getInstance();
            if (mc.isPlaying(item.getPost())) {
                mc.pauseAudio();
            } else {
                mc.playAudio(item.getPost());
            }
        }

        @Override
        public void openVideo(FeedItem item) {
            PostActivity.this.openVideo(item);
        }

        @Override
        public void downloadSong(FeedItem item) {

        }
    };
}
