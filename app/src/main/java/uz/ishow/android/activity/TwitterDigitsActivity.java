package uz.ishow.android.activity;

import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.Toast;

import com.digits.sdk.android.AuthCallback;
import com.digits.sdk.android.Digits;
import com.digits.sdk.android.DigitsAuthButton;
import com.digits.sdk.android.DigitsException;
import com.digits.sdk.android.DigitsOAuthSigning;
import com.digits.sdk.android.DigitsSession;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;

import java.util.Map;

import butterknife.Bind;
import retrofit.RetrofitError;
import uz.ishow.android.R;
import uz.ishow.android.api.body.AuthBody;
import uz.ishow.android.api.callback.BaseCallback;
import uz.ishow.android.api.result.AuthResult;
import uz.ishow.android.event.ProfileEvent;
import uz.ishow.android.event.TimelineEvent;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 27.07.2015 12:27.
 */
public class TwitterDigitsActivity extends BaseActivity {

    public static final String HEADER_VERIFY_CREDENTIALS = "X-Verify-Credentials-Authorization";
    public static final String HEADER_SERVICE_PROVIDER = "X-Auth-Service-Provider";

    @Bind(R.id.auth_button)
    DigitsAuthButton digitsAuthButton;

    @Bind(R.id.name_input)
    EditText nameInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twitter_digits);
        setTitle(R.string.register);

        Digits.getSessionManager().clearActiveSession();

        digitsAuthButton.setBackgroundResource(R.drawable.custom_btn);
        digitsAuthButton.setTextColor(getResources().getColor(R.color.black));
        digitsAuthButton.setCallback(callback);
        digitsAuthButton.setAuthTheme(R.style.Register);
        digitsAuthButton.setText(R.string.btn_continue);
        digitsAuthButton.setEnabled(false);

        nameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                digitsAuthButton.setEnabled(!TextUtils.isEmpty(s.toString().trim()));
            }
        });
    }

    AuthCallback callback = new AuthCallback() {
        @Override
        public void success(DigitsSession session, String phoneNumber) {
//            Toast.makeText(TwitterDigitsActivity.this, "Authentication Successful for " + phoneNumber, Toast.LENGTH_SHORT).show();

//            userIdView.setText(getString(R.string.user_id, session.getId()));
            if (session.getAuthToken() instanceof TwitterAuthToken) {

                TwitterAuthConfig authConfig = TwitterCore.getInstance().getAuthConfig();
                final TwitterAuthToken authToken = (TwitterAuthToken) session.getAuthToken();
                DigitsOAuthSigning oauthSigning = new DigitsOAuthSigning(authConfig, authToken);
                // FOR server
                Map<String, String> authHeaders = oauthSigning.getOAuthEchoHeadersForVerifyCredentials();
                // TODO get auth token from server
                if (authHeaders.containsKey(HEADER_VERIFY_CREDENTIALS) && authHeaders.containsKey(HEADER_SERVICE_PROVIDER)) {
                    api.auth(authHeaders.get(HEADER_VERIFY_CREDENTIALS),
                            authHeaders.get(HEADER_SERVICE_PROVIDER),
                            new AuthBody(phoneNumber, nameInput.getText().toString().trim(), Build.SERIAL),
                            new BaseCallback<AuthResult>() {
                                @Override
                                public void success(AuthResult result) {
                                    saveUser(result.getUser());
                                    settings.saveAuthToken(result.getToken());
                                    postEvent(ProfileEvent.loggedIn());
                                    if (result.getUser().getHasProfile()) {
                                        postEvent(TimelineEvent.reloadMyTimeline());
                                    }
                                    setResult(RESULT_OK);
                                    finish();
                                }

                                @Override
                                public void error(RetrofitError e) {
                                    Toast.makeText(getApplicationContext(),
                                            R.string.auth_error_message,
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                    );

                }
            }
        }

        @Override
        public void failure(DigitsException error) {
            Toast.makeText(TwitterDigitsActivity.this, error.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }
    };
}
