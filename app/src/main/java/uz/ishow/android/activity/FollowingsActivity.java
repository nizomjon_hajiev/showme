package uz.ishow.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import java.util.List;

import butterknife.Bind;
import butterknife.BindString;
import uz.ishow.android.R;
import uz.ishow.android.adapter.UserListAdapter;
import uz.ishow.android.api.callback.ListCallback;
import uz.ishow.android.event.FeedItemEvent;
import uz.ishow.android.model.Profile;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 01.03.2016 14:04.
 */
public class FollowingsActivity extends BaseActivity {

    @Bind(R.id.progress_bar)
    ProgressBar progressBar;

    @Bind(R.id.no_followings)
    View noFollowingsView;

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindString(R.string.followings)
    String followings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followings);
//        setTitle(R.string.followings);
        setHomeAsUp();
        activityLabel.setText(followings);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        reloadContent();
    }

    public void onEvent(FeedItemEvent event) {
        if (event.getType() == FeedItemEvent.Type.FOLLOWING) {
            reloadContent();
        }
    }

    private void reloadContent() {
        progressBar.setVisibility(View.VISIBLE);

        if (getUser() != null && getUser().getId() != null) {
            api.getFollowings(getUser().getId(), new ListCallback<Profile>() {
                @Override
                public void success(List<Profile> result) {
                    if (!result.isEmpty()) {
                        noFollowingsView.setVisibility(View.GONE);

                        UserListAdapter adapter = new UserListAdapter(FollowingsActivity.this,
                                result, mItemListener);
                        recyclerView.setAdapter(adapter);
                    }
                }

                @Override
                public void emptyList() {
                    noFollowingsView.setVisibility(View.VISIBLE);
                }

                @Override
                public void networkError() {
                    showNoNetwork();
                }

                @Override
                public void complete() {
                    progressBar.setVisibility(View.GONE);
                }
            });

//            api.getFollowers(getUser().getId())
//                    .subscribeOn(Schedulers.newThread())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .doOnNext(new Action1<ListResult<Profile>>() {
//                        @Override
//                        public void call(ListResult<Profile> profileListResult) {
//                            profileListResult.getResults().clear();
//                            if (profileListResult.getResults().isEmpty()) {
//                                noFollowingsView.setVisibility(View.VISIBLE);
//                            }
//                        }
//                    })
//
//
//                    .subscribe(new Observer<ListResult<Profile>>() {
//                        @Override
//                        public void onCompleted() {
//                            progressBar.setVisibility(View.GONE);
//                        }
//
//                        @Override
//                        public void onError(Throwable e) {
//                            if (e instanceof RetrofitError) {
//                                if (((RetrofitError) e).getKind() == RetrofitError.Kind.NETWORK) {
//                                    showNoNetwork();
//                                }
//                            }
//                        }
//
//                        @Override
//                        public void onNext(ListResult<Profile> profileListResult) {
//                            if (!profileListResult.getResults().isEmpty()) {
//                                noFollowingsView.setVisibility(View.GONE);
//
//                                UserListAdapter adapter = new UserListAdapter(FollowingsActivity.this,
//                                        profileListResult.getResults(), mItemListener);
//                                recyclerView.setAdapter(adapter);
//                            }
//                        }
//                    });


        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_OPEN_PROFILE) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK, data);
                supportFinishAfterTransition();
            }
        }
    }

    private UserListAdapter.ItemListener mItemListener = new UserListAdapter.ItemListener() {
        @Override
        public void onItemClick(Profile profile) {
            openProfileFeed(profile);
        }
    };
}
