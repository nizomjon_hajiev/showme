package uz.ishow.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.OnClick;
import uz.ishow.android.R;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 20.08.2015 18:10.
 */
public class IntroActivity extends BaseActivity {

//    @Bind(R.id.introView)
//    IntroView introView;

    @Bind(R.id.lang_radio_group)
    RadioGroup langRadioGroup;

    @Bind(R.id.intro_text_uz)
    TextView introTextUz;

    @Bind(R.id.intro_text_ru)
    TextView introTextRu;

    @Bind(R.id.button_continue)
    Button buttonContinue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        boolean isUz = "uz".equals(settings.getLanguage());
        langRadioGroup.check(isUz ? R.id.lang_uz : R.id.lang_ru);
        buttonContinue.setText(isUz ? R.string.btn_continue_uz : R.string.btn_continue_ru);
        langRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                boolean isLangUz = checkedId == R.id.lang_uz;
                settings.setLanguage(isLangUz ? "uz" : "ru");
                if (isLangUz) {
                    introTextRu.startAnimation(getFadeOutAnimation(true));
                    introTextUz.startAnimation(getFadeInAnimation());
                    buttonContinue.setText(R.string.btn_continue_uz);
                } else {
                    introTextUz.startAnimation(getFadeOutAnimation(true));
                    introTextRu.startAnimation(getFadeInAnimation());
                    buttonContinue.setText(R.string.btn_continue_ru);
                }
            }
        });

    }

    @OnClick(R.id.button_continue)
    void onContinueButtonClick() {
        String langCode = langRadioGroup.getCheckedRadioButtonId() == R.id.lang_uz ? "uz" : "ru";
        settings.setLanguage(langCode);
        startActivity(new Intent(getBaseContext(), MainActivity.class));
        finish();
    }


    private Animation.AnimationListener animationListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            boolean isUzbekText = langRadioGroup.getCheckedRadioButtonId() == R.id.lang_uz;
            introTextUz.setVisibility(isUzbekText ? View.VISIBLE : View.INVISIBLE);
            introTextRu.setVisibility(isUzbekText ? View.INVISIBLE : View.VISIBLE);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    };

    public AlphaAnimation getFadeOutAnimation(boolean withCallback) {
        AlphaAnimation fadeOutAnimation = new AlphaAnimation(0.75f, 0f);
        fadeOutAnimation.setDuration(250);
        if (withCallback) {
            fadeOutAnimation.setAnimationListener(animationListener);
        }
        return fadeOutAnimation;
    }

    public AlphaAnimation getFadeInAnimation() {
        AlphaAnimation fadeInAnimation = new AlphaAnimation(0f, 1f);
        fadeInAnimation.setDuration(500);
        return fadeInAnimation;
    }
}
