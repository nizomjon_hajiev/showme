package uz.ishow.android.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cocosw.bottomsheet.BottomSheet;
import com.google.gson.Gson;

import java.util.Locale;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import uz.fonus.util.ImagePickHelper;
import uz.ishow.android.R;
import uz.ishow.android.api.Api;
import uz.ishow.android.api.callback.BaseCallback;
import uz.ishow.android.api.result.LikeResult;
import uz.ishow.android.application.IShowApplication;
import uz.ishow.android.event.AudioEvent;
import uz.ishow.android.event.FeedItemEvent;
import uz.ishow.android.model.Audio;
import uz.ishow.android.model.FeedItem;
import uz.ishow.android.model.Post;
import uz.ishow.android.model.Profile;
import uz.ishow.android.model.User;
import uz.ishow.android.util.CacheUtils;
import uz.ishow.android.util.MediaCenter;
import uz.ishow.android.util.Settings;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 10.06.2015 15:22.
 */
public class BaseActivity extends AppCompatActivity {

    public static final int REQUEST_REGISTER = 101;
    public static final int REQUEST_PICK_IMAGE = 102;
    public static final int REQUEST_OPEN_PROFILE = 103;
    public static final int REQUEST_SEARCH = 104;
    public static final int REQUEST_PICK_PROFILE_IMAGE = 105;
    public static final int REQUEST_CROP_PICKED_IMAGE = 106;
    public static final int REQUEST_ABOUT_APP = 107;
    public static final int REQUEST_OPEN_CATEGORY_USERS = 108;
    public static final int REQUEST_OPEN_NOMINATION = 109;
    public static final int REQUEST_OPEN_FOLLOWINGS = 110;
    public static final int REQUEST_OPEN_POST = 111;


    public static final int RESULT_OPEN_MY_PROFILE = 201;

    @Nullable
    @Bind(R.id.audio_player)
    View audioPlayer;

    @Nullable
    @Bind(R.id.audio_player_progress_bar)
    ProgressBar audioPlayerProgressBar;

    @Nullable
    @Bind(R.id.audio_player_button_play_pause)
    View audioPlayerButtonPlayPause;

    @Nullable
    @Bind(R.id.musicContainer)
    LinearLayout musicContair;


    @Nullable
    @Bind(R.id.audio_player_title)
    TextView audioPlayerTitle;

    @Nullable
    @Bind(R.id.audio_player_author)
    TextView audioPlayerAuthor;

    @Nullable
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Nullable
    @Bind(R.id.activity_label)
    TextView activityLabel;


    @Inject
    Api api;

    @Inject
    Settings settings;

    @Inject
    Gson gson;

    @Inject
    CacheUtils cacheUtils;

    private User currentUser;

    private RegisterCompleteListener registerCompleteListener;
    private ImagePickHelper mImagePickerHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((IShowApplication) getApplication()).inject(this);
        invalidateLocale();
        EventBus.getDefault().register(this);
    }

    public void onEvent(final AudioEvent event) {
        if (audioPlayer != null) {
            if (event.getType() == AudioEvent.Type.START) {
                Audio audio = event.getAudioPost().getAudio();
                audioPlayer.setVisibility(View.VISIBLE);
                audioPlayerTitle.setText(audio.getTitle());
                audioPlayerAuthor.setText(audio.getArtist());
                audioPlayerButtonPlayPause.setSelected(true);
                audioPlayerProgressBar.setProgress(0);
            } else if (event.getType() == AudioEvent.Type.RESUME) {
                audioPlayerProgressBar.setProgress(event.getProgress());
                audioPlayerButtonPlayPause.setSelected(true);

            } else if (event.getType() == AudioEvent.Type.PROGRESS) {
                audioPlayerProgressBar.setProgress(event.getProgress());

            } else if (event.getType() == AudioEvent.Type.CLOSE_PLAYER) {
                audioPlayer.setVisibility(View.GONE);
            } else if (event.getType() == AudioEvent.Type.PAUSE) {
                audioPlayerButtonPlayPause.setSelected(false);
            } else if (event.getType() == AudioEvent.Type.STOP) {
                audioPlayerProgressBar.setProgress(0);
                audioPlayerButtonPlayPause.setSelected(false);
            }
        }
    }

    @Nullable
    @OnClick(R.id.audio_player_button_close)
    void closePlayer() {
        MediaCenter.getInstance().closePlayer();
    }

    @Nullable
    @OnClick(R.id.audio_player_button_play_pause)
    void playPause(View v) {
        if (v.isSelected()) {
            MediaCenter.getInstance().pauseAudio();
        } else {
            MediaCenter.getInstance().resumeAudio();
        }
    }

    @Nullable
    @OnClick(R.id.audio_player_button_play_forward)
    void btnForward(View v) {
        MediaCenter.getInstance().forwardAudio();
    }

    @Nullable
    @OnClick(R.id.audio_player_button_play_backward)
    void btnBackward(View v) {
        MediaCenter.getInstance().backwardAudio();
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
//        Thread.setDefaultUncaughtExceptionHandler(new UnCaughtException(BaseActivity.this));


        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }

        if (audioPlayer != null) {
            Post audioPost = MediaCenter.getInstance().getAudioPost();
            if (audioPost != null) {
                Audio audio = audioPost.getAudio();
                boolean isPlaying = MediaCenter.getInstance().isPlaying(audioPost);
                audioPlayer.setVisibility(View.VISIBLE);
                audioPlayerTitle.setText(audio.getTitle());
                audioPlayerAuthor.setText(audio.getArtist());
                audioPlayerButtonPlayPause.setSelected(isPlaying);
            }
        }


    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void setHomeAsUp() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    public void like(final FeedItem item) {
        final int originLikeCount = item.getPost().getLikesCount();

        if (!settings.isAuthorized()) {
            auth(new RegisterCompleteListener() {
                @Override
                public void onComplete() {
                    like(item);
                }
            });
        } else {
            // Simulate increment
            int likeCount = item.getPost().getLikesCount() + 1;
            postEvent(FeedItemEvent.likeChanged(item.getPost().getId(), true, likeCount));

            api.like(item.getPost().getId(), new BaseCallback<LikeResult>() {
                @Override
                public void success(LikeResult result) {
                    postEvent(FeedItemEvent.likeChanged(item.getPost().getId(), true, result.getLikesCount()));
                }

                @Override
                public void networkError() {
                    showNoNetwork();
                }


                @Override
                public void error(RetrofitError e) {
                    postEvent(FeedItemEvent.likeChanged(item.getPost().getId(), false, originLikeCount));
                }
            });
        }
    }

    public void unlike(final FeedItem item) {
        final int originLikeCount = item.getPost().getLikesCount();
        // Simulate decrement
        int likeCount = item.getPost().getLikesCount();
        if (likeCount > 0) {
            likeCount--;
        }
        postEvent(FeedItemEvent.likeChanged(item.getPost().getId(), false, likeCount));

        api.unlike(item.getPost().getId(), new BaseCallback<LikeResult>() {
            @Override
            public void success(LikeResult result) {
                postEvent(FeedItemEvent.likeChanged(item.getPost().getId(), false, result.getLikesCount()));
            }

            @Override
            public void networkError() {
                showNoNetwork();
            }

            @Override
            public void error(RetrofitError e) {
                postEvent(FeedItemEvent.likeChanged(item.getPost().getId(), true, originLikeCount));
            }
        });
    }


    public void commentItem(final FeedItem item) {
        if (!settings.isAuthorized()) {
            auth(new RegisterCompleteListener() {
                @Override
                public void onComplete() {
                    openCommentActivity(item);
                }
            });
        } else {
            openCommentActivity(item);
        }
    }

    public void showFeedItemOptions(final FeedItem item) {
        if (item != null && item.getPost() != null && item.getPost().getIsDeletable()) {
            new BottomSheet.Builder(this)
                    .sheet(R.id.menu_delete_post, R.string.delete)
                    .listener(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case R.id.menu_delete_post:
                                    deletePost(item);
                                    break;
                            }
                        }
                    }).show();
        }
    }


    private void deletePost(FeedItem item) {
        final Post post = item.getPost();
        if (post == null || !post.getIsDeletable()) {
            return;
        }

        Post audioPost = MediaCenter.getInstance().getAudioPost();
        if (post.equals(audioPost)) {
            closePlayer();
        }

        showTextProgress(R.string.post_deleting);
        api.deletePost(post.getId(), new BaseCallback<Response>() {
            @Override
            public void success(Response result) {
                deletePostFromTimeline(post);
            }

            @Override
            public void networkError() {
                showNoNetwork();
            }

            @Override
            public void error(RetrofitError e) {
                // Already deleted
                if (e.getKind() != RetrofitError.Kind.NETWORK) {
                    if (e.getResponse() != null && e.getResponse().getStatus() == 404) {
                        deletePostFromTimeline(post);
                    } else {
                        BaseActivity.this.showError(e);
                    }
                }
            }

            @Override
            public void complete() {
                hideTextProgress();
            }
        });
    }

    protected void deletePostFromTimeline(Post post) {
        postEvent(FeedItemEvent.postDeleted(post));
    }

    public void openCommentActivity(FeedItem item) {
        Intent intent = new Intent(getApplicationContext(), CommentsActivity.class);
        intent.putExtra("FEED_ITEM", gson.toJson(item));
        startActivity(intent);
    }

    public void onHeaderClick(FeedItem item) {
        Profile author = item.getPost().getAuthor();
        Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
        intent.putExtra("PROFILE", gson.toJson(author));
        startActivity(intent);
    }

    public void openVideo(FeedItem item) {
        if (item.getPost() != null && item.getPost().getVideo() != null
                && !TextUtils.isEmpty(item.getPost().getVideo().getUrl())) {

            MediaCenter.getInstance().pauseAudio();

            Intent intent = new Intent(this, VideoPlayerActivity.class);
            intent.putExtra("URL", item.getPost().getVideo().getUrl());
            startActivity(intent);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        invalidateLocale();
    }

    protected void invalidateLocale() {
        String lang = settings.getLanguage();
        Configuration conf = getResources().getConfiguration();
        if (!lang.equals(conf.locale.getLanguage())) {
            Locale appLocale = new Locale(lang);
            Locale.setDefault(appLocale);
            conf.locale = appLocale;
            getResources().updateConfiguration(conf, getResources().getDisplayMetrics());
        }
    }

    protected boolean interceptBackAction() {
        return false;
    }

    public void auth(RegisterCompleteListener callback) {
        registerCompleteListener = callback;
        Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
        startActivityForResult(intent, REQUEST_REGISTER);
    }

    @Override
    public void onBackPressed() {
        if (!interceptBackAction()) {
            hideKeyboard();
            super.onBackPressed();
        }
    }

    public interface RegisterCompleteListener {
        void onComplete();
    }

    public void openProfileFeed(Profile profile) {
        if (profile != null) {
            if (getUser() != null && getUser().getHasProfile() &&
                    getUser().getId().equals(profile.getId())) {
                openMyProfile();
            } else {
                Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
                intent.putExtra("PROFILE", gson.toJson(profile));
                startActivityForResult(intent, REQUEST_OPEN_PROFILE);
            }
        }
    }


    /**
     * Must override in extended activities for switch to profile tab;
     */
    protected void openMyProfile() {
        setResult(RESULT_OPEN_MY_PROFILE);
        supportFinishAfterTransition();
    }

    public void showNoNetwork() {
        if (!isFinishing()) {
            Toast.makeText(this, R.string.no_connection, Toast.LENGTH_SHORT).show();
        }
    }

    protected void postEvent(Object eventObject) {
        EventBus.getDefault().post(eventObject);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((IShowApplication) getApplication()).onActivityResume(this);
    }

    @Override
    public void finish() {
        ((IShowApplication) getApplication()).onActivityFinish(this);
        super.finish();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }


    public void showError(RetrofitError error) {
        if (!isFinishing() && error.getResponse() != null &&
                error.getKind() != RetrofitError.Kind.NETWORK) {
            // TODO Debug only
            String response = "";
            try {
                response = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
            } catch (Exception e) {
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppAlertDialogTheme);
            builder.setTitle(String.format("Error %d", error.getResponse().getStatus()));
            builder.setMessage(response);
            builder.setPositiveButton("Close", null);
            builder.create().show();
        }
    }

    private ProgressDialog textProgress;

    public void showTextProgress(@StringRes int textResId) {
        if (textProgress != null) {
            textProgress.hide();
        }
        textProgress = new ProgressDialog(this);
        textProgress.setCancelable(false);
        textProgress.setMessage(getString(textResId));
        textProgress.show();
    }

    public void hideTextProgress() {
        if (textProgress != null && !isFinishing()) {
            textProgress.hide();
        }
    }

    public void saveUser(User user) {
        cacheUtils.saveUser(user);
    }

    public User getUser() {
        return cacheUtils.getUser();
    }

    public void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OPEN_MY_PROFILE) {
            openMyProfile();
        }

        if (requestCode == REQUEST_REGISTER) {
            if (resultCode == RESULT_OK) {
                if (registerCompleteListener != null) {
                    registerCompleteListener.onComplete();
                }
            }
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(IShowApplication.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

//
//    SeekBar.OnSeekBarChangeListener seekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
//        @Override
//        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//            audioPlayerProgressBar.setProgress(progress);
//
//        }
//
//        @Override
//        public void onStartTrackingTouch(SeekBar seekBar) {
//
//        }
//
//        @Override
//        public void onStopTrackingTouch(SeekBar seekBar) {
//            MediaCenter.getInstance().playCurrentPosition(seekBar);
//
//        }
//    };


}
