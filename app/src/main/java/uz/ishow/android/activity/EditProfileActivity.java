package uz.ishow.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import retrofit.RetrofitError;
import uz.fonus.util.BitmapUtils;
import uz.fonus.widget.CircularProgressBar;
import uz.ishow.android.R;
import uz.ishow.android.api.SendFileTask;
import uz.ishow.android.api.body.EditUserBody;
import uz.ishow.android.api.callback.BaseCallback;
import uz.ishow.android.api.result.FileUploadResult;
import uz.ishow.android.event.FeedItemEvent;
import uz.ishow.android.event.ProfileEvent;
import uz.ishow.android.model.Profile;
import uz.ishow.android.model.User;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 22.01.2016 14:52.
 */
public class EditProfileActivity extends PickImageActivity {


    @Bind(R.id.addProfileImageText)
    TextView addProfileImageText;

    @Bind(R.id.name_input)
    EditText nameInput;

    @Bind(R.id.phone_input_container)
    View phoneInputContainer;

    @Bind(R.id.about_input_container)
    View aboutInputContainer;


    @Bind(R.id.phone_input)
    EditText phoneInput;

    @Bind(R.id.about_input)
    EditText aboutInput;

    @Bind(R.id.profile_image)
    ImageView profileImage;

    @Bind(R.id.progress_bar)
    CircularProgressBar progressBar;
    @Bind(R.id.followings)
    TextView followingsT;
    @BindString(R.string.profile)
    String profile;

    @Bind(R.id.name_container)
    LinearLayout nameContainer;

    @Bind(R.id.special_name_container)
    LinearLayout specialNameContainer;

    private boolean isProfileImageChanged;

    private FileUploadResult fileUploadResult;
    private Handler handler = new Handler();
    private boolean forceFillProgress = false;
    private boolean isProfileChanged = false;
    private boolean isValidName = false;
    private boolean isRequestSending = false;
    private boolean isFileUpload = false;

    private User user;
    private Bitmap profileImageBitmap;

    private float progress;
    private int progressIncrement;

    private boolean fromTimeLine = false;
    private Pattern phoneFormatUzb = Pattern.compile("\\+?\\s?998[\\-\\s\\(]?([0-9]{2})\\)?[\\-\\s]?([0-9]{3})[\\-\\s]?([0-9]{2})[\\-\\s]?([0-9]{2})");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        setHomeAsUp();
        activityLabel.setText(profile);

        if (getIntent() != null) {
            fromTimeLine = getIntent().getBooleanExtra("FROM_MY_TIMELINE", false);
        }
        aboutInput.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_UP:
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });

        if (fromTimeLine) {
            nameContainer.setVisibility(View.VISIBLE);
            specialNameContainer.setVisibility(View.GONE);
        }
        nameInput.addTextChangedListener(inputChangeListener);
        phoneInput.addTextChangedListener(inputChangeListener);
        aboutInput.addTextChangedListener(inputChangeListener);

        fillData();

        followingsT.setVisibility(View.GONE);
    }

    public void onEvent(ProfileEvent event) {
        if (event.getType() == ProfileEvent.Type.USER_LOGGED_IN ||
                event.getType() == ProfileEvent.Type.USER_LOGGED_OUT) {
            fillData();
        }
    }

    private void fillData() {

        isProfileChanged = false;
        isProfileImageChanged = false;
        isValidName = false;
        fileUploadResult = null;
        profileImageBitmap = null;

        user = getUser();
        if (user != null) {
            nameInput.setText(user.getName());
            if (user.getHasProfile()) {
                phoneInputContainer.setVisibility(View.VISIBLE);
                aboutInputContainer.setVisibility(View.VISIBLE);

                phoneInput.setText(user.getContactPhone());
                aboutInput.setText(user.getAbout());
            }

            if (user.getPhotoGeneral() != null) {
                ImageLoader.getInstance().displayImage(user.getPhotoGeneral(), profileImage);
                addProfileImageText.setVisibility(View.GONE);
            } else {
                profileImage.setImageDrawable(
                        new ColorDrawable(getResources().getColor(R.color.image_placeholder)));
                addProfileImageText.setVisibility(View.VISIBLE);
            }
        }

        supportInvalidateOptionsMenu();
    }

    private TextWatcher inputChangeListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String name = user.getName() != null ? user.getName() : "";
            String inputName = nameInput.getText().toString().trim();
            isValidName = !TextUtils.isEmpty(inputName);
            boolean nameChanged = !TextUtils.equals(inputName, name);
            boolean phoneChanged = false;
            boolean aboutChanged = false;
            if (user.getHasProfile()) {
                String phone = user.getContactPhone() != null ? user.getContactPhone() : "";
                String about = user.getAbout() != null ? user.getAbout() : "";

                phoneChanged = !TextUtils.equals(phoneInput.getText().toString(), phone);
                aboutChanged = !TextUtils.equals(aboutInput.getText().toString(), about);
            }

            isProfileChanged = nameChanged || phoneChanged || aboutChanged;
            supportInvalidateOptionsMenu();
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        MenuItem item2 = menu.findItem(R.id.menu_cancel);
        if (item2 != null) {
            item2.setVisible(isProfileChanged);
            item2.setEnabled(!isRequestSending);
        }

        MenuItem item = menu.findItem(R.id.menu_save);
        if (item != null) {
            item.setEnabled((isProfileChanged || isProfileImageChanged) && isValidName);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_save: {
                save();
                break;
            }

            case R.id.menu_cancel: {
                hideKeyboard();
                fillData();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean isValidPhone() {
        String phone = phoneInput.getText().toString();
        Matcher matcher = phoneFormatUzb.matcher(phone);
        if (matcher.matches()) {
            StringBuilder sb = new StringBuilder();
            sb.append("+998").append(" ")
                    .append(matcher.group(1)).append(" ")
                    .append(matcher.group(2)).append("-")
                    .append(matcher.group(3)).append("-")
                    .append(matcher.group(4));
            phoneInput.setText(sb.toString());
            return true;
        }
        return false;
    }

    private void save() {
        if (isRequestSending) {
            return;
        }

        if (!TextUtils.isEmpty(phoneInput.getText()) && !isValidPhone()) {
            new AlertDialog.Builder(this, R.style.AppAlertDialogTheme)
                    .setMessage(R.string.wrong_phone_format)
                    .setPositiveButton(R.string.button_close, null)
                    .create().show();
            return;
        }

        isRequestSending = true;
        supportInvalidateOptionsMenu();
        hideKeyboard();
        progress = 0f;
        forceFillProgress = false;
        if (isProfileImageChanged) {
            if (fileUploadResult == null) {
                File file = new File(getFilesDir(), CROP_IMAGE_FILENAME);
                if (file.exists()) {
                    isFileUpload = true;
                    nextProgress();
                    new SendFileTask(file.getPath(), api, "profile") {
                        @Override
                        protected void onProgress(int progress) {
//                            updateFileUploadProgress(progress);
                        }

                        @Override
                        protected void success(FileUploadResult result) {
                            saveUserProfile(result);
                        }

                        @Override
                        protected void networkError() {
                            showNoNetwork();
                        }

                        @Override
                        protected void error(RetrofitError error) {
                            isRequestSending = false;
                            supportInvalidateOptionsMenu();
                            cancelProgress();
                            showError(error);
                        }
                    }.execute();
                } else {
                    isRequestSending = false;
                    cancelProgress();
                    supportInvalidateOptionsMenu();
                }
            } else {
                saveUserProfile(fileUploadResult);
            }
        } else {
            saveUserProfile(null);
        }
    }

    private void saveUserProfile(FileUploadResult fileUploadResult) {
        if (isFinishing()) {
            return;
        }
        this.fileUploadResult = fileUploadResult;
        String name = nameInput.getText().toString().trim();
        String phone = phoneInput.getText().toString();
        String about = aboutInput.getText().toString();

        String photo = fileUploadResult != null ? fileUploadResult.getId() : null;
        forceFillProgress = false;
        isFileUpload = false;
        nextProgress();
        api.editUser(new EditUserBody(name, photo, about, phone), new BaseCallback<User>() {
            @Override
            public void success(User user) {
                if (!isFinishing()) {
                    isProfileImageChanged = false;
                    forceFillProgress = true;
                    progressIncrement = Math.max((int) ((100 - progress) / 12), 1);
                    saveUser(user);

                    if (EditProfileActivity.this.fileUploadResult != null) {
                        ImageLoader.getInstance().loadImage(user.getPhotoMini(), null);
                    }

                    EditProfileActivity.this.fileUploadResult = null;
                    EditProfileActivity.this.user = user;
                    isProfileChanged = false;
                    supportInvalidateOptionsMenu();

                    Profile profile = new Profile();
                    profile.setId(user.getId());
                    profile.setName(user.getName());
                    profile.setMiniImage(user.getPhotoMini());
                    profile.setGeneralImage(user.getPhotoGeneral());
                    profile.setBackgroundImage(user.getPhotoBackground());
                    profile.setAbout(user.getAbout());

                    EventBus.getDefault().post(FeedItemEvent.userDataChanged(profile, profileImageBitmap));
                    profileImageBitmap = null;

                    Toast.makeText(EditProfileActivity.this, R.string.save_success, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void networkError() {
                showNoNetwork();
            }

            @Override
            public void error(RetrofitError e) {
                cancelProgress();
                showError(e);
            }

            @Override
            public void complete() {
                isRequestSending = false;
                supportInvalidateOptionsMenu();
            }
        });

    }

    private void cancelProgress() {
        handler.removeCallbacks(progressBarRunnable);
        progressBar.setProgress(0);
        progressBar.setVisibility(View.GONE);
    }

    @OnClick(R.id.profile_image)
    void changeProfileImage() {
        pickImage(REQUEST_PICK_PROFILE_IMAGE);
    }

    private Uri outputUri;
    private final String CROP_IMAGE_FILENAME = "crop.jpg";

    //create helping method cropCapturedImage(Uri picUri)
    public void cropCapturedImage(Uri picUri) {
        File outputFile = new File(getFilesDir(), CROP_IMAGE_FILENAME);
        if (outputFile.exists()) {
            try {
                outputFile.delete();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        outputUri = Uri.fromFile(outputFile);

        Crop.of(picUri, outputUri)
                .asSquare()
                .withMaxSize(1080, 1080)
                .start(this, REQUEST_CROP_PICKED_IMAGE);

    }

    private void nextProgress() {
        if (progressBar.getProgress() == 100) {
            progressBar.setVisibility(View.GONE);
            progressBar.setProgress(0);
        } else {
            progressBar.setVisibility(View.VISIBLE);
            handler.removeCallbacks(progressBarRunnable);
            handler.postDelayed(progressBarRunnable, 50);
        }
    }

    private Runnable progressBarRunnable = new Runnable() {
        @Override
        public void run() {
            if (forceFillProgress) {
                progress += progressIncrement;
            } else {
                if (isFileUpload) {
                    progress += (64 - progress) / 30f;
                    progress = Math.min(progress, 64);
                } else {
                    progress += (100f - progress) / 30f;
                    progress = Math.min(progress, 100);
                }
            }
            progressBar.setProgress((int) progress);
            nextProgress();
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CROP_PICKED_IMAGE) {
                BitmapUtils.exifInvalidate(outputUri.getPath());
                Bitmap bmp = BitmapUtils.decode(outputUri, 200, 200);
                if (bmp != null) {
                    profileImage.setImageBitmap(bmp);
                    addProfileImageText.setVisibility(View.GONE);
                    isProfileImageChanged = true;
                    isValidName = !TextUtils.isEmpty(nameInput.getText().toString().trim());
                    profileImageBitmap = bmp;
                    supportInvalidateOptionsMenu();
                }
            }
        }
    }

    @Override
    protected void onPickedImageReady(String filePath) {
        cropCapturedImage(Uri.fromFile(new File(filePath)));
    }
}
