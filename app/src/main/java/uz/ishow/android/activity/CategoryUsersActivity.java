package uz.ishow.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import uz.ishow.android.R;
import uz.ishow.android.adapter.StarsAdapter;
import uz.ishow.android.api.callback.ListCallback;
import uz.ishow.android.event.FeedItemEvent;
import uz.ishow.android.model.Profile;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 27.10.2015 14:54.
 */
public class CategoryUsersActivity extends BaseActivity {

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    @Bind(R.id.progress_bar)
    ProgressBar progressBar;

    private StarsAdapter adapter;
    List<Profile> mUsers;
    private final String SEARCHING_CATEGORY_BY_EACH = "searching_category_by_each";
    private final String SEARCHING_TYPE = "searching_type";
    private final String CATEGORY_ID = "category_id";
    String categoryId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_users_activity);
        mUsers = new ArrayList<>();

        setHomeAsUp();
        activityLabel.setText(getIntent().getStringExtra("TITLE"));
//        setTitle();
        categoryId = getIntent().getStringExtra("CATEGORY_ID");

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0) {
                    return 2;
                } else {
                    return 1;
                }
            }
        });
        recyclerView.setLayoutManager(gridLayoutManager);
        progressBar.setVisibility(View.VISIBLE);
        api.getCategoryUsers(categoryId, new ListCallback<Profile>() {
            @Override
            public void success(List<Profile> result) {
                mUsers = result;

                api.getCategoryFamousUsers(categoryId, new ListCallback<Profile>() {
                    @Override
                    public void success(List<Profile> result) {
                        Collections.shuffle(mUsers);
                        adapter = new StarsAdapter(CategoryUsersActivity.this, mUsers, result, onUserClickListener);
                        recyclerView.setAdapter(adapter);
                    }

                    @Override
                    public void emptyList() {

                    }

                    @Override
                    public void networkError() {
                        showNoNetwork();
                    }

                    @Override
                    public void complete() {
                        progressBar.setVisibility(View.GONE);
                    }
                });


            }
        });
    }

    public void onEvent(FeedItemEvent event) {
        if (event.getType() == FeedItemEvent.Type.FOLLOWING) {
            if (adapter != null) {
                adapter.followingChanged(event.getProfileId(), event.getFollowersCount(),
                        event.isFollowing());
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_category, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem searchMenu = menu.findItem(R.id.menu_search);
        if (searchMenu != null) {
            searchMenu.setVisible(true);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_search) {
            Intent intent = new Intent(this, SearchActivity.class);
                intent.putExtra(SEARCHING_TYPE, SEARCHING_CATEGORY_BY_EACH);
                intent.putExtra(CATEGORY_ID, categoryId);
            startActivityForResult(intent, REQUEST_SEARCH);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_OPEN_PROFILE) {
            if (resultCode == RESULT_OK && data != null) {
                setResult(RESULT_OK, data);
                finish();
            }
        }
    }

    private StarsAdapter.OnUserClickListener onUserClickListener = new StarsAdapter.OnUserClickListener() {
        @Override
        public void onUserClick(Profile user) {
            openProfileFeed(user);
        }
    };
}
