package uz.ishow.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import butterknife.Bind;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uz.ishow.android.R;
import uz.ishow.android.adapter.UsersGridAdapter;
import uz.ishow.android.model.CatalogContentResult;
import uz.ishow.android.model.Profile;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 04.07.2015 11:11.
 */
public class UsersActivity extends BaseActivity {

    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.users_activity);
        setHomeAsUp();
//        setTitle(getIntent().getStringExtra("TITLE"));

        activityLabel.setText(getIntent().getStringExtra("TITLE"));
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);


        Long catalogId = getIntent().getLongExtra("CATALOG_ID", -1);
        api.getCatalogUsers(catalogId, new Callback<CatalogContentResult>() {
            @Override
            public void success(CatalogContentResult catalogContentResult, Response response) {
                List<Profile> users = catalogContentResult.getResults();
                UsersGridAdapter adapter = new UsersGridAdapter(getApplicationContext(), users, onUserClickListener);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

    private UsersGridAdapter.OnUserClickListener onUserClickListener = new UsersGridAdapter.OnUserClickListener() {
        @Override
        public void onUserClick(Profile user) {
            Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
            intent.putExtra("USER_ID", user.getId());
            intent.putExtra("NAME", user.getName());
            intent.putExtra("PROFILE_IMAGE", user.getMiniImage());
            intent.putExtra("FOLLOWERS_COUNT", user.getFollowersCount());

            startActivity(intent);
        }
    };
}
