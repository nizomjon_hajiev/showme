package uz.ishow.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;
//import rx.Subscription;
import uz.ishow.android.R;
import uz.ishow.android.adapter.ContentSearchAdapter;
import uz.ishow.android.adapter.UserListAdapter;
import uz.ishow.android.api.callback.ListCallback;
import uz.ishow.android.event.FeedItemEvent;
import uz.ishow.android.model.Post;
import uz.ishow.android.model.Profile;
import uz.ishow.android.util.CacheUtils;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 05.11.2015 12:39.
 */
public class SearchActivity extends BaseActivity {

    @Bind(R.id.search_input)
    EditText searchInput;

    @Bind(R.id.no_results_view)
    View noResultsView;

//    @Bind(R.id.recycler_view)
//    RecyclerView recyclerView;

    @Bind(R.id.result_container)
    LinearLayout resultContainer;

    @Bind(R.id.progress_bar_holder)
    View progressBarHolder;


    @Inject
    CacheUtils cacheUtils;
    private UserListAdapter adapter;

    private ContentSearchAdapter contentSearchAdapter;
    private Handler mHandler = new Handler();

    private boolean isClearVisible = false;
    private final String SEARCHING_TIMELINE = "searching_timeline";
    private final String SEARCHING_TYPE = "searching_type";
    private final String SEARCHING_CATEGORY_BY_EACH = "searching_category_by_each";
    private final String CATEGORY_ID = "category_id";

    private boolean isTimelineSearching = false;
    private boolean isFromCategoryByEach = false;
    private List<Profile> mMyFollowers;
    private String categoryId;

    //    Subscription mSubscription;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);
        setHomeAsUp();


        String type = getIntent().getStringExtra(SEARCHING_TYPE);
        mMyFollowers = new ArrayList<>();

        if (type.equals(SEARCHING_TIMELINE)) {
            isTimelineSearching = true;
        } else if (type.equals(SEARCHING_CATEGORY_BY_EACH)) {
            categoryId = getIntent().getStringExtra(CATEGORY_ID);
            isFromCategoryByEach = true;
        }
        searchInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    search();
                    hideKeyboard();
                    return true;
                }
                return false;
            }
        });


//        mSubscription = RxTextView.textChangeEvents(searchInput)
//                .debounce(400, TimeUnit.MILLISECONDS)
//                .filter(new Func1<TextViewTextChangeEvent, Boolean>() {
//                    @Override
//                    public Boolean call(TextViewTextChangeEvent textViewTextChangeEvent) {
//                        return !TextUtils.isEmpty(searchInput.getText().toString());
//                    }
//                })
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe();
//

        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean isEmpty = TextUtils.isEmpty(s);
                if (isEmpty == isClearVisible) {
                    isClearVisible = !isClearVisible;
                    supportInvalidateOptionsMenu();
                }

                if (!isEmpty) {
                    mHandler.removeCallbacks(searchRunnable);
                    mHandler.postDelayed(searchRunnable, 500);
                } else {
                    clearResult();
                }
            }
        });
    }

    public void onEvent(FeedItemEvent event) {
        if (event.getType() == FeedItemEvent.Type.FOLLOWING) {
            if (adapter != null && adapter.getItemCount() > 0) {
                for (int i = 0; i < adapter.getItemCount(); i++) {
                    Profile p = adapter.getItem(i);
                    if (p != null && p.getId() != null && p.getId().equals(event.getProfileId())) {
                        p.setFollowersCount(event.getFollowersCount());
                        p.setIsFollowing(event.isFollowing());
                    }
                }
            }
        }
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuItem item = menu.add(Menu.NONE, R.id.menu_clear, Menu.FIRST, R.string.clear)
//                .setIcon(R.drawable.ic_action_navigation_close);
//
//        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
//
//        return super.onCreateOptionsMenu(menu);
//    }
//
//    @Override
//    public boolean onPrepareOptionsMenu(Menu menu) {
//        MenuItem item = menu.findItem(R.id.menu_clear);
////        item.setVisible(isClearVisible);
//        return super.onPrepareOptionsMenu(menu);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == R.id.menu_clear) {
//            clearSearch();
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    @OnClick(R.id.result_container)
    void onClickToBack() {
        supportFinishAfterTransition();
    }

    @OnClick(R.id.close_search)
    void closeSearch() {
        clearSearch();
    }

    private void clearSearch() {
        searchInput.setText("");
        clearResult();
//        if (adapter != null) {
//            adapter.clear();
//        }
    }

    private void search() {
        final String keyword = searchInput.getText().toString().trim();
        if (TextUtils.isEmpty(keyword)) {
            return;
        }

        hideNoResults();
        showProgress();
        if (!isTimelineSearching) {
            if (isFromCategoryByEach) {
                api.getCategoryUsers(categoryId, new ListCallback<Profile>() {
                    @Override
                    public void success(List<Profile> result) {
                        if (!result.isEmpty()) {
                            final List<Profile> users = new ArrayList<Profile>();
                            for (int i = 0; i < result.size(); i++) {
                                if (result.get(i).getName().toLowerCase().contains(keyword.toLowerCase())) {
                                    users.add(result.get(i));
                                }
                            }
                            api.getCategoryFamousUsers(categoryId, new ListCallback<Profile>() {
                                @Override
                                public void success(List<Profile> result) {
                                    hideProgress();
                                    if (!result.isEmpty()) {
                                        for (int i = 0; i < result.size(); i++) {
                                            if (result.get(i).getName().toLowerCase().contains(keyword.toLowerCase())) {
                                                users.add(result.get(i));
                                            }
                                        }


                                    }
                                    Set<Profile> set = new HashSet<>();
                                    set.addAll(users);
                                    users.clear();
                                    users.addAll(set);
                                    if (users.size() <= 0) {
                                        showNoResults();
                                    } else {
                                        setSearchResult(users);
                                    }
                                    users.clear();
                                }
                            });


                        }
                    }
                });


            } else {
                api.searchUsers(keyword, new ListCallback<Profile>() {
                    @Override
                    public void success(List<Profile> result) {
                        if (!result.isEmpty()) {
                            setSearchResult(result);
                        }
                    }

                    @Override
                    public void emptyList() {
                        showNoResults();
                    }

                    @Override
                    public void networkError() {
                        showNoNetwork();
                    }

                    @Override
                    public void complete() {
                        hideProgress();
                    }
                });
            }
        } else {
            if (isOnline()) {
                final List<Post> posts = new ArrayList<>();
                api.searchUsers(keyword, new ListCallback<Profile>() {
                    @Override
                    public void success(List<Profile> result) {

                        for (int i = 0; i < result.size(); i++) {
                            Post post = new Post();
                            post.setAuthor(result.get(i));
                            post.setContentView(false);
                            posts.add(post);
                        }
                        setContentSearchList(posts);

                        api.searchByContent(keyword, new ListCallback<Post>() {
                            @Override
                            public void success(List<Post> result) {
                                for (Post post : result) {
                                    post.setContentView(true);
                                    posts.add(post);
                                }
                                setContentSearchList(posts);
                                posts.clear();
                            }

                            @Override
                            public void complete() {
                                super.complete();
                                hideProgress();
                            }
                        });
                    }
                });
            } else {
                searchUsersOffline(keyword);
            }

        }
    }

    private void searchUsersOffline(String keyword) {
        boolean isEmpty = true;
        for (int i = 0; i < cacheUtils.getMyFollowers().size(); i++) {
            if (cacheUtils.getMyFollowers().get(i).getName().toLowerCase().contains(keyword.toLowerCase())) {
                mMyFollowers.add(cacheUtils.getMyFollowers().get(i));
                isEmpty = false;
            }
        }

        Set<Profile> set = new HashSet<>();
        set.addAll(mMyFollowers);
        mMyFollowers.clear();
        mMyFollowers.addAll(set);
        if (isEmpty) {
            showNoResults();
        } else {
            setSearchResult(mMyFollowers);
        }
        mMyFollowers.clear();

    }

    private void showNoResults() {
//        if (adapter != null) {
//            adapter.clear();
//        }
        clearResult();
        noResultsView.setVisibility(View.VISIBLE);
    }

    private void hideNoResults() {
        noResultsView.setVisibility(View.GONE);
    }

    private void showProgress() {
        progressBarHolder.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        progressBarHolder.setVisibility(View.GONE);
    }

    private Runnable searchRunnable = new Runnable() {
        @Override
        public void run() {
            search();
        }
    };

    UserListAdapter.ItemListener itemListener = new UserListAdapter.ItemListener() {
        @Override
        public void onItemClick(Profile profile) {
            openProfileFeed(profile);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_OPEN_PROFILE) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK, data);
                supportFinishAfterTransition();
            }
        }
    }

    private void setSearchResult(List<Profile> profiles) {
        resultContainer.removeAllViews();
        adapter = new UserListAdapter(this, profiles, itemListener);
        if (profiles.size() > 0) {
            for (int i = 0; i < adapter.getItemCount() && i < 32; i++) {
                UserListAdapter.ViewHolder vh = adapter.onCreateViewHolder(resultContainer, adapter.getItemViewType(i));
                resultContainer.addView(vh.itemView);
                adapter.onBindViewHolder(vh, i);
            }
        } else {
            showNoResults();
        }
    }

    private void setContentSearchList(List<Post> posts) {
        resultContainer.removeAllViews();
        contentSearchAdapter = new ContentSearchAdapter(this, posts, itemUser, postItemListener);
        if (posts.size() > 0) {
            for (int i = 0; i < contentSearchAdapter.getItemCount() && i < 32; i++) {
                ContentSearchAdapter.ViewHolder vh = contentSearchAdapter.onCreateViewHolder(resultContainer, contentSearchAdapter.getItemViewType(i));
                resultContainer.addView(vh.itemView);
                contentSearchAdapter.bindViewHolder(vh, i);
            }
        } else {
            showNoResults();
        }
    }

    private void clearResult() {

        resultContainer.removeAllViews();
    }

    ContentSearchAdapter.ItemListener itemUser = new ContentSearchAdapter.ItemListener() {
        @Override
        public void onItemClick(Profile profile) {
            openProfileFeed(profile);
        }
    };

    ContentSearchAdapter.PostItemListener postItemListener = new ContentSearchAdapter.PostItemListener() {
        @Override
        public void onPostItemClick(Post post) {
            Toast.makeText(SearchActivity.this, post.getText(), Toast.LENGTH_SHORT).show();
        }
    };
}
