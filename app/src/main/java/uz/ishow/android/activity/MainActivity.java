package uz.ishow.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.ViewFlipper;

import java.util.List;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.OnClick;
import uz.fonus.gcm.GcmHelper;
import uz.fonus.gcm.GcmListener;
import uz.ishow.android.R;
import uz.ishow.android.api.body.StartupSyncBody;
import uz.ishow.android.api.callback.BaseCallback;
import uz.ishow.android.api.callback.ListCallback;
import uz.ishow.android.api.result.StartupSyncResult;
import uz.ishow.android.application.IShowApplication;
import uz.ishow.android.event.FeedItemEvent;
import uz.ishow.android.event.ProfileEvent;
import uz.ishow.android.fragment.CategoriesFragment;
import uz.ishow.android.fragment.MyProfileFragment;
import uz.ishow.android.fragment.MyTimelineFragment;
import uz.ishow.android.fragment.PollFragment;
import uz.ishow.android.fragment.TimelineFragment;
import uz.ishow.android.model.Profile;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 10.06.2015 15:22.
 */
public class MainActivity extends BottomSheetActivity {

    public static final int TAB_FEED = 0;
    public static final int TAB_NEWS = 1;
    public static final int TAB_POLL = 2;
    public static final int TAB_CATALOG = 3;
    public static final int TAB_PROFILE = 4;

    public static final String FRAGMENT_FEED = "FRAGMENT_FEED";
    public static final String FRAGMENT_NEWS = "FRAGMENT_NEWS";
    public static final String FRAGMENT_POLL = "FRAGMENT_POLL";
    public static final String FRAGMENT_CATEGORIES = "FRAGMENT_CATEGORIES";
    public static final String FRAGMENT_MY_PROFILE = "FRAGMENT_MY_PROFILE";
    public static final String FRAGMENT_MY_TIMELINE = "FRAGMENT_MY_TIMELINE";
    private final String SEARCHING_TIMELINE = "searching_timeline";
    private final String SEARCHING_CATEGORIES = "searching_categories";
    private final String SEARCHING_TYPE = "searching_type";

    @Bind({R.id.tab_feed, R.id.tab_news, R.id.tab_poll, R.id.tab_catalog, R.id.tab_profile})
    List<View> tabs;

    @Bind(R.id.tab_add)
    View tabAdd;

    @Bind(R.id.tab_poll)
    View tabPoll;

    @Bind(R.id.view_flipper)
    ViewFlipper viewFlipper;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.activity_label)
    TextView activityLabel;

//    @Bind(R.id.app_icon)
//    View appIcon;

    @BindString(R.string.feed)
    String feed;

    @BindString(R.string.best)
    String best;

    @BindString(R.string.vote)
    String vote;

    @BindString(R.string.catalog)
    String catalog;

    @BindString(R.string.profile)
    String profile;


    @Nullable
    @Bind(R.id.appBarLayout)
    AppBarLayout mAppBarLayout;

    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout mCoordinatorLayout;

    private TimelineFragment feedFragment;
    private TimelineFragment newsFragment;
    private PollFragment pollFragment;
    private CategoriesFragment categoriesFragment;
    private MyProfileFragment myProfileFragment;
    private MyTimelineFragment myTimelineFragment;

    private int mCurrentTabIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (settings.showIntro()) {
            startActivity(new Intent(this, IntroActivity.class));
            finish();
            return;
        }

        setContentView(R.layout.main_activity);

        invalidateTabs();
        activityLabel.setText(feed);

        if (savedInstanceState != null) {
            mCurrentTabIndex = savedInstanceState.getInt("CURRENT_TAB");
        }
        onTabClick(tabs.get(mCurrentTabIndex));
        syncStartupData();
        getFollowedUsers();

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        setSupportActionBar(toolbar);
    }

    private void invalidateTabs() {
        tabAdd.setVisibility(getUser() != null && getUser().getHasProfile() ? View.VISIBLE : View.GONE);
        tabPoll.setVisibility(!settings.isAuthorized() || getUser() == null || !getUser().getHasProfile() ? View.VISIBLE : View.GONE);
    }

    public void onEvent(ProfileEvent event) {
        if (event.getType() == ProfileEvent.Type.USER_LOGGED_IN ||
                event.getType() == ProfileEvent.Type.USER_LOGGED_OUT) {
            invalidateTabs();
            supportInvalidateOptionsMenu();
        }
    }

    @Override
    protected void openMyProfile() {
        onTabClick(tabs.get(TAB_PROFILE));
        scrollToTop(tabs.get(TAB_PROFILE));
    }

    public void onEvent(FeedItemEvent event) {
        if (event.getType() == FeedItemEvent.Type.NEW_POST_ADDED) {
            if (viewFlipper.getDisplayedChild() != TAB_FEED) {
                onTabClick(tabs.get(TAB_FEED));
            }
            scrollToTop(tabs.get(TAB_FEED));
        }
    }

    private void getFollowedUsers() {

        if (getUser() != null && getUser().getId() != null) {
            api.getFollowings(getUser().getId(), new ListCallback<Profile>() {
                @Override
                public void success(List<Profile> result) {
                    if (!result.isEmpty()) {
                        cacheUtils.saveMyFollowers(result);

                    }
                }

                @Override
                public void emptyList() {
//                        noFollowingsView.setVisibility(View.VISIBLE);
                }

                @Override
                public void networkError() {
                    showNoNetwork();
                }

                @Override
                public void complete() {
//                        progressBar.setVisibility(View.GONE);
                }
            });
        }

    }

    private void syncStartupData() {
        if (settings.isAuthorized()) {
            GcmHelper gcmHelper = new GcmHelper(IShowApplication.GCM_SENDER_ID, this, new GcmListener() {
                @Override
                public void onDeviceNotSupport() {
                }

                @Override
                public void onPlayServicesNotFound() {
                }

                @Override
                public void onSendToBackend(String regId) {
                    syncStartupData(regId);
                }
            });

            if (gcmHelper.isRegisterRequired()) {
                gcmHelper.register();
                syncStartupData(null);
            } else {
                syncStartupData(gcmHelper.getRegisterKey());
            }
        }
    }

    private void syncStartupData(String gcmRegId) {
        api.syncStartupData(new StartupSyncBody(gcmRegId), new BaseCallback<StartupSyncResult>() {
            @Override
            public void success(StartupSyncResult result) {
                if (result.getUser() != null) {
                    saveUser(result.getUser());
                    invalidateTabs();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem searchMenu = menu.findItem(R.id.menu_search);
        if (searchMenu != null) {
        }

        MenuItem menuVote = menu.findItem(R.id.menu_vote);
        if (menuVote != null) {
            menuVote.setVisible(getUser() != null && getUser().getHasProfile()
                    && viewFlipper.getDisplayedChild() == TAB_FEED);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_search) {
            Intent intent = new Intent(this, SearchActivity.class);
            if (viewFlipper.getDisplayedChild() == TAB_CATALOG) {
                intent.putExtra(SEARCHING_TYPE, SEARCHING_CATEGORIES);
            } else {
                intent.putExtra(SEARCHING_TYPE, SEARCHING_TIMELINE);
            }
            startActivityForResult(intent, REQUEST_SEARCH);
            return true;
        } else if (item.getItemId() == R.id.menu_vote) {
            onTabClick(tabs.get(TAB_POLL));
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.tab_feed, R.id.tab_news, R.id.tab_poll, R.id.tab_catalog, R.id.tab_profile})
    void onTabClick(final View tab) {
        if (tab.isSelected()) {
            scrollToTop(tab);
            return;
        }

        if (tab.getId() == R.id.tab_profile && !settings.isAuthorized()) {
            auth(null);
            return;
        }

        mCurrentTabIndex = tabs.indexOf(tab);
        for (View tb : tabs) {
            tb.setSelected(tab == tb);
        }

        if (toolbar != null) {
            if (tab.getId() == R.id.tab_profile) {
                toolbar.setVisibility(View.GONE);
            } else {
                toolbar.setVisibility(View.VISIBLE);
            }
        }

        FragmentManager fm = getSupportFragmentManager();
        switch (tab.getId()) {
            case R.id.tab_feed: {

                if (feedFragment == null) {

                    Fragment fragment = fm.findFragmentByTag(FRAGMENT_FEED);
                    if (fragment != null) {
                        feedFragment = (TimelineFragment) fragment;
                    } else {
                        feedFragment = new TimelineFragment();
                        Bundle b = new Bundle();
                        b.putInt("TYPE", TimelineFragment.TYPE_OWN);
                        feedFragment.setArguments(b);
                        fm.beginTransaction().add(R.id.feed_container, feedFragment, FRAGMENT_FEED).commit();
                    }
                }
                viewFlipper.setDisplayedChild(TAB_FEED);
                activityLabel.setText(feed);
                break;
            }

            case R.id.tab_news: {
                if (newsFragment == null) {

                    Fragment fragment = fm.findFragmentByTag(FRAGMENT_NEWS);
                    if (fragment != null) {
                        newsFragment = (TimelineFragment) fragment;
                    } else {
                        newsFragment = new TimelineFragment();
                        Bundle b = new Bundle();
                        b.putInt("TYPE", TimelineFragment.TYPE_NEWS);
                        newsFragment.setArguments(b);
                        fm.beginTransaction()
                                .add(R.id.all_news_container, newsFragment, FRAGMENT_NEWS).commit();
                    }
                }

                viewFlipper.setDisplayedChild(TAB_NEWS);
                activityLabel.setText(best);
                break;
            }

            case R.id.tab_poll: {
                if (pollFragment == null) {
                    Fragment fragment = fm.findFragmentByTag(FRAGMENT_POLL);
                    if (fragment != null) {
                        pollFragment = (PollFragment) fragment;
                    } else {
                        pollFragment = new PollFragment();
                        fm.beginTransaction()
                                .add(R.id.poll_container, pollFragment, FRAGMENT_POLL).commit();
                    }
                }

                viewFlipper.setDisplayedChild(TAB_POLL);
                showToolbar();
                activityLabel.setText(vote);
                break;
            }

            case R.id.tab_catalog: {
                if (categoriesFragment == null) {


                    Fragment fragment = fm.findFragmentByTag(FRAGMENT_CATEGORIES);
                    if (fragment != null) {
                        categoriesFragment = (CategoriesFragment) fragment;
                    } else {
                        categoriesFragment = new CategoriesFragment();
                        fm.beginTransaction()
                                .add(R.id.categories_container, categoriesFragment, FRAGMENT_CATEGORIES)
                                .commit();
                    }
                }
                viewFlipper.setDisplayedChild(TAB_CATALOG);
                showToolbar();
                activityLabel.setText(catalog);
                break;
            }

            case R.id.tab_profile: {
                if (getUser().getHasProfile()) {
                    if (myTimelineFragment == null) {
                        Fragment fragment = fm.findFragmentByTag(FRAGMENT_MY_TIMELINE);
                        if (fragment != null) {
                            myTimelineFragment = (MyTimelineFragment) fragment;
                        } else {
                            myTimelineFragment = new MyTimelineFragment();
                            fm.beginTransaction()
                                    .add(R.id.my_profile_container, myTimelineFragment, FRAGMENT_MY_TIMELINE)
                                    .commit();
                        }
                    }
                } else {
                    if (myProfileFragment == null) {

                        Fragment fragment = fm.findFragmentByTag(FRAGMENT_MY_PROFILE);
                        if (fragment != null) {
                            myProfileFragment = (MyProfileFragment) fragment;
                        } else {
                            myProfileFragment = new MyProfileFragment();
                            fm.beginTransaction()
                                    .add(R.id.my_profile_container, myProfileFragment, FRAGMENT_MY_PROFILE)
                                    .commit();
                        }
                    }
                }
                viewFlipper.setDisplayedChild(TAB_PROFILE);
                IShowApplication.setIsStar(true);
                showToolbar();
                activityLabel.setText(profile);
                break;
            }
        }

        supportInvalidateOptionsMenu();
    }

    private void showToolbar() {
        CoordinatorLayout.LayoutParams params = null;
        if (mAppBarLayout != null) {
            params = (CoordinatorLayout.LayoutParams) mAppBarLayout.getLayoutParams();
        }
        AppBarLayout.Behavior behavior = null;
        if (params != null) {
            behavior = (AppBarLayout.Behavior) params.getBehavior();
        }
        if (mCoordinatorLayout != null) {
            if (behavior != null) {
                behavior.onNestedFling(mCoordinatorLayout, mAppBarLayout, null, 0, -1000, true);
            }
        }
    }

    private void scrollToTop(View tab) {
        switch (tab.getId()) {
            case R.id.tab_feed: {
                if (feedFragment != null) {
                    feedFragment.scrollToTop();
                }
                break;
            }

            case R.id.tab_news: {
                if (newsFragment != null) {
                    newsFragment.scrollToTop();
                }
                break;
            }

            case R.id.tab_catalog: {
                if (categoriesFragment != null) {
                    categoriesFragment.scrollToTop();
                }
                break;
            }

            case R.id.tab_profile: {
                if (myTimelineFragment != null) {
                    myTimelineFragment.scrollToTop();
                }
                break;
            }
        }
    }

    public int getCurrentTabIndex() {
        return viewFlipper.getDisplayedChild();
    }

    @OnClick(R.id.tab_add)
    void onAddClick() {
        showAddPostSheet();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_OPEN_PROFILE || requestCode == REQUEST_SEARCH
                || requestCode == REQUEST_OPEN_CATEGORY_USERS
                || requestCode == REQUEST_OPEN_NOMINATION
                || requestCode == REQUEST_OPEN_FOLLOWINGS
                || requestCode == REQUEST_OPEN_POST) {
            if (resultCode == RESULT_OK && data != null && data.hasExtra("SELECTED_TAB_INDEX")) {
                mCurrentTabIndex = data.getIntExtra("SELECTED_TAB_INDEX", 0);
                for (int i = 0; i < tabs.size(); i++) {
                    View tab = tabs.get(i);
                    if (i == mCurrentTabIndex && !tab.isSelected()) {
                        onTabClick(tab);
                        break;
                    }
                }
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (viewFlipper != null) {
            outState.putInt("CURRENT_TAB", viewFlipper.getDisplayedChild());
        }
    }

    @Override
    public void finish() {
        closePlayer();
        super.finish();
    }
}
