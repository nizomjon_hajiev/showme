package uz.ishow.android.listener;

import uz.ishow.android.model.FeedItem;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 04.08.2015 12:17.
 */
public interface FeedItemListener {
    void onHeaderClick(FeedItem item);

    void onCommentClick(FeedItem item);

    void onAudioClick(FeedItem item);

    void like(FeedItem item);

    void unlike(FeedItem item);

    void showOptions(FeedItem item);

    void openVideo(FeedItem item);

    void downloadSong(FeedItem item);
}
