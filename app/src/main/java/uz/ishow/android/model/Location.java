package uz.ishow.android.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by User on 17.06.2016.
 */
public class Location {

    @SerializedName("predictions")
    List<Predictions> mPredictionses;

    public List<Predictions> getPredictionses() {
        return mPredictionses;
    }

    public void setPredictionses(List<Predictions> predictionses) {
        mPredictionses = predictionses;
    }
}


