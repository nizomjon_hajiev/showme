package uz.ishow.android.model;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 19.06.2015 15:06.
 */
public class Photo {

    private String url;
    private Integer width;
    private Integer height;

    public String getUrl() {
        return url;
    }

    public Integer getWidth() {
        return width != null ? width : 0;
    }

    public Integer getHeight() {
        return height != null ? height : 0;
    }
}
