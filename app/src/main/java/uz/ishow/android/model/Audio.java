package uz.ishow.android.model;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 19.06.2015 15:06.
 */
public class Audio {

    private String url;
    private Integer duration;
    private String title;
    private String artist;
    private Artwork artwork;
    private String readableDuration;
    private boolean isDownloaded;

    public String getUrl() {
        return url;
    }

    public Integer getDuration() {
        return duration;
    }

    public String getHumanReadableDuration() {
        if (duration == null) {
            return "00:00";
        }

        int dur = this.duration;

        int seconds = dur % 60;
        dur /= 60;
        int minutes = dur % 60;
        dur /= 60;
        int hours = dur;

        if (hours == 0) {
            return String.format("%1$d:%2$02d", minutes, seconds);
        } else {
            return String.format("%1$d:%2$02d:%3$02d", hours, minutes, seconds);
        }
    }

    public String getArtist() {
        return artist;
    }

    public String getTitle() {
        return title;
    }

    public String getCover() {
        return artwork != null ? artwork.url : null;
    }

    private class Artwork {
        private String url;
    }


    public String getReadableDuration() {
        return readableDuration;
    }

    public void setReadableDuration(String readableDuration) {
        this.readableDuration = readableDuration;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isDownloaded() {
        return isDownloaded;
    }

    public void  setIsDownloaded(boolean isDownloaded) {
        this.isDownloaded = isDownloaded;
    }
}
