package uz.ishow.android.model;

import android.text.TextUtils;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 22.09.2015 17:36.
 */
public class ProfilePhoto {

    private String url;
    private String background;
    private String mini;
    private String general;


    public String getBackground() {
        return background;
    }

    public String getUrl() {
        return url;
    }

    public String getMini() {
        if (TextUtils.isEmpty(mini) && !TextUtils.isEmpty(url)) {
            return url;
        }
        return mini;
    }

    public void setMini(String mini) {
        this.mini = mini;
    }

    public String getGeneral() {
        return general;
    }

    public void setGeneral(String general) {
        this.general = general;
    }

    public void setBackground(String background) {
        this.background = background;
    }
}
