package uz.ishow.android.model;

import java.util.Date;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 02.07.2015 17:40.
 */
public class FeedItem {

    private String id;
    private String type;

    private Long time;
    private Post post;

    public FeedItem() {
    }

    public FeedItem(Post post) {
        this.post = post;
        this.type = post.getType();
    }

    public boolean isTypeText() {
        return Post.TEXT.equals(type);
    }

    public boolean isTypePhoto() {
        return Post.PHOTO.equals(type);
    }

    public boolean isTypeAudio() {
        return Post.AUDIO.equals(type);
    }

    public boolean isTypeVideo() {
        return Post.VIDEO.equals(type);
    }

    public boolean isTypeSpacer() {
        return type == null;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    /**
     * @deprecated Use {@link Post#getPublishedTime()} instead.
     */
    @Deprecated
    public Date getTime() {
        if (time == null) {
            return null;
        }
        return new Date(time * 1000);
    }

    public Long getTimeInSeconds() {
        return time;
    }

    public Post getPost() {
        return post;
    }

    public boolean postIdEquals(String postId) {
        return post != null && post.getId() != null && post.getId().equals(postId);
    }

    public boolean postAuthorIdEquals(String profileId) {
        return post != null && post.getAuthor() != null && post.getAuthor().getId() != null
                && post.getAuthor().getId().equals(profileId);
    }

}
