package uz.ishow.android.model;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 23.09.2015 10:48.
 */
public class Profile {

    private String id;
    private String name;
    private String about;
    private String contactPhone;
    private Boolean isPremium;
    private ProfilePhoto photo;
    private Boolean canFollow;
    private Integer followersCount;
    private Boolean isFollowing;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String phone) {
        this.contactPhone = phone;
    }

    public Boolean getIsPremium() {
        return isPremium != null && isPremium;
    }

    public ProfilePhoto getPhoto() {
        return photo;
    }

    public Boolean getCanFollow() {
        return canFollow != null && canFollow;
    }

    public Integer getFollowersCount() {
        return followersCount != null ? followersCount : 0;
    }

    public void setFollowersCount(Integer followersCount) {
        this.followersCount = followersCount;
    }

    public Boolean getIsFollowing() {
        return isFollowing != null && isFollowing;
    }

    public void setIsFollowing(Boolean isFollowing) {
        this.isFollowing = isFollowing;
    }

    public String getMiniImage() {
        return photo != null ? photo.getMini() : null;
    }

    public void setMiniImage(String miniUrl) {
        if (photo == null) {
            photo = new ProfilePhoto();
        }
        photo.setMini(miniUrl);
    }

    public String getGeneralImage() {
        return photo != null ? photo.getGeneral() : null;
    }

    public void setGeneralImage(String generalImageUrl) {
        if (photo == null) {
            photo = new ProfilePhoto();
        }
        photo.setGeneral(generalImageUrl);
    }

    public void setBackgroundImage(String backgroundImageUrl) {
        if (photo == null) {
            photo = new ProfilePhoto();
        }
        photo.setBackground(backgroundImageUrl);
    }

    public String getBackgroundImage() {
        return photo != null ? photo.getBackground() : null;
    }

    @Override
    public boolean equals(Object o) {
        return o != null && o instanceof Profile && id != null && id.equals(((Profile) o).getId());
    }
}
