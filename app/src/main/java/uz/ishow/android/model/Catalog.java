package uz.ishow.android.model;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 03.07.2015 17:32.
 */
public class Catalog {

    private Long id;
    private String title;
    private String icon;
    private Integer count;


    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getIcon() {
        if (!icon.startsWith("http") || !icon.startsWith("assets")) {
            return "assets://sample/" + icon;
        }
        return icon;
    }

    public Integer getCount() {
        return count;
    }
}
