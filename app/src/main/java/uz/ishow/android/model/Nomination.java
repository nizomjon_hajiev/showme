package uz.ishow.android.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 21.02.2016 14:34.
 */
public class Nomination {

    private String id;
    private String name;
    private String nameRu;
    private String finalTitle;
    private String finalTitleRu;
    private String description;
    private String descriptionRU;
    private Boolean voted;
    private String status;

    @SerializedName("voted_contributor")
    private String votedNomineeId;

    @SerializedName("contributors")
    private List<Nominee> nominees;

    private WinnerNominee winner;

    private String type;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNameRu() {
        return nameRu;
    }

    public String getFinalTitle() {
        return finalTitle;
    }

    public String getFinalTitleRu() {
        return finalTitleRu;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionRU() {
        return descriptionRU;
    }

    public boolean isActive() {
        return "active".equals(status);
    }

    public boolean isTypeAudio() {
        return "audio".equals(type);
    }

    public boolean isTypeVideo() {
        return "video".equals(type);
    }

    public boolean isTypePhoto() {
        return "photo".equals(type);
    }

    public boolean isTypeText() {
        return "text".equals(type);
    }

    public boolean isTypeUser() {
        return "user".equals(type);
    }

    public List<Nominee> getNominees() {
        return nominees;
    }

    public WinnerNominee getWinner() {
        return winner;
    }

    public String getVotedNomineeId() {
        return votedNomineeId;
    }
}
