package uz.ishow.android.model;

/**
 * Created by User on 02.04.2016.
 */
public class AudioOffline {
    public String audio_id;
    public String audio_name;
    public String audio_title;
    public String audio_duration;

    public byte[] audio_image;
    public String audio_path;

    public byte[] getAudio_image() {
        return audio_image;
    }

    public void setAudio_image(byte[] audio_image) {
        this.audio_image = audio_image;
    }


    public String getAudio_id() {
        return audio_id;
    }

    public void setAudio_id(String audio_id) {
        this.audio_id = audio_id;
    }

    public String getAudio_name() {
        return audio_name;
    }

    public void setAudio_name(String audio_name) {
        this.audio_name = audio_name;
    }

    public String getAudio_title() {
        return audio_title;
    }

    public void setAudio_title(String audio_title) {
        this.audio_title = audio_title;
    }

    public String getAudio_duration() {
        return audio_duration;
    }

    public void setAudio_duration(String audio_duration) {
        this.audio_duration = audio_duration;
    }

    public String getAudio_path() {
        return audio_path;
    }

    public void setAudio_path(String audio_path) {
        this.audio_path = audio_path;
    }


}
