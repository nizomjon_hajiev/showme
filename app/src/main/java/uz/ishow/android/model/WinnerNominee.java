package uz.ishow.android.model;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 22.02.2016 8:15.
 */
public class WinnerNominee extends Nominee {

    private String nominationName;

    public String getNominationName() {
        return nominationName;
    }

    public void setNominationName(String nominationName) {
        this.nominationName = nominationName;
    }
}
