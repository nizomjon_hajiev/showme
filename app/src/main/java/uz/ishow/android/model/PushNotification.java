package uz.ishow.android.model;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 16.01.2016 15:27.
 */
public class PushNotification {

    private String title;
    private String postId;
    private String targetId;
    private String type;

    public PushNotification() {
    }

    public PushNotification(String title, String postId, String targetId, String type) {
        this.title = title;
        this.postId = postId;
        this.targetId = targetId;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public String getPostId() {
        return postId;
    }

    public String getTargetId() {
        return targetId;
    }

    public String getType() {
        return type;
    }

    public boolean isTypeComment() {
        return "comment".equals(type);
    }

    public boolean isTypeLike() {
        return "like".equals(type);
    }

    public boolean isTypeFollow() {
        return "follow".equals(type);
    }

    @Override
    public int hashCode() {
        return ("" + postId + targetId + type).hashCode();
    }
}
