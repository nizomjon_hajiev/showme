package uz.ishow.android.model;

import java.util.List;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 26.10.2015 11:27.
 */
public class Category {

    private String id;
    private String name;
    private String nameRu;
    private List<Profile> users;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNameRu() {
        return nameRu;
    }

    public List<Profile> getUsers() {
        return users;
    }
}
