package uz.ishow.android.model;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 21.02.2016 18:01.
 */
public class Nominee {

    private String id;
    private Integer votesCount;
    private Profile user;
    private Post post;

    private boolean votingEnabled;
    private boolean isMyChoice;
    private boolean isVotesCountVisible;

    public String getId() {
        return id;
    }

    public Integer getVotesCount() {
        return votesCount != null ? votesCount : 0;
    }

    public Profile getUser() {
        return user;
    }

    public Post getPost() {
        return post;
    }

    public boolean isVotingEnabled() {
        return votingEnabled;
    }

    public void setVotingEnabled(boolean votingEnabled) {
        this.votingEnabled = votingEnabled;
    }

    public boolean isMyChoice() {
        return isMyChoice;
    }

    public void setIsMyChoise(boolean isMyChoise) {
        this.isMyChoice = isMyChoise;
    }

    public boolean isVotesCountVisible() {
        return isVotesCountVisible;
    }

    public void setVotesCountVisible(boolean votesCountVisible) {
        this.isVotesCountVisible = votesCountVisible;
    }

    @Override
    public boolean equals(Object o) {
        return o != null && o instanceof Nominee && id != null && id.equals(((Nominee) o).getId());
    }
}
