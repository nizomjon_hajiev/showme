package uz.ishow.android.model;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 10.06.2015 17:10.
 */
public class User {

    private String id;
    private ProfilePhoto photo;
    private String name;
    private Boolean hasProfile;
    private String contactPhone;
    private String about;
    private Boolean isBlockable;
    private Integer followersCount;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getHasProfile() {
        return hasProfile != null && hasProfile;
    }

    public String getPhotoMini() {
        return photo != null ? photo.getMini() : null;
    }

    public String getPhotoGeneral() {
        if (photo != null && photo.getGeneral() == null && photo.getUrl() != null) {
            return photo.getUrl();
        }
        return photo != null ? photo.getGeneral() : null;
    }

    public void setPhotoGeneral(String url) {
        if (photo == null) {
            photo = new ProfilePhoto();
        }

        photo.setGeneral(url);
    }

    public String getPhotoBackground() {
        if (photo != null && photo.getBackground() == null && photo.getUrl() != null) {
            return photo.getUrl();
        }
        return photo != null ? photo.getBackground() : null;
    }

    public void setPhotoBackground(String url) {
        if (photo == null) {
            photo = new ProfilePhoto();
        }
        photo.setBackground(url);
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getAbout() {
        return about;
    }

    public Boolean getIsBlockable() {
        return isBlockable != null && isBlockable;
    }

    public void setIsBlockable(Boolean isBlockable) {
        this.isBlockable = isBlockable;
    }

    public Integer getFollowersCount() {
        return followersCount != null ? followersCount : 0;
    }

    @Override
    public boolean equals(Object o) {
        return o != null && o instanceof User && id != null && id.equals(((User) o).getId());
    }
}
