package uz.ishow.android.model;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 08.01.2016 10:55.
 */
public class Video {

    private Photo cover;
    private String url;

    public Photo getCover() {
        return cover;
    }

    public String getUrl() {
        return url;
    }
}
