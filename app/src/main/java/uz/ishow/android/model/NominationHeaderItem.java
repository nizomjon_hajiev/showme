package uz.ishow.android.model;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 21.02.2016 19:03.
 */
public class NominationHeaderItem extends Nominee {
    private String description;

    public NominationHeaderItem(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
