package uz.ishow.android.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 18.06.2015 16:20.
 */
public class Post {

    public static final String TEXT = "text";
    public static final String PHOTO = "photo";
    public static final String AUDIO = "audio";
    public static final String VIDEO = "video";

    private String id;

    private Integer likesCount;

    private Integer commentsCount;

    private Boolean hasLiked;

    private Profile author;

    private Boolean isDeletable;

    private String text;
    private Photo photo;
    private Audio audio;
    private Video video;
    private String type;

    private AudioOffline audioOffline;
    @SerializedName("published")
    private Long publishedTime;

    private Boolean isBlocked;

    private Boolean isContentView;

    public String getId() {
        return id;
    }

    public Integer getLikesCount() {
        return likesCount != null ? likesCount : 0;
    }

    public Integer getCommentsCount() {
        return commentsCount != null ? commentsCount : 0;
    }

    public void setLikesCount(Integer likesCount) {
        this.likesCount = likesCount;
    }

    public Boolean getHasLiked() {
        return hasLiked != null && hasLiked;
    }

    public void setHasLiked(Boolean hasLiked) {
        this.hasLiked = hasLiked;
    }

    public Boolean getIsDeletable() {
        return isDeletable != null && isDeletable;
    }

    public void setCommentsCount(Integer commentsCount) {
        this.commentsCount = commentsCount;
    }

    public Profile getAuthor() {
        return author;
    }

    public String getText() {
        return text;
    }

    public Photo getPhoto() {
        return photo;
    }

    public Audio getAudio() {
        return audio;
    }

    public Video getVideo() {
        return video;
    }

    public Boolean getIsBlocked() {
        return isBlocked != null && isBlocked;
    }

    public String getType() {
        return type;
    }

    public Date getPublishedTime() {
        if (publishedTime == null) {
            return null;
        }
        return new Date(publishedTime * 1000);
    }

    public void setAudio(Audio audio) {
        this.audio = audio;
    }

    @Override
    public boolean equals(Object o) {
        return o != null && o instanceof Post && id != null && id.equals(((Post) o).id);
    }

    public AudioOffline getAudioOffline() {
        return audioOffline;
    }

    public void setAudioOffline(AudioOffline audioOffline) {
        this.audioOffline = audioOffline;
    }

    public Boolean isContentView() {
        return isContentView;
    }

    public void setContentView(Boolean contentView) {
        isContentView = contentView;
    }

    public void setAuthor(Profile author) {
        this.author = author;
    }
}
