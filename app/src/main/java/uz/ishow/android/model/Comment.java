package uz.ishow.android.model;

import java.util.Date;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 07.07.2015 17:14.
 */
public class Comment {

    private String id;

    private Long time;
    private User user;
    private Boolean isDeletable;

    private String text;

    public String getId() {
        return id;
    }

    public Date getTime() {
        if (time == null) {
            return null;
        }
        return new Date(time * 1000);
    }

    public Long getTimeInSeconds() {
        return time;
    }

    public User getUser() {
        return user;
    }

    public String getText() {
        return text;
    }

    public Boolean getIsDeletable() {
        return isDeletable != null && isDeletable;
    }

    public Boolean getIsBlockable() {
        return user != null && user.getIsBlockable();
    }

    public void setIsBlockable(boolean isBlockable) {
        if (user != null) {
            user.setIsBlockable(isBlockable);
        }
    }

    @Override
    public boolean equals(Object o) {
        return o != null && o instanceof Comment && id != null && id.equals(((Comment) o).id);
    }
}
