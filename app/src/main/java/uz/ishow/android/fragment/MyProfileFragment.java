package uz.ishow.android.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import retrofit.RetrofitError;
import uz.fonus.util.BitmapUtils;
import uz.fonus.widget.CircularProgressBar;
import uz.ishow.android.R;
import uz.ishow.android.activity.AboutActivity;
import uz.ishow.android.activity.BaseActivity;
import uz.ishow.android.activity.FollowingsActivity;
import uz.ishow.android.activity.MainActivity;
import uz.ishow.android.adapter.MusicAdapter;
import uz.ishow.android.api.SendFileTask;
import uz.ishow.android.api.body.EditUserBody;
import uz.ishow.android.api.callback.BaseCallback;
import uz.ishow.android.api.result.FileUploadResult;
import uz.ishow.android.database.DatabaseHelper;
import uz.ishow.android.event.AudioEvent;
import uz.ishow.android.event.FeedItemEvent;
import uz.ishow.android.event.ProfileEvent;
import uz.ishow.android.model.AudioOffline;
import uz.ishow.android.model.Post;
import uz.ishow.android.model.Profile;
import uz.ishow.android.model.User;
import uz.ishow.android.util.MediaCenter;
import uz.ishow.android.util.SongsManager;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 30.06.2015 14:04.
 */
public class MyProfileFragment extends PickImageFragment {

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.addProfileImageText)
    TextView addProfileImageText;

    @Bind(R.id.phone_input_container)
    View phoneInputContainer;

    @Bind(R.id.about_input_container)
    View aboutInputContainer;

    @Bind(R.id.phone_input)
    EditText phoneInput;

    @Bind(R.id.about_input)
    EditText aboutInput;

    @Bind(R.id.profile_image)
    ImageView profileImage;

    @Bind(R.id.progress_bar)
    CircularProgressBar progressBar;

    @Bind(R.id.recycler_songs_list)
    RecyclerView recyclerView;

    @Bind(R.id.my_musics)
    TextView myMusics;

    @Bind(R.id.activity_label)
    TextView activityLabel;

    @BindString(R.string.profile)
    String profile;

    @Bind(R.id.clear_edit_text)
    View clearText;

    @Bind(R.id.profile_name)
    EditText profileName;

    private boolean isProfileImageChanged;

    private FileUploadResult fileUploadResult;
    private Handler handler = new Handler();
    private boolean forceFillProgress = false;
    private boolean isProfileChanged = false;
    private boolean isValidName = false;
    private boolean isRequestSending = false;
    private boolean isFileUpload = false;
    private User user;
    private Bitmap profileImageBitmap;

    private float progress;
    private int progressIncrement;
    private List<AudioOffline> mAudios;
    private ArrayList<String> listPath;
    MusicAdapter musicAdapter;
    SongsManager songsManager;
    DatabaseHelper databaseHelper;
    private Pattern phoneFormatUzb = Pattern.compile("\\+?\\s?998[\\-\\s\\(]?([0-9]{2})\\)?[\\-\\s]?([0-9]{3})[\\-\\s]?([0-9]{2})[\\-\\s]?([0-9]{2})");

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        aboutInput.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_UP:
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });

        profileName.addTextChangedListener(inputChangeListener);
        phoneInput.addTextChangedListener(inputChangeListener);
        aboutInput.addTextChangedListener(inputChangeListener);

        activityLabel.setText(profile);
        initMenu();
        fillData();
        registerEventSubscriber();

        songsManager = new SongsManager(getContext());
        databaseHelper = new DatabaseHelper(getActivity());

        loadSongsFromLocalStorage();


    }

    private void initMenu() {
        toolbar.inflateMenu(R.menu.menu_profile);

        MenuItem itemAbout = toolbar.getMenu().findItem(R.id.menu_about);
        if (itemAbout != null) {
            itemAbout.setVisible(true);
        }

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_save: {
                        save();
                        break;
                    }

                    case R.id.menu_cancel: {
                        hideKeyboard();
                        fillData();
                        break;
                    }

                    case R.id.menu_about: {
                        Intent intent = new Intent(getActivity(), AboutActivity.class);
                        startActivityForResult(intent, REQUEST_ABOUT_APP);
                        break;
                    }
                }
                return true;
            }
        });
    }

    @Override
    public void supportInvalidateOptionsMenu() {
        super.supportInvalidateOptionsMenu();

        MenuItem item2 = toolbar.getMenu().findItem(R.id.menu_cancel);
        if (item2 != null) {
            item2.setVisible(isProfileChanged);
            item2.setEnabled(!isRequestSending);
        }

        MenuItem item = toolbar.getMenu().findItem(R.id.menu_save);
        if (item != null) {
            item.setEnabled((isProfileChanged || isProfileImageChanged) && isValidName);
        }
    }

    public void onEvent(ProfileEvent event) {
        if (event.getType() == ProfileEvent.Type.USER_LOGGED_IN ||
                event.getType() == ProfileEvent.Type.USER_LOGGED_OUT) {
            fillData();
        }
    }


    public void onEvent(AudioEvent event) {
        if (musicAdapter != null) {
            musicAdapter.onEvent(event);
        }
        if (event.getType() == AudioEvent.Type.DOWNLOADED) {

            loadSongsFromLocalStorage();
            musicAdapter.notifyDataSetChanged();

        }
    }

    private void fillData() {
        if (hasActivity()) {
            isProfileChanged = false;
            isProfileImageChanged = false;
            fileUploadResult = null;
            profileImageBitmap = null;

            user = getBaseActivity().getUser();
            if (user != null) {
                profileName.setText(user.getName());
                if (user.getHasProfile()) {
                    phoneInputContainer.setVisibility(View.VISIBLE);
                    aboutInputContainer.setVisibility(View.VISIBLE);

                    phoneInput.setText(user.getContactPhone());
                    aboutInput.setText(user.getAbout());
                }

                if (user.getPhotoGeneral() != null) {
                    ImageLoader.getInstance().displayImage(user.getPhotoGeneral(), profileImage);
                    addProfileImageText.setVisibility(View.GONE);
                } else {
                    profileImage.setImageDrawable(
                            new ColorDrawable(getResources().getColor(R.color.image_placeholder)));
                    addProfileImageText.setVisibility(View.VISIBLE);
                }
            }

            supportInvalidateOptionsMenu();
        }
    }

    private TextWatcher inputChangeListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            String name = user.getName() != null ? user.getName() : "";
            String inputName = profileName.getText().toString().trim();
            boolean nameChanged = !TextUtils.equals(inputName, name);
            isValidName = !TextUtils.isEmpty(inputName);
            boolean phoneChanged = false;
            boolean aboutChanged = false;
            if (user.getHasProfile()) {
                String phone = user.getContactPhone() != null ? user.getContactPhone() : "";
                String about = user.getAbout() != null ? user.getAbout() : "";

                phoneChanged = !TextUtils.equals(phoneInput.getText().toString(), phone);
                aboutChanged = !TextUtils.equals(aboutInput.getText().toString(), about);
            }

            isProfileChanged = nameChanged || phoneChanged || aboutChanged;
            supportInvalidateOptionsMenu();
        }
    };


    private boolean isValidPhone() {
        String phone = phoneInput.getText().toString();
        Matcher matcher = phoneFormatUzb.matcher(phone);
        if (matcher.matches()) {
            StringBuilder sb = new StringBuilder();
            sb.append("+998").append(" ")
                    .append(matcher.group(1)).append(" ")
                    .append(matcher.group(2)).append("-")
                    .append(matcher.group(3)).append("-")
                    .append(matcher.group(4));
            phoneInput.setText(sb.toString());
            return true;
        }
        return false;
    }

    private void save() {
        if (isRequestSending) {
            return;
        }

        if (!TextUtils.isEmpty(phoneInput.getText()) && !isValidPhone()) {
            new AlertDialog.Builder(getActivity(), R.style.AppAlertDialogTheme)
                    .setMessage(R.string.wrong_phone_format)
                    .setPositiveButton(R.string.button_close, null)
                    .create().show();
            return;
        }

        isRequestSending = true;
        getActivity().supportInvalidateOptionsMenu();
        hideKeyboard();
        progress = 0f;
        forceFillProgress = false;
        if (isProfileImageChanged) {
            if (fileUploadResult == null) {
                File file = new File(getActivity().getFilesDir(), CROP_IMAGE_FILENAME);
                if (file.exists()) {
                    isFileUpload = true;
                    nextProgress();
                    new SendFileTask(file.getPath(), api, "profile") {
                        @Override
                        protected void onProgress(int progress) {
//                            updateFileUploadProgress(progress);
                        }

                        @Override
                        protected void success(FileUploadResult result) {
                            saveUser(result);
                        }

                        @Override
                        protected void networkError() {
                            Toast.makeText(getActivity(), R.string.no_connection, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        protected void error(RetrofitError error) {
                            isRequestSending = false;
                            supportInvalidateOptionsMenu();
                            cancelProgress();
                            showError(error);
                        }
                    }.execute();
                } else {
                    isRequestSending = false;
                    cancelProgress();
                    supportInvalidateOptionsMenu();
                }
            } else {
                saveUser(fileUploadResult);
            }
        } else {
            saveUser(null);
        }
    }

    @OnClick(R.id.clear_edit_text)
    void clearText() {
        profileName.setText(null);
    }

    private void saveUser(FileUploadResult fileUploadResult) {
        if (getActivity() == null || getActivity().isFinishing()) {
            return;
        }
        this.fileUploadResult = fileUploadResult;
        String name = profileName.getText().toString().trim();
        String phone = phoneInput.getText().toString();
        String about = aboutInput.getText().toString();

        String photo = fileUploadResult != null ? fileUploadResult.getId() : null;
        forceFillProgress = false;
        isFileUpload = false;
        nextProgress();
        api.editUser(new EditUserBody(name, photo, about, phone), new BaseCallback<User>() {
            @Override
            public void success(User user) {
                if (getActivity() != null && !getActivity().isFinishing()) {
                    isProfileImageChanged = false;
                    forceFillProgress = true;
                    progressIncrement = Math.max((int) ((100 - progress) / 12), 1);
                    getMainActivity().saveUser(user);

                    if (MyProfileFragment.this.fileUploadResult != null) {
                        ImageLoader.getInstance().loadImage(user.getPhotoMini(), null);
                    }

                    MyProfileFragment.this.fileUploadResult = null;
                    MyProfileFragment.this.user = user;
                    isProfileChanged = false;
                    getActivity().supportInvalidateOptionsMenu();

                    Profile profile = new Profile();
                    profile.setId(user.getId());
                    profile.setName(user.getName());
                    profile.setMiniImage(user.getPhotoMini());
                    profile.setGeneralImage(user.getPhotoGeneral());
                    profile.setBackgroundImage(user.getPhotoBackground());
                    profile.setAbout(user.getAbout());

                    EventBus.getDefault().post(FeedItemEvent.userDataChanged(profile, profileImageBitmap));
                    profileImageBitmap = null;

                    Toast.makeText(getActivity(), R.string.save_success, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void networkError() {
                showNoNetworkError();
            }

            @Override
            public void error(RetrofitError e) {
                cancelProgress();
                showError(e);
            }

            @Override
            public void complete() {
                isRequestSending = false;
                supportInvalidateOptionsMenu();
            }
        });

    }

    private void cancelProgress() {
        handler.removeCallbacks(progressBarRunnable);
        progressBar.setProgress(0);
        progressBar.setVisibility(View.GONE);
    }

    @OnClick(R.id.profile_image)
    void changeProfileImage() {
        pickImage(REQUEST_PICK_PROFILE_IMAGE);
    }

    @OnClick(R.id.followings)
    void openFollowings() {
        Intent intent = new Intent(getActivity(), FollowingsActivity.class);
        getBaseActivity().startActivityForResult(intent, BaseActivity.REQUEST_OPEN_FOLLOWINGS);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CROP_PICKED_IMAGE && resultCode == Activity.RESULT_OK) {
            BitmapUtils.exifInvalidate(outputUri.getPath());
            Bitmap bmp = BitmapUtils.decode(outputUri, 200, 200);
            if (bmp != null) {
                profileImage.setImageBitmap(bmp);
                addProfileImageText.setVisibility(View.GONE);
                isProfileImageChanged = true;
                profileImageBitmap = bmp;
                supportInvalidateOptionsMenu();
            }
        }
        if (requestCode == REQUEST_ABOUT_APP && (resultCode == Activity.RESULT_OK ||
                resultCode == AboutActivity.RESULT_LOGOUT)) {
            getActivity().finish();
            startActivity(new Intent(getActivity(), MainActivity.class));
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_edit_profile;
    }

    private Uri outputUri;
    private final String CROP_IMAGE_FILENAME = "crop.jpg";

    //create helping method cropCapturedImage(Uri picUri)
    public void cropCapturedImage(Uri picUri) {
        File outputFile = new File(getActivity().getFilesDir(), CROP_IMAGE_FILENAME);
        if (outputFile.exists()) {
            try {
                outputFile.delete();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        outputUri = Uri.fromFile(outputFile);

        Crop.of(picUri, outputUri).asSquare().withMaxSize(1080, 1080)
                .start(getActivity(), this, REQUEST_CROP_PICKED_IMAGE);

    }

    private void nextProgress() {
        if (progressBar.getProgress() == 100) {
            progressBar.setVisibility(View.GONE);
            progressBar.setProgress(0);
        } else {
            progressBar.setVisibility(View.VISIBLE);
            handler.removeCallbacks(progressBarRunnable);
            handler.postDelayed(progressBarRunnable, 50);
        }
    }


    private Runnable progressBarRunnable = new Runnable() {
        @Override
        public void run() {
            if (forceFillProgress) {
                progress += progressIncrement;
            } else {
                if (isFileUpload) {
                    progress += (64 - progress) / 30f;
                    progress = Math.min(progress, 64);
                } else {
                    progress += (100f - progress) / 30f;
                    progress = Math.min(progress, 100);
                }
            }
            progressBar.setProgress((int) progress);
            nextProgress();
        }
    };


    private MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }

    @Override
    protected void onPickedImageReady(String filePath) {
        cropCapturedImage(Uri.fromFile(new File(filePath)));
    }


    private void loadSongsFromLocalStorage() {

        mAudios = databaseHelper.getAllAudios();

        if (mAudios.size() > 0) {
            myMusics.setVisibility(View.VISIBLE);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            musicAdapter = new MusicAdapter(mAudios, getActivity(), onItemClickListener);
            recyclerView.setAdapter(musicAdapter);
        }
    }

    MusicAdapter.OnItemClickListener onItemClickListener = new MusicAdapter.OnItemClickListener() {
        @Override
        public void playAudio(AudioOffline audio, int position) {
            MediaCenter mc = MediaCenter.getInstance();
            Post post = new Post();
            post.setAudioOffline(audio);
            if (mc.isOnlinePlaying()) {
                mc.closePlayer();
                mc.playOffline(audio);
            } else {
                boolean isPlaying = MediaCenter.getInstance().isDownloadedAudioPlaying(post);
                if (isPlaying) {
                    mc.pauseAudio();
                } else {
                    mc.playOffline(audio);
                }
            }

        }

        @Override
        public void delete(final AudioOffline audio, final int position) {


            new AlertDialog.Builder(getActivity(), R.style.AppAlertDialogTheme)

                    .setMessage(R.string.agreement_delete_audio)
                    .setNegativeButton(R.string.no, null)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            musicAdapter.deleteItem(position);
                            databaseHelper.deleteAudio(audio.getAudio_path());
                            File file = new File(audio.audio_path);
                            if (file != null) {
                                boolean delete = file.delete();
                            }
                        }
                    })
                    .create()
                    .show();




        }
    };


}
