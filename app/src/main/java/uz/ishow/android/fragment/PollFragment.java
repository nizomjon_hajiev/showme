package uz.ishow.android.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import uz.ishow.android.R;
import uz.ishow.android.activity.BaseActivity;
import uz.ishow.android.activity.NominationActivity;
import uz.ishow.android.adapter.PollNominationAdapter;
import uz.ishow.android.api.callback.ListCallback;
import uz.ishow.android.model.Nomination;
import uz.ishow.android.util.CacheUtils;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 30.06.2015 14:04.
 */
public class PollFragment extends BaseFragment {

    @Inject
    CacheUtils cacheUtils;

    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    @Bind(R.id.empty_view)
    View emptyView;

    //Save to cache one time in fragment live;
    private boolean isCached;

    private PollNominationAdapter nominationAdapter;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        reloadContent();
        swipeRefreshLayout.requestDisallowInterceptTouchEvent(true);
        swipeRefreshLayout.setColorSchemeResources(R.color.primary, R.color.primaryDark);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                reloadContent();
            }
        });
    }

    private void reloadContent() {
        showRefreshIndicator();
        if (nominationAdapter == null) {
            loadFromCache();
        }

        api.getNominations(new ListCallback<Nomination>() {
            @Override
            public void success(List<Nomination> nominations) {
                emptyView.setVisibility(View.GONE);
                nominationAdapter = new PollNominationAdapter(nominations, mItemClickListener, !settings.isLanguageUz());
                recyclerView.setAdapter(nominationAdapter);
                saveToCacheIfRequired(nominations);
            }

            @Override
            public void emptyList() {
                emptyView.setVisibility(View.VISIBLE);
            }

            @Override
            public void networkError() {
                showNoNetworkError();
            }

            @Override
            public void complete() {
                hideRefreshIndicator();
            }
        });
    }

    private void saveToCacheIfRequired(List<Nomination> nominations) {
        if (!isCached) {
            cacheUtils.saveNominations(nominations);
            isCached = true;
        }
    }

    private void loadFromCache() {
        List<Nomination> nominations = cacheUtils.getNominations();
        if (nominations != null && !nominations.isEmpty()) {
            nominationAdapter = new PollNominationAdapter(nominations, mItemClickListener, !settings.isLanguageUz());
            recyclerView.setAdapter(nominationAdapter);
        }
    }

    private PollNominationAdapter.ItemClickListener mItemClickListener = new PollNominationAdapter.ItemClickListener() {
        @Override
        public void onItemClick(Nomination nomination) {
            if (nomination != null && nomination.getId() != null) {
                Intent intent = new Intent(getActivity(), NominationActivity.class);
                intent.putExtra("ID", nomination.getId());
                intent.putExtra("TITLE", settings.isLanguageUz() ? nomination.getName() : nomination.getNameRu());
                getActivity().startActivityForResult(intent, BaseActivity.REQUEST_OPEN_NOMINATION);
            }
        }
    };

    private void showRefreshIndicator() {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });
    }

    private void hideRefreshIndicator() {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_poll;
    }
}
