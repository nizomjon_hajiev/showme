package uz.ishow.android.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import javax.inject.Inject;

import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import retrofit.RetrofitError;
import uz.ishow.android.activity.BaseActivity;
import uz.ishow.android.api.Api;
import uz.ishow.android.application.IShowApplication;
import uz.ishow.android.util.Settings;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 10.06.2015 16:46.
 */
public abstract class BaseFragment extends Fragment {

    public static final int REQUEST_PICK_PROFILE_IMAGE = 201;
    public static final int REQUEST_CROP_PICKED_IMAGE = 202;
    public static final int REQUEST_ABOUT_APP = 203;

    @Inject
    Api api;

    @Inject
    Settings settings;

    @Inject
    Gson gson;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((IShowApplication) getActivity().getApplication()).inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(getLayout(), container, false);
        ButterKnife.bind(this, view);
//        Thread.setDefaultUncaughtExceptionHandler(new UnCaughtException(getBaseActivity()));

        return view;
    }

    @LayoutRes
    protected abstract int getLayout();


    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    protected void showNoNetworkError() {
        if (hasActivity()) {
            getBaseActivity().showNoNetwork();
        }
    }

    public void supportInvalidateOptionsMenu() {
        if (hasActivity()) {
            getActivity().supportInvalidateOptionsMenu();
        }
    }


    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    protected void registerEventSubscriber() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    protected boolean hasActivity() {
        return getActivity() != null && !getActivity().isFinishing();
    }

    protected void showError(RetrofitError error) {
        if (getBaseActivity() != null && !getBaseActivity().isFinishing()) {
            getBaseActivity().showError(error);
        }
    }

    public void hideKeyboard() {
        if (hasActivity()) {
            getBaseActivity().hideKeyboard();
        }
    }
}
