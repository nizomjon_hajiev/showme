package uz.ishow.android.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import uz.ishow.android.R;
import uz.ishow.android.activity.BaseActivity;
import uz.ishow.android.activity.CategoryUsersActivity;
import uz.ishow.android.activity.SearchActivity;
import uz.ishow.android.adapter.CategoryAdapter;
import uz.ishow.android.api.callback.ListCallback;
import uz.ishow.android.event.FeedItemEvent;
import uz.ishow.android.model.Category;
import uz.ishow.android.model.Profile;
import uz.ishow.android.util.CacheUtils;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 30.06.2015 14:04.
 */
public class CategoriesFragment extends BaseFragment {

    public static final int REQUEST_SEARCH = 104;
    private final String SEARCHING_CATEGORIES = "searching_categories";
    private final String SEARCHING_TYPE = "searching_type";
    @Inject
    CacheUtils cacheUtils;

    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;

    CategoryAdapter adapter;

    private long mCachedTime;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);


        swipeRefreshLayout.requestDisallowInterceptTouchEvent(true);
        swipeRefreshLayout.setColorSchemeResources(R.color.primary, R.color.primaryDark);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadCategories();
            }
        });

        loadFromCache();
        loadCategories();
        registerEventSubscriber();
    }

    public void onEvent(FeedItemEvent event) {
        if (adapter != null) {
            adapter.onEvent(event);
        }
    }

    public void loadCategories() {
        showRefreshIndicator();
        api.getCategories(new ListCallback<Category>() {
            @Override
            public void success(List<Category> result) {
                if (!result.isEmpty()) {
                    adapter = new CategoryAdapter(result, categoryItemListener,
                            settings.isLanguageUz());
                    recyclerView.setAdapter(adapter);
                    saveToCacheIfRequired(result);
                }
            }

            @Override
            public void complete() {
                hideRefreshIndicator();
            }
        });
    }

    public void loadFromCache() {
        List<Category> categories = cacheUtils.getCategories();
        if (categories != null && !categories.isEmpty()) {
            adapter = new CategoryAdapter(categories, categoryItemListener,
                    settings.isLanguageUz());
            recyclerView.setAdapter(adapter);
        }
    }

    public void saveToCacheIfRequired(List<Category> categories) {
        if (hasActivity() && System.currentTimeMillis() - mCachedTime > 300000) {
            cacheUtils.saveCategories(categories);
            mCachedTime = System.currentTimeMillis();
        }
    }

    public void scrollToTop() {
        if (recyclerView != null) {
            LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            int pos = layoutManager.findFirstVisibleItemPosition();
            if (pos >= 1) {
                layoutManager.scrollToPosition(1);
            }
            layoutManager.smoothScrollToPosition(recyclerView, null, 0);
        }
    }

    private void showRefreshIndicator() {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });
    }

    private void hideRefreshIndicator() {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private CategoryAdapter.CategoryItemListener categoryItemListener = new CategoryAdapter.CategoryItemListener() {
        @Override
        public void showUserProfile(Profile profile) {
            getBaseActivity().openProfileFeed(profile);
        }

        @Override
        public void showCategoryUsers(Category cat) {
            Intent intent = new Intent(getActivity(), CategoryUsersActivity.class);
            intent.putExtra("TITLE", settings.isLanguageUz() ? cat.getName() : cat.getNameRu());
            intent.putExtra("CATEGORY_ID", cat.getId());
            getBaseActivity().startActivityForResult(intent, BaseActivity.REQUEST_OPEN_CATEGORY_USERS);
        }
    };

    @Override
    protected int getLayout() {
        return R.layout.catalog_fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_category, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        super.onPrepareOptionsMenu(menu);
        MenuItem searchMenu = menu.findItem(R.id.menu_search);
        searchMenu.setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_search) {
            Intent intent = new Intent(getActivity(), SearchActivity.class);
            intent.putExtra(SEARCHING_TYPE, SEARCHING_CATEGORIES);

            startActivityForResult(intent, REQUEST_SEARCH);
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

}
