package uz.ishow.android.fragment;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tonicartos.superslim.LayoutManager;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import de.greenrobot.event.EventBus;
import uz.ishow.android.R;
import uz.ishow.android.activity.SearchActivity;
import uz.ishow.android.adapter.FeedAdapter;
import uz.ishow.android.adapter.UsersGridAdapter;
import uz.ishow.android.api.callback.ListCallback;
import uz.ishow.android.database.DatabaseHelper;
import uz.ishow.android.event.AudioEvent;
import uz.ishow.android.event.FeedItemEvent;
import uz.ishow.android.event.TimelineEvent;
import uz.ishow.android.event.TimelineProfileEvent;
import uz.ishow.android.listener.FeedItemListener;
import uz.ishow.android.model.AudioOffline;
import uz.ishow.android.model.FeedItem;
import uz.ishow.android.model.Post;
import uz.ishow.android.model.Profile;
import uz.ishow.android.util.CacheUtils;
import uz.ishow.android.util.DbBitmapUtility;
import uz.ishow.android.util.MediaCenter;
import uz.ishow.android.view.util.EndlessRecyclerViewOnScrollListener;
import uz.ishow.android.view.util.EndlessRecyclerViewOnScrollListenerNew;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 22.09.2015 16:10.
 */
public class TimelineFragment extends BaseFragment {

    public static final int TYPE_OWN = 1;
    public static final int TYPE_NEWS = 2;
    public static final int TYPE_PROFILE = 3;
    public static final int REQUEST_SEARCH = 104;
    private final String SEARCHING_TIMELINE = "searching_timeline";
    private final String SEARCHING_TYPE = "searching_type";
    private String lastCachedFeedItemId;
    private boolean hasTimeline = false;

    private String lastCachedSuggestedProfileId;
    private String lastCachedProfileFeedItemId;
    @Inject
    CacheUtils cacheUtils;

    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    @Bind(R.id.no_posts)
    View noPosts;

    @Bind(R.id.subscribe_for_follow)
    View subscribeForFollow;

    @Bind(R.id.empty_vew_container)
    LinearLayout emptyViewContainer;
    long enqueue;

    private FeedAdapter feedAdapter;

    private EndlessRecyclerViewOnScrollListenerNew endlessOnScrollListenerNew;

    private EndlessRecyclerViewOnScrollListener endlessOnScrollListener;
    private int type;

    private boolean previousLoading;
    private String profileId;
    private boolean canFollow;
    private boolean isFollowing;
    private UsersGridAdapter suggestionAdapter;

    private boolean isTimeLineType, isNewsType;
    private String downloadedSongId = "downloaded_id";
    SharedPreferences sharedPreferences;
    DownloadManager downloadManager;
    private HashMap<Long, Post> specialFeedItem;
    DatabaseHelper databaseHelper;
    File dir;
    MediaCenter mc;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        databaseHelper = new DatabaseHelper(getActivity());
        specialFeedItem = new HashMap<>();

        mc = MediaCenter.getInstance();
        type = getArguments().getInt("TYPE", TYPE_OWN);
        profileId = getArguments().getString("PROFILE_ID");
        canFollow = getArguments().getBoolean("CAN_FOLLOW");
        isFollowing = getArguments().getBoolean("IS_FOLLOWING");


        swipeRefreshLayout.setEnabled(type != TYPE_PROFILE);

        if (type == TYPE_PROFILE) {
            emptyViewContainer.setGravity(Gravity.TOP);
            setRecyclerViewAsFeed(false);
            // scroll down not enabled if adapter not set
            recyclerView.setAdapter(new FeedAdapter(null, false, true, getActivity(), feedItemListener));
        } else {
            setRecyclerViewAsFeed(true);

        }

        reloadContent();

        swipeRefreshLayout.requestDisallowInterceptTouchEvent(true);
        swipeRefreshLayout.setColorSchemeResources(R.color.primary, R.color.primaryDark);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                reloadContent();
            }
        });
        registerEventSubscriber();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
    }

    public void onEvent(TimelineEvent event) {
        if (event.getType() == TimelineEvent.Type.RELOAD_MY_TIMELINE) {
            if (type == TYPE_OWN) {
                reloadContent();
            }
        } else if (event.getType() == TimelineEvent.Type.REFRESH_CONTENT) {
            refresh();
        }
    }

    public void onEvent(FeedItemEvent event) {
        if (feedAdapter != null) {
            feedAdapter.onEvent(event);
            if (event.getType() == FeedItemEvent.Type.NEW_POST_ADDED) {
                reloadContent();
            }
        }

        if (suggestionAdapter != null && event.getType() == FeedItemEvent.Type.FOLLOWING) {
            suggestionAdapter.followingChanged(event.getProfileId(), event.getFollowersCount(),
                    event.isFollowing());
        }
    }


    public void scrollToTop() {

        if (recyclerView != null) {

            if (settings.isAuthorized() && isTimeLineType && hasTimeline) {
                LayoutManager layoutManager = (LayoutManager) recyclerView.getLayoutManager();
                int pos = layoutManager.findFirstVisibleItemPosition();
                if (pos == 0) {
                    View child = layoutManager.getChildAt(0);
                    if (child != null) {
                        int top = layoutManager.getDecoratedTop(child);
                        if (top == 0) {
                            return;
                        }
                    }
                }
                if (pos >= 1) {
                    recyclerView.scrollToPosition(0);
                }
                recyclerView.smoothScrollToPosition(0);

            } else if (settings.isAuthorized() && isTimeLineType && !hasTimeline) {
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int pos = layoutManager.findFirstVisibleItemPosition();
                if (pos == 0) {
                    View child = layoutManager.getChildAt(0);
                    if (child != null) {
                        int top = layoutManager.getDecoratedTop(child);
                        if (top == 0) {
                            return;
                        }
                    }
                }
                if (pos >= 1) {
                    recyclerView.scrollToPosition(0);
                }
                recyclerView.smoothScrollToPosition(0);
                recyclerView.smoothScrollToPosition(0);
            } else if (!settings.isAuthorized() && isTimeLineType) {
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int pos = layoutManager.findFirstVisibleItemPosition();
                if (pos == 0) {
                    View child = layoutManager.getChildAt(0);
                    if (child != null) {
                        int top = layoutManager.getDecoratedTop(child);
                        if (top == 0) {
                            return;
                        }
                    }
                }
                if (pos >= 1) {
                    recyclerView.scrollToPosition(0);
                }
                recyclerView.smoothScrollToPosition(0);
            }

            if (isNewsType) {
                LayoutManager layoutManager = (LayoutManager) recyclerView.getLayoutManager();
                int pos = layoutManager.findFirstCompletelyVisibleItemPosition();
                if (pos == 0) {
                    View child = layoutManager.getChildAt(0);
                    if (child != null) {
                        int top = layoutManager.getDecoratedTop(child);
                        if (top == 0) {
                            return;
                        }
                    }
                }
                if (pos >= 1) {
                    recyclerView.scrollToPosition(0);
                }
                recyclerView.smoothScrollToPosition(0);

            }

        }

    }

    public void onEvent(AudioEvent event) {
        if (feedAdapter != null) {
            feedAdapter.onEventBackgroundThread(event);
        }
        if (settings.isAuthorized()) {
            if (event.getType() == AudioEvent.Type.SHOULD_DOWNLOAD && mc.isShouldDownload()) {
                if (!event.getAudioPost().getAudio().isDownloaded()) {
                    mc.setShouldDownload(false);
                    downloadFile(event.getAudioPost());
                }
            }
        }
    }


    public void refresh() {
        if (type == TYPE_PROFILE) {
            swipeRefreshLayout.setEnabled(true);
        }
        showRefreshIndicator();
        reloadContent();
    }


    private void reloadContent() {
        showRefreshIndicator();
        noPosts.setVisibility(View.GONE);
        subscribeForFollow.setVisibility(View.GONE);

        switch (type) {
            case TYPE_OWN: {
                loadMyTimeline();
                isTimeLineType = true;
                isNewsType = false;
                break;
            }

            case TYPE_NEWS: {
                loadNews();
                isTimeLineType = false;
                isNewsType = true;
                break;
            }

            case TYPE_PROFILE: {
                loadProfileTimeline();
                isTimeLineType = false;
                isNewsType = false;
                break;
            }
        }
    }

    private void loadMyTimeline() {
        if (feedAdapter == null) {
            loadMyTimelineFromCache();
        }
        api.getMyFeed(null, new ListCallback<FeedItem>() {
            @Override
            public void success(List<FeedItem> list) {
                if (hasActivity() && !list.isEmpty()) {
                    suggestionAdapter = null;
                    hasTimeline = true;
                    setRecyclerViewAsFeed(true);
                    feedAdapter = new FeedAdapter(list, true, false, getActivity(),
                            feedItemListener);
                    recyclerView.setAdapter(feedAdapter);
                    saveMyTimelineToCacheIfRequired(list);
                }
            }

            @Override
            public void emptyList() {
                loadSuggestionList();
            }

            @Override
            public void networkError() {
                showNoNetworkError();
            }

            @Override
            public void complete() {
                hideRefreshIndicator();
            }
        });
    }

    private void loadMyTimelineFromCache() {

        List<FeedItem> feeds = cacheUtils.getMyTimeline();
        if (feeds != null && !feeds.isEmpty()) {
            suggestionAdapter = null;
            setRecyclerViewAsFeed(true);
            feedAdapter = new FeedAdapter(feeds, true, false, getActivity(), feedItemListener);
            recyclerView.setAdapter(feedAdapter);
        } else {
            loadSuggestionFromCache();
        }
    }

    private void loadNewsFromCache() {
        List<FeedItem> feeds = cacheUtils.getNewsTimeline();
        if (feeds != null && !feeds.isEmpty()) {
            suggestionAdapter = null;
            setRecyclerViewAsFeed(true);
            feedAdapter = new FeedAdapter(feeds, true, false, getActivity(), feedItemListener);
            recyclerView.setAdapter(feedAdapter);
        }
    }

    private void loadOwnProfileTimelineFromCache() {
        List<FeedItem> feeds = cacheUtils.getOwnProfileTimeline();
        if (feeds != null && !feeds.isEmpty()) {
            suggestionAdapter = null;
            setRecyclerViewAsFeed(false);
            feedAdapter = new FeedAdapter(feeds, false, false, getActivity(), feedItemListener);
            recyclerView.setAdapter(feedAdapter);
        }
    }


    private void loadSuggestionFromCache() {
        List<Profile> suggestions = cacheUtils.getSuggestions();
        if (suggestions != null && !suggestions.isEmpty()) {
            setRecyclerViewAsSuggestion();
            suggestionAdapter = new UsersGridAdapter(getActivity(), suggestions,
                    new UsersGridAdapter.OnUserClickListener() {
                        @Override
                        public void onUserClick(Profile profile) {
                            getBaseActivity().openProfileFeed(profile);
                        }
                    });

            suggestionAdapter.setIsTypeSuggestion(true);
            recyclerView.setAdapter(suggestionAdapter);
        }
    }

    private void saveMyTimelineToCacheIfRequired(List<FeedItem> feedItems) {
        if (hasActivity() && !feedItems.isEmpty()
                && !TextUtils.equals(feedItems.get(0).getId(), lastCachedFeedItemId)) {
            cacheUtils.saveMyTimeline(feedItems);
            lastCachedFeedItemId = feedItems.get(0).getId();
        }
    }

    private void saveProfileTimelineToCacheIfRequired(List<FeedItem> feedItems) {
        if (hasActivity() && isOwnProfileTimeline() && !feedItems.isEmpty()
                && !TextUtils.equals(feedItems.get(0).getId(), lastCachedProfileFeedItemId)) {
            cacheUtils.saveOwnProfileTimeline(feedItems);
            lastCachedProfileFeedItemId = feedItems.get(0).getId();
        }
    }

    private void saveNewsToCacheIfRequired(List<FeedItem> feedItems) {
        if (hasActivity() && !feedItems.isEmpty()
                && !TextUtils.equals(feedItems.get(0).getId(), lastCachedFeedItemId)) {
            cacheUtils.saveNewsTimeline(feedItems);
            lastCachedFeedItemId = feedItems.get(0).getId();
        }
    }

    private void saveSuggestionsToCacheIfRequired(List<Profile> suggestions) {
        if (hasActivity() && !suggestions.isEmpty() &&
                !TextUtils.equals(suggestions.get(0).getId(), lastCachedSuggestedProfileId)) {
            cacheUtils.saveSuggestions(suggestions);
            lastCachedSuggestedProfileId = suggestions.get(0).getId();
        }
    }

    private void loadNews() {
        if (feedAdapter == null) {
            loadNewsFromCache();
        }
        api.getNews(null, new ListCallback<FeedItem>() {
            @Override
            public void success(List<FeedItem> result) {
                if (hasActivity()) {
                    feedAdapter = new FeedAdapter(result, true, false, getActivity(),
                            feedItemListener);
                    recyclerView.setAdapter(feedAdapter);
                    saveNewsToCacheIfRequired(result);
                }
            }

            @Override
            public void emptyList() {
                noPosts.setVisibility(View.VISIBLE);
            }

            @Override
            public void complete() {
                hideRefreshIndicator();
            }

            @Override
            public void networkError() {
                showNoNetworkError();
            }
        });
    }

    private boolean isOwnProfileTimeline() {
        return profileId != null && getBaseActivity() != null && getBaseActivity().getUser() != null
                && profileId.equals(getBaseActivity().getUser().getId());
    }

    private void loadProfileTimeline() {
        if (isOwnProfileTimeline() && feedAdapter == null) {
            loadOwnProfileTimelineFromCache();
        }
        api.getProfileTimeline(profileId, null, new ListCallback<FeedItem>() {
            @Override
            public void success(List<FeedItem> result) {
                if (hasActivity()) {
                    if (!result.isEmpty() && result.get(0) != null && result.get(0).getPost() != null) {
                        Post post = result.get(0).getPost();
                        if (post.getAuthor() != null) {
                            // Nizomjon buni normalno qilish kerak sizga qoldirdim :)
                            EventBus.getDefault().post(new TimelineProfileEvent(post.getAuthor()));
                        }
                    }

                    feedAdapter = new FeedAdapter(result, false, true, getActivity(), feedItemListener);
                    feedAdapter.setOpenProfileDisabled(true);
                    recyclerView.setAdapter(feedAdapter);
                    saveProfileTimelineToCacheIfRequired(result);
                }
            }

            @Override
            public void error(Exception e) {
                e.printStackTrace();
            }

            @Override
            public void emptyList() {
                noPosts.setVisibility(View.VISIBLE);
                if (canFollow && !isFollowing) {
                    subscribeForFollow.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void complete() {
                hideRefreshIndicator();
                swipeRefreshLayout.setEnabled(false);
            }

            @Override
            public void networkError() {
                showNoNetworkError();
            }
        });
    }

    private void loadSuggestionList() {
        showRefreshIndicator();
        api.getSuggestedProfiles(new ListCallback<Profile>() {
            @Override
            public void success(List<Profile> result) {
                if (hasActivity() && !result.isEmpty()) {
                    setRecyclerViewAsSuggestion();
                    suggestionAdapter = new UsersGridAdapter(getActivity(), result,
                            new UsersGridAdapter.OnUserClickListener() {
                                @Override
                                public void onUserClick(Profile profile) {
                                    getBaseActivity().openProfileFeed(profile);
                                }
                            });

                    suggestionAdapter.setIsTypeSuggestion(true);
                    recyclerView.setAdapter(suggestionAdapter);
                    saveSuggestionsToCacheIfRequired(result);
                }
            }

            @Override
            public void complete() {
                hideRefreshIndicator();
            }
        });
    }

    private void setRecyclerViewAsFeed(boolean isTimeline) {

        if (isTimeline) {
            LayoutManager layoutManager = new LayoutManager(getActivity());
            isTimeLineType = true;
            if (endlessOnScrollListenerNew != null) {
                recyclerView.removeOnScrollListener(endlessOnScrollListenerNew);
            }

            endlessOnScrollListenerNew = new EndlessRecyclerViewOnScrollListenerNew(layoutManager) {
                @Override
                public void onLoadMore() {
                    if (!previousLoading) {
                        loadPrevious();
                    }
                }
            };

            recyclerView.addOnScrollListener(endlessOnScrollListenerNew);
            recyclerView.setLayoutManager(layoutManager);

        } else {
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
            isTimeLineType = false;
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

            if (endlessOnScrollListener != null) {
                recyclerView.removeOnScrollListener(endlessOnScrollListener);
            }

            endlessOnScrollListener = new EndlessRecyclerViewOnScrollListener(layoutManager) {
                @Override
                public void onLoadMore() {
                    if (!previousLoading) {
                        loadPrevious();
                    }
                }
            };

            recyclerView.addOnScrollListener(endlessOnScrollListener);
            recyclerView.setLayoutManager(layoutManager);

        }
    }

    private void loadPrevious() {

        switch (type) {
            case TYPE_OWN: {
                loadPreviousTimeline();
                break;
            }

            case TYPE_NEWS: {
                loadPreviousNews();
                break;
            }

            case TYPE_PROFILE: {
                loadPreviousProfileTimeline();
                break;
            }
        }

    }

    private void loadPreviousTimeline() {
        if (feedAdapter != null && feedAdapter.getLastItem() != null) {
            FeedItem item = feedAdapter.getLastItem();
            Long time = item.getTimeInSeconds();

            if (time != null) {
                previousLoading = true;
                api.getMyFeed(time, new ListCallback<FeedItem>() {
                    @Override
                    public void success(List<FeedItem> result) {
                        if (hasActivity() && feedAdapter != null) {
                            feedAdapter.addAll(result);
                        }
                    }

                    @Override
                    public void complete() {
                        previousLoading = false;
                    }
                });
            }
        }
    }

    private void loadPreviousNews() {
        if (feedAdapter != null && feedAdapter.getLastItem() != null) {
            FeedItem item = feedAdapter.getLastItem();
            if (item.getTimeInSeconds() != null) {
                Long time = item.getTimeInSeconds();
                previousLoading = true;
                api.getNews(time, new ListCallback<FeedItem>() {
                    @Override
                    public void success(List<FeedItem> result) {
                        if (feedAdapter != null) {
                            feedAdapter.addAll(result);
                        }
                    }

                    @Override
                    public void complete() {
                        previousLoading = false;
                    }
                });
            }
        }
    }

    private void loadPreviousProfileTimeline() {
        if (feedAdapter != null && feedAdapter.getLastItem() != null) {
            FeedItem item = feedAdapter.getLastItem();
            Long time = item.getTimeInSeconds();
            if (time != null) {
                previousLoading = true;
                api.getProfileTimeline(profileId, time, new ListCallback<FeedItem>() {
                    @Override
                    public void success(List<FeedItem> result) {
                        if (feedAdapter != null) {
                            feedAdapter.addAll(result);
                        }
                    }

                    @Override
                    public void complete() {
                        previousLoading = false;
                    }
                });
            }
        }
    }

    private void setRecyclerViewAsSuggestion() {
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        // merge first row
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? 2 : 1;
            }
        });
        hasTimeline = false;
//        setRecyclerViewAsFeed(false);
        if (endlessOnScrollListener != null) {
            recyclerView.removeOnScrollListener(endlessOnScrollListener);
        }
        recyclerView.setLayoutManager(layoutManager);
    }

    private void showRefreshIndicator() {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });
    }

    private void hideRefreshIndicator() {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    protected int getLayout() {
        return R.layout.feed_layout;
    }

    protected FeedItemListener feedItemListener = new FeedItemListener() {
        @Override
        public void onHeaderClick(FeedItem item) {
            getBaseActivity().openProfileFeed(item.getPost().getAuthor());
        }

        @Override
        public void onCommentClick(FeedItem item) {
            getBaseActivity().commentItem(item);
        }

        @Override
        public void like(FeedItem item) {
            getBaseActivity().like(item);
        }

        @Override
        public void unlike(FeedItem item) {
            getBaseActivity().unlike(item);
        }

        @Override
        public void showOptions(FeedItem item) {
            getBaseActivity().showFeedItemOptions(item);
        }

        @Override
        public void onAudioClick(FeedItem item) {
            MediaCenter mc = MediaCenter.getInstance();
            if (getBaseActivity().isOnline()) {
                if (mc.isPlaying(item.getPost())) {
                    mc.pauseAudio();
                } else {
                    mc.playAudio(item.getPost());
                }
            } else {
                if (databaseHelper.isExistById(item.getPost().getId())) {

                    try {
                        AudioOffline audioOffline = databaseHelper.getAudioItem(item.getPost().getId());
                        Post post = new Post();
                        post.setAudioOffline(audioOffline);
                        boolean isPlaying = MediaCenter.getInstance().isDownloadedAudioPlaying(post);
                        if (isPlaying) {
                            mc.pauseAudio();
                        } else {
                            mc.playOffline(audioOffline);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    if (mc.isPlaying(item.getPost())) {
                        mc.pauseAudio();
                    } else {
                        mc.playAudio(item.getPost());
                    }
                }
            }
        }

        @Override
        public void openVideo(FeedItem item) {
            getBaseActivity().openVideo(item);
        }

        @Override
        public void downloadSong(final FeedItem item) {
            if (settings.isAuthorized()) {

                new AlertDialog.Builder(getActivity(), R.style.AppAlertDialogTheme)

                        .setMessage(R.string.agreement_downloading_active_user)
                        .setNegativeButton(R.string.no, null)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                downloadFile(item.getPost());
                            }
                        })
                        .create()
                        .show();


            } else {
                new AlertDialog.Builder(getActivity(), R.style.AppAlertDialogTheme)
                        .setMessage(R.string.agreement_downloading_guest_user)
                        .setPositiveButton(R.string.yes, null)
                        .create()
                        .show();

            }
        }
    };


    private void downloadFile(Post feedItem) {


        dir = new File(getActivity().getExternalFilesDir(null), "audio");
        if (!dir.exists()) {
            dir.mkdir();
        }


        downloadManager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
        Uri download_uri = Uri.parse(feedItem.getAudio().getUrl());

        DownloadManager.Request request = new DownloadManager.Request(download_uri);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedOverRoaming(false);
        request.setDescription(feedItem.getAudio().getTitle());
        request.setTitle(feedItem.getAudio().getArtist());
        String fileName = feedItem.getAudio().getArtist() + "-" + feedItem.getAudio().getTitle();

        request.setDestinationInExternalFilesDir(getActivity(), dir.getName(), fileName);


        enqueue = downloadManager.enqueue(request);
        specialFeedItem.put(enqueue, feedItem);
        IntentFilter intentFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        getActivity().registerReceiver(receiver, intentFilter);

    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0L);
//            if (id != enqueue) {
//                Log.v("Test", "Ingnoring unrelated download " + id);
//                return;
//            }
            DownloadManager.Query query = new DownloadManager.Query();
//            long id = sharedPreferences.getLong(downloadedSongId, 0);
            query.setFilterById(id);
            Cursor cursor = downloadManager.query(query);
            if (cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
                int status = cursor.getInt(columnIndex);
                int columnReason = cursor.getColumnIndex(DownloadManager.COLUMN_REASON);
                int reason = cursor.getInt(columnReason);

                switch (status) {
                    case DownloadManager.STATUS_FAILED:
                        String failedReason = "";
                        switch (reason) {
                            case DownloadManager.ERROR_CANNOT_RESUME:
                                failedReason = "ERROR_CANNOT_RESUME";
                                break;
                            case DownloadManager.ERROR_DEVICE_NOT_FOUND:
                                failedReason = "ERROR_DEVICE_NOT_FOUND";
                                break;
                            case DownloadManager.ERROR_FILE_ALREADY_EXISTS:
                                failedReason = "ERROR_FILE_ALREADY_EXISTS";
                                break;
                            case DownloadManager.ERROR_FILE_ERROR:
                                failedReason = "ERROR_FILE_ERROR";
                                break;
                            case DownloadManager.ERROR_HTTP_DATA_ERROR:
                                failedReason = "ERROR_HTTP_DATA_ERROR";
                                break;
                            case DownloadManager.ERROR_INSUFFICIENT_SPACE:
                                failedReason = "ERROR_INSUFFICIENT_SPACE";
                                break;
                            case DownloadManager.ERROR_TOO_MANY_REDIRECTS:
                                failedReason = "ERROR_TOO_MANY_REDIRECTS";
                                break;
                            case DownloadManager.ERROR_UNHANDLED_HTTP_CODE:
                                failedReason = "ERROR_UNHANDLED_HTTP_CODE";
                                break;
                            case DownloadManager.ERROR_UNKNOWN:
                                failedReason = "ERROR_UNKNOWN";
                                break;
                        }

                        Toast.makeText(context,
                                "FAILED: " + failedReason,
                                Toast.LENGTH_LONG).show();
                        break;
                    case DownloadManager.STATUS_PAUSED:
                        String pausedReason = "";

                        switch (reason) {
                            case DownloadManager.PAUSED_QUEUED_FOR_WIFI:
                                pausedReason = "PAUSED_QUEUED_FOR_WIFI";
                                break;
                            case DownloadManager.PAUSED_UNKNOWN:
                                pausedReason = "PAUSED_UNKNOWN";
                                break;
                            case DownloadManager.PAUSED_WAITING_FOR_NETWORK:
                                pausedReason = "PAUSED_WAITING_FOR_NETWORK";
                                break;
                            case DownloadManager.PAUSED_WAITING_TO_RETRY:
                                pausedReason = "PAUSED_WAITING_TO_RETRY";
                                break;
                        }

                        Toast.makeText(context,
                                "PAUSED: " + pausedReason,
                                Toast.LENGTH_LONG).show();
                        break;
                    case DownloadManager.STATUS_PENDING:
                        break;
                    case DownloadManager.STATUS_RUNNING:

                        break;
                    case DownloadManager.STATUS_SUCCESSFUL:

                        Toast.makeText(context,
                                getActivity().getResources().getString(R.string.success_download),
                                Toast.LENGTH_LONG).show();
                        final AudioOffline audioOffline = new AudioOffline();
                        if (specialFeedItem.get(id) != null) {
                            final Post post = specialFeedItem.get(id);

                            audioOffline.setAudio_id(post.getId());
                            audioOffline.setAudio_name(post.getAudio().getArtist());
                            audioOffline.setAudio_title(post.getAudio().getTitle());
                            audioOffline.setAudio_duration(post.getAudio().getHumanReadableDuration());
                            String fileName = post.getAudio().getArtist() + "-" + post.getAudio().getTitle();
                            audioOffline.setAudio_path(dir.getPath() + "/" + fileName);
                            if (post.getAudio().getCover() != null) {
                                ImageLoader.getInstance().loadImage(post.getAudio().getCover(), new ImageLoadingListener() {
                                    @Override
                                    public void onLoadingStarted(String imageUri, View view) {

                                    }

                                    @Override
                                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                                    }

                                    @Override
                                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                                        audioOffline.setAudio_image(DbBitmapUtility.getBytes(loadedImage));
                                        databaseHelper.addAudio(audioOffline);
                                        databaseHelper.removeDublicateAudios();
                                        EventBus.getDefault().post(AudioEvent.downloaded(post));

                                    }

                                    @Override
                                    public void onLoadingCancelled(String imageUri, View view) {

                                    }
                                });

                            } else {
                                databaseHelper.addAudio(audioOffline);
                                databaseHelper.removeDublicateAudios();
                                EventBus.getDefault().post(AudioEvent.downloaded(post));
                            }
                        }


                        break;
                }
            }
        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_category, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        super.onPrepareOptionsMenu(menu);
        MenuItem searchMenu = menu.findItem(R.id.menu_search);
        searchMenu.setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_search) {
            Intent intent = new Intent(getActivity(), SearchActivity.class);
            intent.putExtra(SEARCHING_TYPE, SEARCHING_TIMELINE);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

//    @Override
//    public void onPause() {
//        super.onPause();
//        getActivity().unregisterReceiver(receiver);
//    }


}
