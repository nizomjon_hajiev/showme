package uz.ishow.android.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;
import uz.ishow.android.R;
import uz.ishow.android.activity.AboutActivity;
import uz.ishow.android.activity.EditProfileActivity;
import uz.ishow.android.activity.MainActivity;
import uz.ishow.android.application.IShowApplication;
import uz.ishow.android.event.FeedItemEvent;
import uz.ishow.android.event.TimelineProfileEvent;
import uz.ishow.android.model.Profile;
import uz.ishow.android.model.User;
import uz.ishow.android.util.CacheUtils;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 19.01.2016 12:36.
 */
public class MyTimelineFragment extends BaseFragment {

    @Inject
    CacheUtils cacheUtils;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.profile_image)
    ImageView profileImage;

    @Bind(R.id.parallaxImage)
    ImageView parallaxImage;

    @Bind(R.id.app_bar_layout)
    AppBarLayout appBarLayout;

    @Bind(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;

    @Bind(R.id.subscribers)
    TextView followers;

    @Bind(R.id.name)
    TextView nameTextView;

    @Bind(R.id.bio)
    TextView bio;


    private TimelineFragment mTimelineFragment;
    private User mUser;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mUser = getBaseActivity().getUser();
        IShowApplication.setIsStar(true);
        fillProfileInfo();
        initMenu();

        if (mTimelineFragment == null) {
            mTimelineFragment = (TimelineFragment) getChildFragmentManager().findFragmentByTag("TIMELINE");
            if (mTimelineFragment == null) {
                mTimelineFragment = new TimelineFragment();
                Bundle args = new Bundle();
                args.putInt("TYPE", TimelineFragment.TYPE_PROFILE);
                args.putString("PROFILE_ID", mUser.getId());
                args.putBoolean("CAN_FOLLOW", false);
                args.putBoolean("IS_FOLLOWING", false);
                mTimelineFragment.setArguments(args);

                getChildFragmentManager().beginTransaction()
                        .add(R.id.timeline_container, mTimelineFragment, "TIMELINE").commit();
            }
        }

        registerEventSubscriber();
    }

    public void onEvent(FeedItemEvent event) {
        if (event.getType() == FeedItemEvent.Type.USER_DATA_CHANGED) {
            mUser = getBaseActivity().getUser();
            if (event.getProfileImageBitmap() != null) {
                profileImage.setImageBitmap(event.getProfileImageBitmap());
            }
            fillProfileInfo();
            if (mTimelineFragment != null) {
                mTimelineFragment.onEvent(event);
            }
        }
    }

    public void onEvent(TimelineProfileEvent event) {
        Profile p = event.getProfile();
        mUser = getBaseActivity().getUser();

        if (mUser != null && p != null && mUser.getId() != null && mUser.getId().equals(p.getId())) {
            if (!TextUtils.equals(mUser.getName(), p.getName())
                    || !TextUtils.equals(mUser.getPhotoMini(), p.getMiniImage())
                    || !TextUtils.equals(mUser.getAbout(), p.getAbout())
                    ) {
                mUser.setName(p.getName());
                if (!TextUtils.isEmpty(p.getAbout())) {
                    mUser.setAbout(p.getAbout());
                }

                mUser.setPhotoGeneral(p.getGeneralImage());
                mUser.setPhotoBackground(p.getBackgroundImage());
                getBaseActivity().saveUser(mUser);
                fillProfileInfo();
            }
        }
    }

    private void fillProfileInfo() {
        String name = mUser.getName();
        invalidateFollowersCountView();
        collapsingToolbarLayout.setTitle(name);
        nameTextView.setText(name);

        bio.setText(mUser.getAbout());

        ImageLoader.getInstance().displayImage(mUser.getPhotoGeneral(), profileImage);
        ImageLoader.getInstance().displayImage(mUser.getPhotoBackground(), parallaxImage);
    }

    private void initMenu() {
        toolbar.inflateMenu(R.menu.menu_profile_feed);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.menu_refresh) {
                    refresh();
                } else if (item.getItemId() == R.id.menu_about) {
                    Intent intent = new Intent(getActivity(), AboutActivity.class);
                    startActivityForResult(intent, REQUEST_ABOUT_APP);
                }
                return true;
            }
        });

        MenuItem itemAbout = toolbar.getMenu().findItem(R.id.menu_about);
        if (itemAbout != null) {
            itemAbout.setVisible(true);
        }
    }

    private void refresh() {
        if (mTimelineFragment != null) {
            mTimelineFragment.refresh();
        }
    }

    private void invalidateFollowersCountView() {
        int followersCount = mUser.getFollowersCount();

        if (mUser.getHasProfile()) {
            String text = followersCount == 0 ? getString(R.string.no_subscribers) :
                    getResources().getQuantityString(R.plurals.subscribers, followersCount, followersCount);
            followers.setText(text);
        }
    }


    @OnClick(R.id.button_edit)
    void editProfile() {
        Intent intent = new Intent(getActivity(), EditProfileActivity.class);
        intent.putExtra("FROM_MY_TIMELINE", true);
        startActivity(intent);
    }


    @Override
    protected int getLayout() {
        return R.layout.fragment_my_timeline;
    }

    public void scrollToTop() {
        if (mTimelineFragment != null) {
            mTimelineFragment.scrollToTop();
        }

        if (appBarLayout != null) {
            appBarLayout.setExpanded(true);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ABOUT_APP) {
            if (resultCode == AboutActivity.RESULT_LOGOUT || resultCode == Activity.RESULT_OK) {
                getBaseActivity().finish();
                startActivity(new Intent(getBaseActivity(), MainActivity.class));
            }
        }
    }
}
