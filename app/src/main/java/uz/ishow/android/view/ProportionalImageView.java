package uz.ishow.android.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 19.06.2015 16:13.
 */
public class ProportionalImageView extends ImageView {

    private int mImageWidth;
    private int mImageHeight;

    private static final float MAX_VERTICAL_ASPECT_RATIO = 1.5f;

    public ProportionalImageView(Context context) {
        super(context);
    }

    public ProportionalImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProportionalImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public ProportionalImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setProportion(int width, int height) {
        mImageWidth = width;
        mImageHeight = height;
        requestLayout();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = width;

        if (mImageWidth > 0 && mImageHeight > 0) {
            height = width * mImageHeight / mImageWidth;
            int maxHeight = (int) (width * MAX_VERTICAL_ASPECT_RATIO);
            height = Math.min(height, maxHeight);
        }

        Drawable d = getDrawable();
        if (d != null && d instanceof BitmapDrawable) {
            final int drawableWidth = getDrawable().getIntrinsicWidth();
            final int drawableHeight = getDrawable().getIntrinsicHeight();

            height = width * drawableHeight / drawableWidth;
            int maxHeight = (int) (width * MAX_VERTICAL_ASPECT_RATIO);
            height = Math.min(height, maxHeight);
        }

        int finalMeasureWidthSpec = MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY);
        int finalMeasureHeightSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
        super.onMeasure(finalMeasureWidthSpec, finalMeasureHeightSpec);
    }

}
