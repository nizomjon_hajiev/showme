package uz.ishow.android.view.util;

import android.support.v7.widget.RecyclerView;

import com.tonicartos.superslim.LayoutManager;

/**
 * Created by User on 16.04.2016.
 */

public abstract class EndlessRecyclerViewOnScrollListenerNew extends RecyclerView.OnScrollListener {

    LayoutManager layoutManager;

    private int visibleThreshold = 2;
    private int firstVisibleItem;
    private int lastVisibleItem;
    private int visibleItemCount;
    private int totalItemCount;

    private boolean fromBottom = true;

    public EndlessRecyclerViewOnScrollListenerNew(LayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    public EndlessRecyclerViewOnScrollListenerNew(LayoutManager layoutManager, boolean fromBottom) {
        this(layoutManager);
        this.fromBottom = fromBottom;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        visibleItemCount = recyclerView.getChildCount();

        if (fromBottom) {
            totalItemCount = layoutManager.getItemCount();
            if (layoutManager instanceof LayoutManager) {
                firstVisibleItem = layoutManager.findFirstCompletelyVisibleItemPosition();
            } else {
                return;
            }

            if ((totalItemCount - visibleItemCount - firstVisibleItem) <= visibleThreshold) {
                onLoadMore();
            }
        } else {
            if (layoutManager instanceof LayoutManager) {
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();
            } else {
                return;
            }

            if (lastVisibleItem - visibleItemCount <= visibleThreshold) {
                onLoadMore();
            }
        }
    }

    public abstract void onLoadMore();
}
