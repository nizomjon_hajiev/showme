package uz.ishow.android.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 12.01.2016 11:48.
 */
public class VideoPlayer extends VideoView {

    private PlaybackListener mPlaybackListener;

    public VideoPlayer(Context context) {
        super(context);
    }

    public VideoPlayer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VideoPlayer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public VideoPlayer(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void start() {
        super.start();
        if (mPlaybackListener != null) {
            mPlaybackListener.onStart();
        }
    }

    @Override
    public void resume() {
        super.resume();
        if (mPlaybackListener != null) {
            mPlaybackListener.onResume();
        }
    }

    @Override
    public void pause() {
        super.pause();
        if (mPlaybackListener != null) {
            mPlaybackListener.onPause();
        }
    }

    public void setPlaybackListener(PlaybackListener playbackListener) {
        this.mPlaybackListener = playbackListener;
    }

    public interface PlaybackListener {
        void onStart();
        void onPause();
        void onResume();
    }

}
