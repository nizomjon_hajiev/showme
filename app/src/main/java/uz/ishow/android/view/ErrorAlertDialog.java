package uz.ishow.android.view;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.OnClick;
import uz.ishow.android.R;

/**
 * Created by User on 11.05.2016.
 */
public class ErrorAlertDialog extends Dialog {

    public Context c;
    public Dialog d;

    public TextView yes, no;

    public ErrorAlertDialog(Context context) {
        super(context);
        this.c = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_error_dialog);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.close_btn)
    void closeApp() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        c.startActivity(intent);
    }
}
