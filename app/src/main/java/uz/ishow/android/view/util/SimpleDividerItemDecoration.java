package uz.ishow.android.view.util;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import uz.ishow.android.R;

public class SimpleDividerItemDecoration extends RecyclerView.ItemDecoration {
    private Drawable mDivider;
    private int mHorizontalMargin;
 
    public SimpleDividerItemDecoration(Context context) {
        if (Build.VERSION.SDK_INT < 21) {
            //noinspection deprecation
            mDivider = context.getResources().getDrawable(R.drawable.line_divider);
        } else {
            mDivider = context.getResources().getDrawable(R.drawable.line_divider, context.getTheme());
        }
    }

    public SimpleDividerItemDecoration(Context context, int horizontalMargin) {
        this(context);
        this.mHorizontalMargin = horizontalMargin;
    }
 
    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();
 
        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);
 
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
 
            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + mDivider.getIntrinsicHeight();

            mDivider.setBounds(left + mHorizontalMargin, top, right - mHorizontalMargin, bottom);
            mDivider.draw(c);
        }
    }
}