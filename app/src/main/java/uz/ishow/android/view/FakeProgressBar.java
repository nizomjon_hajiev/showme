package uz.ishow.android.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.util.AttributeSet;
import android.widget.ProgressBar;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 19.11.2015 9:51.
 */
public class FakeProgressBar extends ProgressBar {

    public static final int NORMAL = 0;
    public static final int SUCCESS = 1;
    public static final int ERROR = 2;

    private int frameTime = 50;
    private int animationTime = 250;

    @DrawableRes
    private int defaultDrawable;
    @DrawableRes
    private int successDrawable;
    @DrawableRes
    private int errorDrawable;

    private float progress;
    private float max;

    private int status;
    private float increment = 1f;
    private float decrement = 1f;

    public FakeProgressBar(Context context) {
        super(context);
    }

    public FakeProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FakeProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public FakeProgressBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setProgressDrawables(@DrawableRes int defaultDrawable,
                                     @DrawableRes int successDrawable,
                                     @DrawableRes int errorDrawable) {
        this.defaultDrawable = defaultDrawable;
        this.successDrawable = successDrawable;
        this.errorDrawable = errorDrawable;
    }

    public void startProgress(float max) {
        setMax(10000);
        setVisibility(VISIBLE);
        setProgress(0);
        setProgressDrawableCompat(defaultDrawable);
        this.max = max;
        progress = 0f;
        status = NORMAL;
        nextProgress();
    }

    public void startSuccessProgress() {
        increment = (100f - progress) * frameTime / animationTime;
        setProgressDrawableCompat(successDrawable);
        status = SUCCESS;
        nextProgress();
    }

    public void startErrorProgress() {
        increment = progress * frameTime / animationTime;
        setProgressDrawableCompat(errorDrawable);
        status = ERROR;
        nextProgress();
    }

    private void setProgressDrawableCompat(int resId) {
        if (Build.VERSION.SDK_INT >= 21) {
            setProgressDrawable(getResources().getDrawable(resId, null));
        } else {
            //noinspection deprecation
            setProgressDrawable(getResources().getDrawable(resId));
        }
    }

    private void nextProgress() {
        removeCallbacks(progressRunnable);
        if (status == NORMAL) {
            progress += (max - progress) / 30;
        } else if (status == SUCCESS) {
            if (progress >= 100f) {
                // on complete
                setVisibility(GONE);
                return;
            }

            progress += increment;
        } else if (status == ERROR) {
            if (progress <= 0f) {
                // on complete
                setVisibility(GONE);
                return;
            }

            progress -= decrement;
        }

        setProgress((int)(100 * progress));
        postDelayed(progressRunnable, frameTime);
    }

    private Runnable progressRunnable = new Runnable() {
        @Override
        public void run() {
            nextProgress();
        }
    };
}
