package uz.ishow.android.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 26.11.2015 15:53.
 */
public class SpecialFrameLayout extends FrameLayout {


    public SpecialFrameLayout(Context context) {
        super(context);
    }

    public SpecialFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SpecialFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public SpecialFrameLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(resolveSize(1, widthMeasureSpec),
                resolveSize(1, heightMeasureSpec));
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        if (!changed) {
            return;
        }
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                child.layout(left, top, right, bottom);
            }
        }
    }
}
