package uz.ishow.android.view.util;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 23.09.2015 11:33.
 */
public abstract class EndlessRecyclerViewOnScrollListener extends RecyclerView.OnScrollListener {

    RecyclerView.LayoutManager layoutManager;

    private int visibleThreshold = 2;
    private int firstVisibleItem;
    private int lastVisibleItem;
    private int visibleItemCount;
    private int totalItemCount;

    private boolean fromBottom = true;

    public EndlessRecyclerViewOnScrollListener(RecyclerView.LayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    public EndlessRecyclerViewOnScrollListener(RecyclerView.LayoutManager layoutManager, boolean fromBottom) {
        this(layoutManager);
        this.fromBottom = fromBottom;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        visibleItemCount = recyclerView.getChildCount();

        if (fromBottom) {
            totalItemCount = layoutManager.getItemCount();
            if (layoutManager instanceof LinearLayoutManager) {
                firstVisibleItem = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
            } else {
                return;
            }

            if ((totalItemCount - visibleItemCount - firstVisibleItem) <= visibleThreshold) {
                onLoadMore();
            }
        } else {
            if (layoutManager instanceof LinearLayoutManager) {
                lastVisibleItem = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
            } else {
                return;
            }

            if (lastVisibleItem - visibleItemCount <= visibleThreshold) {
                onLoadMore();
            }
        }
    }

    public abstract void onLoadMore();
}
