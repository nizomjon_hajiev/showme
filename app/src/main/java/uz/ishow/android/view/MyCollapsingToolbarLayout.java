package uz.ishow.android.view;

import android.content.Context;
import android.support.design.widget.CollapsingToolbarLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 13.07.2015 14:58.
 */
public class MyCollapsingToolbarLayout extends CollapsingToolbarLayout {

    private final ArrayList<View> mMatchParentChildren = new ArrayList<View>(1);

    public MyCollapsingToolbarLayout(Context context) {
        super(context);
    }

    public MyCollapsingToolbarLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyCollapsingToolbarLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int count = getChildCount();

        int maxHeight = 0;

        mMatchParentChildren.clear();

        for (int i = 0; i < count; i++) {
            final View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                final LayoutParams lp = (LayoutParams) child.getLayoutParams();
                if (lp.height != ViewGroup.LayoutParams.MATCH_PARENT) {
                    measureChildWithMargins(child, widthMeasureSpec, 0, heightMeasureSpec, 0);
                    maxHeight = Math.max(maxHeight, child.getMeasuredHeight() + lp.topMargin + lp.bottomMargin);
                } else {
                    mMatchParentChildren.add(child);
                }
            }
        }

//        maxHeight += getPaddingTopWithForeground() + getPaddingBottomWithForeground();

        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = maxHeight;

        setMeasuredDimension(width, height);

        if (mMatchParentChildren.size() > 0) {
            for (int i = 0; i < mMatchParentChildren.size(); i++) {
                final View child = mMatchParentChildren.get(i);

                final MarginLayoutParams lp = (MarginLayoutParams) child.getLayoutParams();
                int childWidthMeasureSpec;
                int childHeightMeasureSpec;

                if (lp.width == LayoutParams.MATCH_PARENT) {
                    childWidthMeasureSpec = MeasureSpec.makeMeasureSpec(getMeasuredWidth() -
                                    getPaddingLeftWithForeground() - getPaddingRightWithForeground() -
                                    lp.leftMargin - lp.rightMargin,
                            MeasureSpec.EXACTLY);
                } else {
                    childWidthMeasureSpec = getChildMeasureSpec(widthMeasureSpec,
                            getPaddingLeftWithForeground() + getPaddingRightWithForeground() +
                                    lp.leftMargin + lp.rightMargin,
                            lp.width);
                }

                childHeightMeasureSpec = MeasureSpec.makeMeasureSpec(getMeasuredHeight() -
                                getPaddingTopWithForeground() - getPaddingBottomWithForeground() -
                                lp.topMargin - lp.bottomMargin,
                        MeasureSpec.EXACTLY);

                child.measure(childWidthMeasureSpec, childHeightMeasureSpec);
            }
        } else {
            System.out.println(mMatchParentChildren);
        }
    }


    int getPaddingLeftWithForeground() {
        return 0;
    }

    int getPaddingRightWithForeground() {
        return 0;
    }

    private int getPaddingTopWithForeground() {
        return 0;
    }

    private int getPaddingBottomWithForeground() {
        return 0;
    }

}
