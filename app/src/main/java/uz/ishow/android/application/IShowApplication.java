package uz.ishow.android.application;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Handler;
import android.support.multidex.MultiDex;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import dagger.ObjectGraph;
import uz.fonus.gcm.GcmIntentService;
import uz.ishow.android.activity.BaseActivity;
import uz.ishow.android.activity.MainActivity;
import uz.ishow.android.activity.PostActivity;
import uz.ishow.android.model.Profile;
import uz.ishow.android.model.PushNotification;
import uz.ishow.android.module.MainModule;
import uz.ishow.android.util.MediaCenter;
import uz.ishow.android.util.NotificationUtils;
import uz.ishow.android.util.Settings;
import uz.ishow.android.util.Utils;

//import com.crashlytics.android.Crashlytics;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 10.06.2015 17:29.
 */


public class IShowApplication extends Application {


    public static final String GCM_SENDER_ID = "963892658278";

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "ykCwB16KGIZP86OvI70ZJqesz";
    private static final String TWITTER_SECRET = "JzbvQKoVxAEhMN4BcEmhfZZxZGL8Rmvniu94ZPVfYuzm6yzNbD";

    private ObjectGraph graph;
    private static Profile sProfile;
    private static boolean isStar;
    private List<BaseActivity> activities = new ArrayList<>();
    private Handler handler = new Handler();
    public boolean shouldDownload;
    public static IShowApplication iShowApplication;

    public static IShowApplication getInstanse() {
        return iShowApplication;
    }

    @Inject
    Settings settings;

    @Inject
    Gson gson;
    File myDirection;

    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
//        Fabric.with(this, new TwitterCore(authConfig), new Digits(), new Crashlytics());

        graph = ObjectGraph.create(new MainModule(this));
        inject(this);
        invalidateLocale();

        myDirection = new File(getExternalFilesDir(null), "audio");

        if (!myDirection.exists()) {
            myDirection.mkdirs();
        }

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);

        int savedVersionCode = settings.getAppVersionCode();
        int versionCode = Utils.getVersionCode(getApplicationContext());
        if (versionCode > savedVersionCode) {
            try {
                ImageLoader.getInstance().clearMemoryCache();
                ImageLoader.getInstance().clearDiskCache();
                settings.saveAppVersionCode(versionCode);
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }

        MediaCenter.initInstance(this);

        IntentFilter intentFilter = new IntentFilter(GcmIntentService.NEW_PUSH_INTENT);
        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (!settings.isAuthorized()) {
                    return;
                }

                String json = intent.getStringExtra("body");
                if (!TextUtils.isEmpty(json)) {
                    try {
                        PushNotification pushNotification = gson.fromJson(json, PushNotification.class);
                        if (pushNotification != null) {
                            if (!settings.isPushShowed(pushNotification)) {
                                NotificationUtils.show(pushNotification, getApplicationContext());
                                settings.setPushAsShowed(pushNotification);
                            }
                        }
                    } catch (JsonSyntaxException e) {
                    }

                }
            }
        }, intentFilter);

        IntentFilter intentFilter2 = new IntentFilter(NotificationUtils.OPEN_POST_FROM_NOTIFICATION);
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Intent activityIntent = new Intent(getApplicationContext(), PostActivity.class);
                activityIntent.putExtras(intent);
//                if (activities.isEmpty()) {
                activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activityIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(activityIntent);
//                } else {
//                    activityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    activities.get(activities.size() - 1).startActivity(activityIntent);
//                }
            }
        }, intentFilter2);


    }

    public File getMyDirection() {
        return myDirection;
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public void inject(Object object) {
        try {
            graph.inject(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        invalidateLocale();
    }

    protected void invalidateLocale() {
        String lang = settings.getLanguage();
        Configuration conf = getResources().getConfiguration();
        if (!lang.equals(conf.locale.getLanguage())) {
            Locale appLocale = new Locale(lang);
            Locale.setDefault(appLocale);
            conf.locale = appLocale;
            getResources().updateConfiguration(conf, getResources().getDisplayMetrics());
        }
    }

    public void onActivityResume(BaseActivity activity) {
        if (!activities.contains(activity)) {
            activities.add(activity);
        }
    }

    public void onActivityFinish(BaseActivity activity) {
        if (activities.contains(activity)) {
            activities.remove(activity);
        }

        if (activities.size() == 0) {
            handler.postDelayed(pauseMediaPlayerRunnable, 500);

            if (activity instanceof PostActivity) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }
    }

    private Runnable pauseMediaPlayerRunnable = new Runnable() {
        @Override
        public void run() {
            if (activities.size() == 0) {
                MediaCenter.getInstance().pauseAudio();
            }
        }
    };

    public static Profile getProfile() {
        return sProfile;
    }

    public static void setProfile(Profile profile) {
        sProfile = profile;
    }

    public static boolean isStar() {
        return isStar;
    }

    public static void setIsStar(boolean isStar) {
        IShowApplication.isStar = isStar;
    }

    public boolean isShouldDownload() {
        return shouldDownload;
    }

    public void setShouldDownload(boolean shouldDownload) {
        this.shouldDownload = shouldDownload;
    }
}
