package uz.ishow.android.event;

import uz.ishow.android.model.Audio;
import uz.ishow.android.model.Post;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 26.10.2015 16:37.
 */
public class AudioEvent {

    private Type type;
    private int progress;
    private Post audioPost;
    private Audio mAudio;

    public static AudioEvent start(Post post) {
        AudioEvent e = new AudioEvent();
        e.type = Type.START;
        e.audioPost = post;
        return e;
    }

    public static AudioEvent resume(Post post, int progress) {
        AudioEvent e = new AudioEvent();
        e.type = Type.RESUME;
        e.audioPost = post;
        e.progress = progress;
        return e;
    }

    public static AudioEvent forward(Post post, int progress) {
        AudioEvent e = new AudioEvent();
        e.type = Type.FORWARD;
        e.progress = progress;
        return e;
    }

    public static AudioEvent backward(Post post, int progress) {
        AudioEvent e = new AudioEvent();
        e.type = Type.BACKWARD;
        e.progress = progress;
        return e;
    }

    public static AudioEvent progress(Post post, int progress) {
        AudioEvent e = new AudioEvent();
        e.type = Type.PROGRESS;
        e.audioPost = post;
        e.progress = progress;
        return e;
    }


    public static AudioEvent downloaded(Post audio) {
        AudioEvent e = new AudioEvent();
        e.audioPost = audio;
        e.type = Type.DOWNLOADED;
        return e;
    }

    public static AudioEvent should_download(Post post) {
        AudioEvent e = new AudioEvent();
        e.audioPost = post;
        e.type = Type.SHOULD_DOWNLOAD;
        return e;
    }

    public static AudioEvent stop(Post audioPost) {
        AudioEvent e = new AudioEvent();
        e.type = Type.STOP;
        e.audioPost = audioPost;
        return e;
    }

    public static AudioEvent pause(Post audioPost) {
        AudioEvent e = new AudioEvent();
        e.type = Type.PAUSE;
        e.audioPost = audioPost;
        return e;
    }

    public static AudioEvent closePlayer() {
        AudioEvent e = new AudioEvent();
        e.type = Type.CLOSE_PLAYER;
        return e;
    }

    public enum Type {
        START,
        PAUSE,
        RESUME,
        STOP,
        DOWNLOADED,
        SHOULD_DOWNLOAD,
        PROGRESS,
        FORWARD,
        BACKWARD,
        CLOSE_PLAYER
    }

    public Type getType() {
        return type;
    }

    public int getProgress() {
        return progress;
    }

    public Post getAudioPost() {
        return audioPost;
    }

    public Audio getAudio() {
        return mAudio;
    }
}
