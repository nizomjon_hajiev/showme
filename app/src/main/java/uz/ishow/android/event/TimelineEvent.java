package uz.ishow.android.event;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 20.11.2015 10:21.
 */
public class TimelineEvent {

    private Type type;


    public enum Type {
        RELOAD_MY_TIMELINE,
        REFRESH_CONTENT
    }


    public static TimelineEvent reloadMyTimeline() {
        TimelineEvent event = new TimelineEvent();
        event.type = Type.RELOAD_MY_TIMELINE;
        return event;
    }

    public static TimelineEvent refreshTimeline() {
        TimelineEvent event = new TimelineEvent();
        event.type = Type.REFRESH_CONTENT;
        return event;
    }

    public Type getType() {
        return type;
    }
}
