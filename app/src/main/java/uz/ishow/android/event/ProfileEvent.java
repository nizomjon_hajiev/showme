package uz.ishow.android.event;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 16.11.2015 10:46.
 */
public class ProfileEvent {

    private Type type;

    public enum Type {
        USER_LOGGED_IN,
        USER_LOGGED_OUT
    }

    public static ProfileEvent loggedIn() {
        ProfileEvent e = new ProfileEvent();
        e.type = Type.USER_LOGGED_IN;
        return e;
    }

    public static ProfileEvent loggedOut() {
        ProfileEvent e = new ProfileEvent();
        e.type = Type.USER_LOGGED_OUT;
        return e;
    }

    public Type getType() {
        return type;
    }
}
