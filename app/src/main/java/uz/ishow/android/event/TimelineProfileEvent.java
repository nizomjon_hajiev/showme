package uz.ishow.android.event;

import uz.ishow.android.model.Profile;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 17.03.2016 22:28.
 */
public class TimelineProfileEvent {

    private Type mType;
    private Profile profile;
    public boolean canCall;

    public enum Type {
        CALL_TO_USER,
        GIVE_OPTION_TO_CALL
    }

    public static TimelineProfileEvent callToUser() {
        TimelineProfileEvent event = new TimelineProfileEvent();
        event.mType = Type.CALL_TO_USER;
        return event;
    }

    public static TimelineProfileEvent giveOptionToCall(boolean canCallTo) {
        TimelineProfileEvent event = new TimelineProfileEvent();
        event.mType = Type.GIVE_OPTION_TO_CALL;
        event.canCall = canCallTo;
        return event;
    }

    public TimelineProfileEvent(Profile profile) {
        this.profile = profile;
    }

    public TimelineProfileEvent() {
    }

    public boolean isCanCall() {
        return canCall;
    }

    public Profile getProfile() {
        return profile;
    }

    public Type getType() {
        return mType;
    }
}
