package uz.ishow.android.event;

import android.graphics.Bitmap;

import uz.ishow.android.model.Post;
import uz.ishow.android.model.Profile;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 30.09.2015 17:03.
 */
public class FeedItemEvent {

    private Type type;
    private String profileId;
    private String postId;
    private int likeCount;
    private int commentCount;
    private Profile user;
    private Bitmap profileImageBitmap;
    private Post post;

    private boolean isLiked;
    private boolean isFollowing;
    private int followersCount;

    public enum Type {
        FOLLOWING,
        DISFOLLOWING,
        COMMENT,
        LIKE,
        USER_DATA_CHANGED,
        NEW_POST_ADDED,
        POST_DELETED
    }

    public static FeedItemEvent followingChanged(Profile user,String profileId, int followersCount, boolean isFollowing) {
        FeedItemEvent event = new FeedItemEvent();
        event.type = Type.FOLLOWING;
        event.user = user;
        event.profileId = profileId;
        event.isFollowing = isFollowing;
        event.followersCount = followersCount;
        return event;
    }

    public static FeedItemEvent followingChangedToDis(Profile user,String profileId, int followersCount, boolean isFollowing) {
        FeedItemEvent event = new FeedItemEvent();
        event.type = Type.DISFOLLOWING;
        event.user = user;
        event.profileId = profileId;
        event.isFollowing = isFollowing;
        event.followersCount = followersCount;
        return event;
    }


    public static FeedItemEvent likeChanged(String postId, boolean isLiked, int likeCount) {
        FeedItemEvent event = new FeedItemEvent();
        event.type = Type.LIKE;
        event.postId = postId;
        event.likeCount = likeCount;
        event.isLiked = isLiked;
        return event;
    }

    public static FeedItemEvent commentChanged(String postId, int commentCount) {
        FeedItemEvent event = new FeedItemEvent();
        event.type = Type.COMMENT;
        event.postId = postId;
        event.commentCount = commentCount;
        return event;
    }

    public static FeedItemEvent userDataChanged(Profile user, Bitmap profileImageBitmap) {
        FeedItemEvent event = new FeedItemEvent();
        event.type = Type.USER_DATA_CHANGED;
        event.user = user;
        if (user != null) {
            event.profileId = user.getId();
        }
        event.profileImageBitmap = profileImageBitmap;
        return event;
    }

    public static FeedItemEvent newPostAdded() {
        FeedItemEvent event = new FeedItemEvent();
        event.type = Type.NEW_POST_ADDED;
        return event;
    }

    public static FeedItemEvent postDeleted(Post post) {
        FeedItemEvent event = new FeedItemEvent();
        event.type = Type.POST_DELETED;
        event.post = post;
        if (post != null) {
            event.postId = post.getId();
        }
        return event;
    }

    public Type getType() {
        return type;
    }

    public String getProfileId() {
        return profileId;
    }

    public String getPostId() {
        return postId;
    }

    public boolean isFollowing() {
        return isFollowing;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public Profile getUser() {
        return user;
    }

    public Bitmap getProfileImageBitmap() {
        return profileImageBitmap;
    }

    public Post getPost() {
        return post;
    }

    public int getFollowersCount() {
        return followersCount;
    }
}
