package uz.ishow.android.module;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import de.greenrobot.event.EventBus;
import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;
import uz.ishow.android.activity.AboutActivity;
import uz.ishow.android.activity.BaseActivity;
import uz.ishow.android.activity.CategoryUsersActivity;
import uz.ishow.android.activity.CommentsActivity;
import uz.ishow.android.activity.CreatePostActivity;
import uz.ishow.android.activity.EditProfileActivity;
import uz.ishow.android.activity.FollowingsActivity;
import uz.ishow.android.activity.IntroActivity;
import uz.ishow.android.activity.LocAndTagActivity;
import uz.ishow.android.activity.MainActivity;
import uz.ishow.android.activity.NominationActivity;
import uz.ishow.android.activity.PickPhotoActivity;
import uz.ishow.android.activity.PollActivity;
import uz.ishow.android.activity.PostActivity;
import uz.ishow.android.activity.ProfileActivity;
import uz.ishow.android.activity.RegisterActivity;
import uz.ishow.android.activity.SearchActivity;
import uz.ishow.android.activity.TwitterDigitsActivity;
import uz.ishow.android.activity.UsersActivity;
import uz.ishow.android.activity.VideoPlayerActivity;
import uz.ishow.android.api.Api;
import uz.ishow.android.application.IShowApplication;
import uz.ishow.android.event.ProfileEvent;
import uz.ishow.android.fragment.BaseFragment;
import uz.ishow.android.fragment.CategoriesFragment;
import uz.ishow.android.fragment.MyProfileFragment;
import uz.ishow.android.fragment.MyTimelineFragment;
import uz.ishow.android.fragment.PollFragment;
import uz.ishow.android.fragment.TimelineFragment;
import uz.ishow.android.util.CacheUtils;
import uz.ishow.android.util.Settings;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 26.04.2015 20:40.
 */

@Module(
        library = true,
        injects = {
                IShowApplication.class,
                BaseActivity.class,
                MainActivity.class,
                IntroActivity.class,
                UsersActivity.class,
                ProfileActivity.class,
                CommentsActivity.class,
                TwitterDigitsActivity.class,
                CreatePostActivity.class,
                CategoryUsersActivity.class,
                SearchActivity.class,
                AboutActivity.class,
                VideoPlayerActivity.class,
                RegisterActivity.class,
                PostActivity.class,
                EditProfileActivity.class,
                PollActivity.class,
                NominationActivity.class,
                FollowingsActivity.class,
                PickPhotoActivity.class,
                LocAndTagActivity.class,

                BaseFragment.class,
                TimelineFragment.class,
                CategoriesFragment.class,
                PollFragment.class,
                MyTimelineFragment.class,
                MyProfileFragment.class,
                PollFragment.class

        }
)
public class MainModule {

    private IShowApplication application;

    //    public static final String PRODUCTION_ENDPOINT = "http://192.168.1.188:8000/v2/";
    public static final String BETA_ENDPOINT = "http://beta.app.ishow.uz/v2/";
    public static final String PRODUCTION_ENDPOINT = "http://app.ishow.uz/v2/";

    public static final AppType APP_TYPE = AppType.PRODUCTION;

    public MainModule(IShowApplication application) {
        this.application = application;
    }


    @Provides
    @Singleton
    RestAdapter provideRestAdapter(final Gson gson, final Settings settings, final CacheUtils cacheUtils) {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);

        return new RestAdapter.Builder()
                .setEndpoint(PRODUCTION_ENDPOINT
                )

                .setClient(new OkClient(okHttpClient))
//                .setClient(new MockHttpClient(application))
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader("Accept", "*/*");
                        if (settings.isAuthorized()) {
                            request.addHeader("Authorization",
                                    String.format("Token %s", settings.getAuthToken()));
                            request.addHeader("Accept-Language", settings.getLanguage());
                        }
                    }
                })
                .setErrorHandler(new ErrorHandler() {
                    @Override
                    public Throwable handleError(RetrofitError cause) {
                        if (cause.getResponse() != null && cause.getResponse().getStatus() == 401) {
                            cacheUtils.clear();
                            settings.saveAuthToken(null);
                            EventBus.getDefault().post(ProfileEvent.loggedOut());
                        }
                        return cause;
                    }
                })
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(new GsonConverter(gson)).build();
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }

    @Provides
    @Singleton
    Settings provideSettings(SharedPreferences preferences) {
        return new Settings(preferences);
    }

    @Provides
    @Singleton
    CacheUtils provideCacheUtils(Gson gson) {
        CacheUtils cacheUtils = new CacheUtils(application, gson);
        EventBus.getDefault().register(cacheUtils);
        return cacheUtils;
    }



    @Provides
    @Singleton
    Api provideFeedApi(RestAdapter restAdapter) {
        return restAdapter.create(Api.class);
    }

    public enum AppType {
        BETA,
        PRODUCTION
    }
}
