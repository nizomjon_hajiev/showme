package uz.fonus.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import com.google.android.gms.gcm.GoogleCloudMessaging;


/**
 * Created by Sarimsakov Bakhrom Azimovich on 29.09.13 17:46.
 */
public class GcmIntentService extends IntentService {

    public static final String NEW_PUSH_INTENT = "uz.fonus.gcm.GcmIntentService.NEW_PUSH_INTENT";

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Bundle extras = intent.getExtras();

        if(extras == null) {
            return;
        }

        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {

            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {

            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {

                Intent localBroadcast = new Intent(NEW_PUSH_INTENT);
                localBroadcast.putExtras(intent);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(localBroadcast);
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }
}
