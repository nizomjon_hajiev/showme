package uz.fonus.gcm;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 30.09.13 8:46.
 */
public class GcmHelper {

    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    Activity mActivity;

    public static final String TAG = "GcmHelper";
    GoogleCloudMessaging gcm;
    String regid;

    private GcmListener mGcmListener;
    private String mSenderId;


    public GcmHelper(String senderId, Activity activity, @Nullable GcmListener gcmListener) {
        this.mActivity = activity;
        this.mGcmListener = gcmListener;
        this.mSenderId = senderId;
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    public boolean checkPlayServices(boolean showDialog) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mActivity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                if (showDialog) {
                    GooglePlayServicesUtil.getErrorDialog(resultCode, mActivity,
                            PLAY_SERVICES_RESOLUTION_REQUEST).show();
                }
            } else {
                Log.i(TAG, "This device is not supported.");
                if (mGcmListener != null) {
                    mGcmListener.onDeviceNotSupport();
                }
                return false;
            }

            if (mGcmListener != null) {
                mGcmListener.onPlayServicesNotFound();
            }
            return false;
        }
        return true;
    }

    public boolean isRegisterRequired() {
        if (checkPlayServices(false)) {
            gcm = GoogleCloudMessaging.getInstance(mActivity);
            regid = getRegistrationId(mActivity);
            return regid.isEmpty();

        }
        return true;
    }

    public String getRegisterKey() {
        gcm = GoogleCloudMessaging.getInstance(mActivity);
        regid = getRegistrationId(mActivity);
        return regid;
    }

    public void register() {
        if (checkPlayServices(true)) {
            gcm = GoogleCloudMessaging.getInstance(mActivity);
            regid = getRegistrationId(mActivity);

            if (regid.isEmpty()) {
                registerInBackground();
            }
        }
    }


    /**
     * Registers the application with GCM servers asynchronously.
     * <p/>
     * Stores the registration ID and the app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {

            private Exception error;

            @Override
            protected String doInBackground(Void... params) {
                String msg;
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(mActivity);
                    }
                    regid = gcm.register(mSenderId);
                    msg = "Device registered, registration ID=" + regid;
                    storeRegistrationId(mActivity, regid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    error = ex;
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                if (error == null) {
                    sendRegistrationIdToBackend();
                }
            }
        }.execute(null, null, null);
    }

    /**
     * Stores the registration ID and the app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId   registration ID
     */
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGcmPreferences(context);
        int appVersion = getAppVersion(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.apply();
    }


    /**
     * Gets the current registration ID for application on GCM service, if there is one.
     * <p/>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     * registration ID.
     */
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGcmPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }

        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            return "";
        }
        return registrationId;
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (Exception e) {
        }

        return 0;
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGcmPreferences(Context context) {
        return context.getSharedPreferences("gcm_preferences", Context.MODE_PRIVATE);
    }

    private void sendRegistrationIdToBackend() {
        // Your implementation here.
        if (mGcmListener != null) {
            mGcmListener.onSendToBackend(regid);
        }
    }

}
