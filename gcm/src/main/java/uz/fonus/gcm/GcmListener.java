package uz.fonus.gcm;

/**
 * Created by Sarimsakov Bakhrom Azimovich on 15.07.2015 16:05.
 */
public interface GcmListener {
    void onDeviceNotSupport();
    void onPlayServicesNotFound();
    void onSendToBackend(String regId);
}
